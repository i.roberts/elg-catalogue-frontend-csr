import React from "react";
import FreeText from "../editorCommonComponents/FreeText";
import LRAutocomplete from "../EditorCorpusComponents/LRAutocomplete";
import AutocompleteOneChoice from "../editorCommonComponents/AutocompleteOneChoice";
//import RecordSelectList from "../editorCommonComponents/RecordSelectList";
import LanguageSpecificText from "../editorCommonComponents/LanguageSpecificText";
import GenericSchemaParser from "../../parsers/GenericSchemaParser";
import AutocompleteRecommendedChoices from "../editorCommonComponents/AutocompleteRecommendedChoices";
//import FreeTextList from "../editorCommonComponents/FreeTextList";
import LdSubclassNGramItem from "../EditorLdComponents/LdSubclassNGramItem";
import { ldNgramObj } from "../Models/LdModel";
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from "@material-ui/core/Button"
import messages from "../../config/messages";
import AutocompleteModelType from "../editorCommonComponents/AutocompleteModelType";

export default class LdSubclassMLModelItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = { initialValue: props.initialValue }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            initialValue: nextProps.initialValue
        };
    }

    setString = (field, value) => {
        const { initialValue } = this.state;
        initialValue[field] = value;
        this.setState({ initialValue });
    }

    setSingleSelectListGeneric = (event, selectedObjArray, field) => {
        const { initialValue } = this.state;
        if (selectedObjArray.length === 0) {
            selectedObjArray[0] = { value: null };
        }
        initialValue[field] = selectedObjArray[0].value;
        this.setState({ initialValue }, this.onBlur);
    }

    updateNgramModel = (valueObj) => {
        const { initialValue } = this.state;
        initialValue["n_gram_model"] = valueObj;
        this.setState({ initialValue });
    }

    setObjectGeneral = (field, valueObj) => {
        const { initialValue } = this.state;
        initialValue[field] = valueObj;
        this.setState({ initialValue }, this.onBlur());
    }

    updateModel__array = (obj2update, valueArray) => {
        const { initialValue } = this.state;
        if (!initialValue[obj2update]) {
            initialValue[obj2update] = [];
        }
        initialValue[obj2update] = valueArray;
        this.setState({ initialValue }, this.onBlur());
    }


    onBlur = () => {
        this.props.setValues(this.props.initialValue);
    }

    render() {
        const { initialValue } = this.state;
        const showNgramModel = initialValue.model_type && initialValue.model_type.filter(item => item === "http://w3id.org/meta-share/meta-share/ngramModel" || item === "n-gram model").length > 0;
        if (showNgramModel && !initialValue["n_gram_model"]) {
            initialValue["n_gram_model"] = JSON.parse(JSON.stringify(ldNgramObj));
        }
        return <div>
            <div className="pb-3">
                <AutocompleteModelType className="wd-100"
                    {...GenericSchemaParser.getFormElement("model_function", this.props.model_function)}
                    initialValuesArray={initialValue.model_function || []}
                    field="model_function"
                    updateModel_Array={this.updateModel__array} />
            </div>

            <div className="pb-3">
                <AutocompleteModelType className="wd-100"
                    {...GenericSchemaParser.getFormElement("model_type", this.props.model_type)}
                    initialValuesArray={initialValue.model_type || []}
                    field="model_type"
                    updateModel_Array={this.updateModel__array} />
            </div>

            <div className="pt-3 pb-3"><FreeText className="wd-100" {...this.props.model_variant} initialValue={initialValue.model_variant} field="model_variant" lr_subclass={true} updateModel={this.setString} /></div>
            {false && <div className="pb-3"><LRAutocomplete {...GenericSchemaParser.getFormElement("typesystem", this.props.typesystem)} initialValueArray={initialValue.typesystem || []} field="typesystem" updateModel_Array={this.setObjectGeneral} /> </div>}
            {false && <div className="pb-3">
                <AutocompleteOneChoice
                    {...GenericSchemaParser.getFormElement("method", this.props.method)}
                    initialValue={initialValue.method || ""}
                    field="method"
                    updateModel_String={this.setObjectGeneral}
                />
            </div>}
            {/*<div className="pt-3 pb-3"><FreeText className="wd-100" {...this.props.ml_framework} initialValue={initialValue.ml_framework} field="ml_framework" lr_subclass={true} updateModel={this.setString} /></div>*/}
            {/*<div className="pb-3"><RecordSelectList className="wd-100" {...this.props.ml_framework1} choices={this.props.ml_framework1.choices} default_value={initialValue.ml_framework1} field="ml_framework1" lr_subclass={true} setSelectedvalue={this.setSingleSelectListGeneric} /></div>*/}
            <div className="pb-3"><AutocompleteRecommendedChoices className="wd-100" {...GenericSchemaParser.getFormElement("development_framework", this.props.development_framework)} recommended_choices={GenericSchemaParser.getFormElement("development_framework", this.props.development_framework).choices} initialValuesArray={initialValue.development_framework || []} field="development_framework" updateModel_array={this.updateModel__array} /></div>

            <div className="pb-3"><LanguageSpecificText {...GenericSchemaParser.getFormElement("training_corpus_details", this.props.training_corpus_details)} defaultValueObj={initialValue.training_corpus_details || { "en": "" }} field="training_corpus_details" multiline={true} maxRows={6} setLanguageSpecificText={this.setObjectGeneral} /></div>
            <div className="pb-3"><LanguageSpecificText {...GenericSchemaParser.getFormElement("training_process_details", this.props.training_process_details)} defaultValueObj={initialValue.training_process_details || { "en": "" }} field="training_process_details" multiline={true} maxRows={6} setLanguageSpecificText={this.setObjectGeneral} /></div>
            <div className="pb-3"><LanguageSpecificText {...GenericSchemaParser.getFormElement("bias_details", this.props.bias_details)} defaultValueObj={initialValue.bias_details || { "en": "" }} field="bias_details" multiline={true} maxRows={6} setLanguageSpecificText={this.setObjectGeneral} /></div>

            <div className="pb-3"><LRAutocomplete {...GenericSchemaParser.getFormElement("has_original_source", this.props.has_original_source)} initialValueArray={initialValue.has_original_source || []} field="has_original_source" updateModel_Array={this.setObjectGeneral} /> </div>
            {/*<div className="pb-3"><FreeTextList className="wd-100" {...GenericSchemaParser.getFormElement("model_function", this.props.model_function)} default_value_Array={initialValue.model_function || []} field="model_function" updateModel_Array={this.updateModel__array} /></div>*/}
            {/*<div className="pb-3"><FreeTextList className="wd-100" {...GenericSchemaParser.getFormElement("model_type", this.props.model_type)} default_value_Array={initialValue.model_type || []} field="model_type" updateModel_Array={this.updateModel__array} /></div>*/}



            <div className="pb-3"><LRAutocomplete {...GenericSchemaParser.getFormElement("requires_lr", this.props.requires_lr)} initialValueArray={initialValue.requires_lr || []} field="requires_lr" updateModel_Array={this.setObjectGeneral} /> </div>

            {showNgramModel && <div className="pb-3 inner--group nested--group">

                <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1}>

                    <Grid item sm={11}>
                        <Typography variant="h4" className="section-links" >{this.props.n_gram_model.label}</Typography>
                        <Typography variant="h4" className="section-links" >{this.props.n_gram_model.help_text}</Typography>
                    </Grid>

                    {false && <Grid item sm={1}>
                        {!initialValue.n_gram_model &&
                            <Button
                                className="inner-link-default--purple"
                                onClick={(e) => this.updateNgramModel(JSON.parse(JSON.stringify(ldNgramObj)))}>
                                {messages.group_elements_create}
                            </Button>
                        }
                        {initialValue.n_gram_model &&
                            <Button
                                className="inner-link-default--purple"
                                onClick={(e) => this.updateNgramModel(null)}>
                                {messages.group_elements_remove}
                            </Button>
                        }
                    </Grid>}
                    <Grid item sm={11}>
                        {initialValue.n_gram_model && <LdSubclassNGramItem {...this.props.n_gram_model.children} initialValue={JSON.parse(JSON.stringify(initialValue.n_gram_model || ldNgramObj))} setValues={this.updateNgramModel} />}
                    </Grid>

                </Grid>
            </div>
            }
            { /*
            <div className="pb-3"><LRAutocomplete {...GenericSchemaParser.getFormElement("annotation_schema", this.props.annotation_schema)} initialValueArray={initialValue.annotation_schema || []} field="annotation_schema" updateModel_Array={this.setObjectGeneral} />  </div>
            <div className="pb-3"><LRAutocomplete {...GenericSchemaParser.getFormElement("annotation_resource", this.props.annotation_resource)} initialValueArray={initialValue.annotation_resource || []} field="annotation_resource" updateModel_Array={this.setObjectGeneral} />  </div>
            <div className="pb-3"><LRAutocomplete {...GenericSchemaParser.getFormElement("tagset", this.props.tagset)} initialValueArray={initialValue.tagset || []} field="tagset" updateModel_Array={this.setObjectGeneral} /> </div> 
            <div className="pb-3"><LanguageSpecificText {...GenericSchemaParser.getFormElement("algorithm", this.props.algorithm)} defaultValueObj={initialValue.algorithm || { "en": "" }} field="algorithm" setLanguageSpecificText={this.setObjectGeneral} /></div>
            <div className="pb-3"><LanguageSpecificText {...GenericSchemaParser.getFormElement("algorithm_details", this.props.algorithm_details)} defaultValueObj={initialValue.algorithm_details || { "en": "" }} field="algorithm_details" setLanguageSpecificText={this.setObjectGeneral} /></div>
            */}

        </div>
    }

}