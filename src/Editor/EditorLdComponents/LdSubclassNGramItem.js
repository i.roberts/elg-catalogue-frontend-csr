import React from "react";
import { TextField } from "@material-ui/core";
import AutocompleteChoicesChips from "../editorCommonComponents/AutocompleteChoicesChips";
import GenericSchemaParser from "../../parsers/GenericSchemaParser";
//import RecordRadioBoolean from "../editorCommonComponents/RecordRadioBoolean";
//import FreeText from "../editorCommonComponents/FreeText";
//import FreeTextList from "../editorCommonComponents/FreeTextList";

export default class LdSubclassNGramItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = { initialValue: props.initialValue }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            initialValue: nextProps.initialValue
        };
    }

    setNumber = (field, value) => {
        if (value.length === 0) {
            value = null;
        } else {
            value = Number(value);
        }
        const { initialValue } = this.state;
        initialValue[field] = value;
        this.setState({ initialValue }, this.onBlur());
    }

    setString = (field, value) => {
        const { initialValue } = this.state;
        initialValue[field] = value;
        this.setState({ initialValue }, this.onBlur());
    }

    setObjectBooleanIsFactored = (e) => {
        const { initialValue } = this.state;
        if (initialValue.is_factored === null || initialValue.weighteis_factoredd_grammar === undefined) {
            initialValue.is_factored = null;
        }
        initialValue.is_factored = e.target.value === "true";
        this.setState({ initialValue }, this.onBlur());
    }

    setObjectBooleanInterpolated = (e) => {
        const { initialValue } = this.state;
        if (initialValue.interpolated === null || initialValue.interpolated === undefined) {
            initialValue.interpolated = null;
        }
        initialValue.interpolated = e.target.value === "true";
        this.setState({ initialValue }, this.onBlur());
    }

    updateModel__array = (field, valueArray) => {
        const { initialValue } = this.state;
        if (!initialValue[field]) {
            initialValue[field] = [];
        }
        initialValue[field] = valueArray;
        this.setState({ initialValue }, this.onBlur());
    }

    onBlur = () => {
        this.props.setValues(this.state.initialValue);
    }

    render() {
        const { initialValue } = this.state;
        return <div>

            <div className="pt-3 pb-3"><AutocompleteChoicesChips className="wd-100"  {...GenericSchemaParser.getFormElement("base_item", this.props.base_item)} lr_subclass={true} initialValuesArray={initialValue.base_item || []} field="base_item" updateModel_array={this.updateModel__array} /></div>
            <div className="pb-3"><TextField
                className="wd-100"
                variant="outlined"
                type={"number"}
                inputProps={{ name: 'order' }}
                required={this.props.order.required}
                disabled={this.props.order.read_only}
                label={this.props.order.label}
                helperText={this.props.order.help_text}
                value={initialValue.order === null ? "" : initialValue.order}
                onChange={(e) => { this.setNumber("order", e.target.value) }}
            /></div>
            {false && <div className="pb-3"><TextField
                className="wd-100"
                variant="outlined"
                type={"number"}
                inputProps={{ name: 'perplexity' }}
                required={this.props.perplexity.required}
                disabled={this.props.perplexity.read_only}
                label={this.props.perplexity.label}
                helperText={this.props.perplexity.help_text}
                value={initialValue.perplexity === null ? "" : initialValue.perplexity}
                onChange={(e) => { this.setNumber("perplexity", e.target.value) }}
            /></div>}
            {/*
            <div className="pt-3 pb-3"><FreeText className="wd-100" {...this.props.smoothing} initialValue={initialValue.smoothing} field="smoothing" lr_subclass={true} updateModel={this.setString} /></div>
            <div className="pb-3"><FreeTextList className="wd-100" {...this.props.factor} default_value_Array={initialValue.factor ? JSON.parse(JSON.stringify(initialValue.factor)) : []} field="factor" updateModel_Array={this.updateModel__array} /></div>
            <div className="pb-3"><RecordRadioBoolean className="wd-100" type={this.props.is_factored.type} required={this.props.is_factored.required} label={this.props.is_factored.label} help_text={this.props.is_factored.help_text} default_value={initialValue.is_factored} handleBooleanChange={this.setObjectBooleanIsFactored} /></div>
            <div className="pb-3"><RecordRadioBoolean className="wd-100" type={this.props.interpolated.type} required={this.props.interpolated.required} label={this.props.interpolated.label} help_text={this.props.interpolated.help_text} default_value={initialValue.interpolated} handleBooleanChange={this.setObjectBooleanInterpolated} /></div>
           */}

        </div>
    }

}