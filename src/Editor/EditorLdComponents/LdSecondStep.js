import React from "react";
import GenericSchemaParser from "../../parsers/GenericSchemaParser";
import { ReactComponent as TextIcon } from "./../../assets/elg-icons/office-file-text-graph-alternate.svg";
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import VerticalTabPanel from '../../componentsAPI/CustomVerticalTabs/VerticalTabPanel';
import Grid from '@material-ui/core/Grid';
import LanguageSpecificText from "../editorCommonComponents/LanguageSpecificText";
import LdModelSubclass from "./LdModelSubclass";
//import FreeTextList from "../editorCommonComponents/FreeTextList";
import AutocompleteChoicesChips from "../editorCommonComponents/AutocompleteChoicesChips";
import FreeText from "../editorCommonComponents/FreeText";
//import WebsiteList from "../editorCommonComponents/WebsiteList";
//import ActualUseArray from "./ActualUseArray";
//import ValidationArray from "./ValidationArray";
//import RecordSubjectAutocomplete from "../editorCommonComponents/RecordSubjectAutocomplete";
import RecordSelectList from "../editorCommonComponents/RecordSelectList";
//import RecordRadioBoolean from "../editorCommonComponents/RecordRadioBoolean";
//import LRAutocompleteSingle from "../EditorCorpusComponents/LRAutocompleteSingle";
import { LD_SECOND_SECTION_TABS_HEADERS, SUBCLASS_TYPE_MODEL, OTHER, UNSPECIFIED, SUBCLASS_TYPE_OTHER } from "../../config/editorConstants";
import { ldMediaUnspecifiedPartObj } from "../Models/LdModel";

function a11yProps(index) {
    return {
        id: `vertical-tab-${index}`,
        'aria-controls': `vertical-tabpanel-${index}`,
    };
}

export default class LdSecondStep extends React.Component {
    constructor(props) {
        super(props);
        this.state = { tab: props.tabInSection || 0 };
    }

    /* componentDidUpdate(prevProps, prevState) {
         const this_error = this.props.yupError;
         const prev_error = prevProps.yupError;
         if (this_error && prev_error && JSON.stringify(this_error) !== JSON.stringify(prev_error)) {
             //this.collorElements();
         }
     }*/

    collorElements = () => {
        const error = this.props.yupError;
        const elements2Color = []
        if (error && error.errors) {
            var ids = document.querySelectorAll("*[id]");
            error.errors.forEach((validationError, index) => {
                for (let index = 0; ids && index < ids.length; index++) {
                    const element = ids[index];
                    const element_id = ids[index].id;
                    element.classList.remove("yup-error-dynamic");
                    const parts = validationError.split(">");
                    if (parts && ((parts.length >= 5 && parts[4].includes(element_id)) || (parts.length >= 6 && parts[5].includes(element_id)))) {
                        elements2Color.push(element);
                        if (this.props.yupClickError && this.props.yupClickError.includes(element_id)) {
                            //element.scrollIntoView(false);
                            element.scrollIntoView({
                                behavior: 'auto',
                                block: 'center',
                                inline: 'center'
                            });
                        }
                    }
                }
            })
        }
        elements2Color.forEach(element => {
            element.classList.add("yup-error-dynamic");
        })
    }

    componentDidMount() {
        this.collorElements();
    }

    toggleTab = (tabIndex) => {
        this.setState({ tab: tabIndex });
        this.props.settabInSection(tabIndex);
    }

    updateModel_generic_array = (obj2update, valueArray, lr_subclass) => {
        const { model } = this.props;
        if (lr_subclass) {
            if (!model.described_entity.lr_subclass[obj2update]) {
                model.described_entity.lr_subclass[obj2update] = [];
            }
            model.described_entity.lr_subclass[obj2update] = valueArray;
            this.props.updateModel(model);
        } else {
            if (!model.described_entity[obj2update]) {
                model.described_entity[obj2update] = [];
            }
            model.described_entity[obj2update] = valueArray;
            this.props.updateModel(model);
        }
    }

    updateModel_generic_array_Subclass = (obj2update, valueArray) => {
        const { model } = this.props;
        if (!model.described_entity.lr_subclass[obj2update]) {
            model.described_entity.lr_subclass[obj2update] = [];
        }
        model.described_entity.lr_subclass[obj2update] = valueArray;
        this.props.updateModel(model);

    }

    setObjectGeneral = (field, valueObj) => {
        const { model } = this.props;
        if (!model.described_entity.lr_subclass[field]) {
            model.described_entity.lr_subclass[field] = "";
        }
        model.described_entity.lr_subclass[field] = valueObj;
        this.props.updateModel(model);
    }

    setObjectSubclass = (field, valueObj, ld_subclass_selected_choice) => {
        const { model } = this.props;
        if (!model.described_entity.lr_subclass[field]) {
            model.described_entity.lr_subclass[field] = {};
        }
        model.described_entity.lr_subclass[field] = valueObj;
        if (ld_subclass_selected_choice !== true && ld_subclass_selected_choice !== false) {
            if (!model.described_entity.lr_subclass["ld_subclass"]) {
                model.described_entity.lr_subclass["ld_subclass"] = "";
            }
            model.described_entity.lr_subclass["ld_subclass"] = ld_subclass_selected_choice;
        }

        if ((this.props.for_info || model.described_entity.lr_subclass["ld_subclass"] === SUBCLASS_TYPE_MODEL) && !model.described_entity.lr_subclass["unspecified_part"]) {
            model.described_entity.lr_subclass["unspecified_part"] = JSON.parse(JSON.stringify(ldMediaUnspecifiedPartObj));
            model.described_entity.lr_subclass.language_description_media_part = [];
        }/* else if (!this.props.for_info) {
            model.described_entity.lr_subclass["unspecified_part"] = null;
        }*/

        this.props.updateModel(model);
    }

    selectedValue = (e, value, field) => {
        const { model } = this.props;
        if (model.described_entity.lr_subclass[field] === null || model.described_entity.lr_subclass[field] === undefined) {
            model.described_entity.lr_subclass[field] = null;
        }
        model.described_entity.lr_subclass[field] = (value && value.length > 0) ? value[0].value : null;
        this.props.updateModel(model);
    }

    render() {
        const subclass = this.props.schema_lr_subclass;
        const grammarPart = this.props.schema_grammar_part;
        const mlPart = this.props.schema_ml_model_part;
        const { model } = this.props;
        if (!model.described_entity?.lr_subclass?.ld_subclass) {
            model.described_entity.lr_subclass.ld_subclass = SUBCLASS_TYPE_OTHER;
        }

        //rules
        model.described_entity.lr_subclass.personal_data_included !== "http://w3id.org/meta-share/meta-share/yesP" ? model.described_entity.lr_subclass["personal_data_details"] = null : void 0;
        model.described_entity.lr_subclass.sensitive_data_included !== "http://w3id.org/meta-share/meta-share/yesS" ? model.described_entity.lr_subclass["sensitive_data_details"] = null : void 0;
        model.described_entity.lr_subclass.anonymized !== "http://w3id.org/meta-share/meta-share/yesA" ? model.described_entity.lr_subclass["anonymization_details"] = null : void 0;
        const anonymized_required = (model.described_entity.lr_subclass.sensitive_data_included === "http://w3id.org/meta-share/meta-share/yesS" || model.described_entity.lr_subclass.personal_data_included === "http://w3id.org/meta-share/meta-share/yesP");

        return <div>
            <form >
                <div className="tabs-main-container">
                    <div className="vertical-tabs-container-forms">
                        <Tabs value={this.state.tab} onChange={this.toggleTab} variant="scrollable" orientation="vertical" aria-label="vertical tabs example" className="vertical-tabs-forms">
                            {LD_SECOND_SECTION_TABS_HEADERS.map((tab, index) => <Tab key={index} label={<><div><TextIcon className="small-icon general-icon--grey mr-05" /></div><div className="pt-2"> {tab} <div className="pt-2"></div></div> </>}  {...a11yProps(index)} onClick={() => { this.toggleTab(index); }} />)}
                        </Tabs>

                        <VerticalTabPanel value={this.state.tab} index={0} className="vertical-tab-pannel">
                            <Grid container direction="row" alignItems="flex-start" spacing={2} className="vertical-tabs-inner">
                                <Grid item xs>
                                    {![OTHER, UNSPECIFIED].includes(model.described_entity.lr_subclass.ld_subclass) && <div className="pb-3"
                                        id={GenericSchemaParser.getFormElement("language_description_subclass", subclass.language_description_subclass).label}>
                                        <LdModelSubclass className="wd-100"
                                            {...GenericSchemaParser.getFormElement("language_description_subclass", subclass.language_description_subclass)}
                                            subclass={subclass}
                                            grammarPart={grammarPart}
                                            mlPart={mlPart}
                                            initialValue={model.described_entity.lr_subclass.language_description_subclass}
                                            initialValue_ld_subclass_type={model.described_entity.lr_subclass.ld_subclass}
                                            field="language_description_subclass"
                                            lr_subclass={true}
                                            model={model}
                                            updateModel={this.setObjectSubclass}
                                        /></div>}

                                    <div className="pb-3" id={GenericSchemaParser.getFormElement("personal_data_included", subclass.personal_data_included).label}>
                                        <RecordSelectList
                                            className="wd-100"
                                            {...GenericSchemaParser.getFormElement("personal_data_included", subclass.personal_data_included)}
                                            required={this.props.for_info ? false : false}
                                            default_value={model.described_entity.lr_subclass.personal_data_included}
                                            field="personal_data_included"
                                            setSelectedvalue={this.selectedValue}
                                        />
                                    </div>
                                    {model.described_entity.lr_subclass.personal_data_included === "http://w3id.org/meta-share/meta-share/yesP" && <div className="pb-3" id={GenericSchemaParser.getFormElement("personal_data_details", subclass.personal_data_details).label}><LanguageSpecificText {...GenericSchemaParser.getFormElement("personal_data_details", subclass.personal_data_details)} defaultValueObj={model.described_entity.lr_subclass.personal_data_details || { "en": "" }} field="personal_data_details" multiline={true} maxRows={6} setLanguageSpecificText={this.setObjectGeneral} /></div>}

                                    <div className="pb-3" id={GenericSchemaParser.getFormElement("sensitive_data_included", subclass.sensitive_data_included).label}>
                                        <RecordSelectList
                                            className="wd-100"
                                            {...GenericSchemaParser.getFormElement("sensitive_data_included", subclass.sensitive_data_included)}
                                            required={this.props.for_info ? false : false}
                                            default_value={model.described_entity.lr_subclass.sensitive_data_included}
                                            field="sensitive_data_included"
                                            setSelectedvalue={this.selectedValue} />
                                    </div>
                                    {model.described_entity.lr_subclass.sensitive_data_included === "http://w3id.org/meta-share/meta-share/yesS" && <div className="pb-3" id={GenericSchemaParser.getFormElement("sensitive_data_details", subclass.sensitive_data_details).label}><LanguageSpecificText {...GenericSchemaParser.getFormElement("sensitive_data_details", subclass.sensitive_data_details)} defaultValueObj={model.described_entity.lr_subclass.sensitive_data_details || { "en": "" }} field="sensitive_data_details" multiline={true} maxRows={6} setLanguageSpecificText={this.setObjectGeneral} /></div>}

                                    <div className="pb-3" id={GenericSchemaParser.getFormElement("anonymized", subclass.anonymized).label}>
                                        <RecordSelectList
                                            className="wd-100"
                                            {...GenericSchemaParser.getFormElement("anonymized", subclass.anonymized)}
                                            required={anonymized_required}
                                            default_value={model.described_entity.lr_subclass.anonymized}
                                            field="anonymized"
                                            setSelectedvalue={this.selectedValue}
                                        />
                                    </div>
                                    {model.described_entity.lr_subclass.anonymized === "http://w3id.org/meta-share/meta-share/yesA" && <div className="pb-3" id={GenericSchemaParser.getFormElement("anonymization_details", subclass.anonymization_details).label}><LanguageSpecificText {...GenericSchemaParser.getFormElement("anonymization_details", subclass.anonymization_details)} defaultValueObj={model.described_entity.lr_subclass.anonymization_details || { "en": "" }} field="anonymization_details" multiline={true} maxRows={6} setLanguageSpecificText={this.setObjectGeneral} /></div>}

                                    <div className="pt-3 pb-3" id={GenericSchemaParser.getFormElement("additional_hw_requirements", subclass.additional_hw_requirements).label}>
                                        <FreeText
                                            className="wd-100"
                                            {...GenericSchemaParser.getFormElement("additional_hw_requirements", subclass.additional_hw_requirements)}
                                            initialValue={model.described_entity.lr_subclass.additional_hw_requirements}
                                            field="additional_hw_requirements"
                                            updateModel={this.setObjectGeneral}
                                        />
                                    </div>

                                    {/*<div className="pb-3"><FreeTextList className="wd-100" {...GenericSchemaParser.getFormElement("requires_software", subclass.requires_software)} default_value_Array={model.described_entity.lr_subclass.requires_software ? JSON.parse(JSON.stringify(model.described_entity.lr_subclass.requires_software)) : []} field="requires_software" updateModel_Array={this.updateModel_generic_array_Subclass} /></div>*/}
                                    {false && <div className="pb-3" id={GenericSchemaParser.getFormElement("required_hardware", subclass.required_hardware).label}>
                                        <AutocompleteChoicesChips className="wd-100"
                                            {...GenericSchemaParser.getFormElement("required_hardware", subclass.required_hardware)}
                                            initialValuesArray={model.described_entity.lr_subclass.required_hardware || []}
                                            field="required_hardware"
                                            lr_subclass={true}
                                            updateModel_array={this.updateModel_generic_array}
                                        />
                                    </div>}
                                    {/*<div className="pb-3"><LanguageSpecificText {...GenericSchemaParser.getFormElement("running_environment_details", subclass.running_environment_details)} defaultValueObj={model.described_entity.lr_subclass.running_environment_details || { "en": "" }} field="running_environment_details" lr_subclass={true} setLanguageSpecificText={this.setObjectSubclass} /></div>*/}

                                    {/* //NOT USED IN MINIMAL
                                            <div className="pb-3"> <ActualUseArray {...GenericSchemaParser.getFormElement("actual_use", data.actual_use)} initialValueArray={model.described_entity.actual_use || []} field="actual_use" updateModel_array={this.updateModel_generic_array} /></div>
                                            <div className="pb-3"> <ValidationArray {...GenericSchemaParser.getFormElement("validation", data.validation)} initialValueArray={model.described_entity.validation || []} field="validation" updateModel_array={this.updateModel_generic_array} /></div>
                                        */}
                                </Grid>
                            </Grid>
                        </VerticalTabPanel>

                    </div>
                </div>
            </form>

        </div>
    }
}