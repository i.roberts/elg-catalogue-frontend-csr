import React from "react";
import { withRouter,Redirect } from "react-router-dom";
import GoToCatalogue from "../componentsAPI/CommonComponents/GoToCatalogue";
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import { EDITOR_PERMITTED_ROLES, AUTHENTICATED_KEYCLOAK_USER_ROLES } from "../config/constants";

class EditorRoute extends React.Component {
    constructor(props) {
        super(props);
        this.state = { value: 0, keycloak: props.keycloak, UserRoles: [] };
    }

    componentDidMount() {
        const { keycloak } = this.props;
        this.setState({ UserRoles: AUTHENTICATED_KEYCLOAK_USER_ROLES(keycloak) });
    }

    isAuthorizedToView = (roles) => {
        var isAuthorized = false;
        EDITOR_PERMITTED_ROLES.forEach(item => {
            if (roles.includes(item)) {
                isAuthorized = true;
            }
        })
        return isAuthorized;
    }
    render() {
        if (!this.props.keycloak || !this.props.keycloak.authenticated) {
            return <Redirect to="/" />
        }
        const isAuth = this.isAuthorizedToView(this.state.UserRoles);
        if (!isAuth) {
            return <div>
                <div className="editor--header">
                    <Container maxWidth="xl"><Typography variant="h1" className="padding5 font-color-white">ELG EDITOR</Typography></Container>
                    <GoToCatalogue />
                </div>
                <Container style={{ padding: '0', maxWidth: "100%" }}>
                    <div>You do not have sufficient rights.</div>
                </Container>
            </div>
        }
        return (
            <div>
                {this.props.children}
            </div>
        )
    }
}
export default withRouter(EditorRoute);