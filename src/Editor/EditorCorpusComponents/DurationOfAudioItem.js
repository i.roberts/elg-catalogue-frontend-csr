import React from "react";
import RecordSelectList from "../editorCommonComponents/RecordSelectList";
import { TextField } from "@material-ui/core";
import Grid from '@material-ui/core/Grid';

export default class DurationOfAudioItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = { duration_of_audioItem: props.duration_of_audioItem }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            duration_of_audioItem: nextProps.duration_of_audioItem
        };
    }

    setNumber = (field, value) => {
        if (value.length === 0) {
            value = null;
        } else {
            value = Number(value);
        }
        const { duration_of_audioItem } = this.state;
        duration_of_audioItem[field] = value;
        this.setState({ duration_of_audioItem });
    }

    setSelectListValueDurationUnit = (event, selectedObjArray) => {
        const { duration_of_audioItem } = this.state;
        duration_of_audioItem.duration_unit = (selectedObjArray && selectedObjArray.length > 0) ? selectedObjArray[0].value : null;
        this.setState({ duration_of_audioItem }, this.onBlur);
    }

    onBlur = () => {
        const { duration_of_audioItem } = this.state;
        const exist_duration_unit = duration_of_audioItem.duration_unit ? true : false;
        const exist_amount = duration_of_audioItem.amount !== null ? true : false;
        if (exist_duration_unit === false && exist_amount === false) {
            duration_of_audioItem["editor-placeholder"] = true;
        } else {
            delete duration_of_audioItem["editor-placeholder"];
        }
        this.props.updateModel("setduration_of_audioItem", this.props.duration_of_audioItemIndex, duration_of_audioItem);
    }


    render() {
        const { duration_of_audioItem } = this.state;

        return <div onBlur={() => this.onBlur()}>
            <Grid container direction="row" alignItems="baseline" justifyContent="flex-start" spacing={1}>
                <Grid item xs={6}>
                    <TextField
                        className="wd-100"
                        variant="outlined"
                        type="number"
                        required={this.props.formElements.amount.required}
                        disabled={this.props.formElements.amount.read_only}
                        label={this.props.formElements.amount.label}
                        helperText={this.props.formElements.amount.help_text}
                        value={duration_of_audioItem.amount === null ? "" : duration_of_audioItem.amount}
                        onChange={(e) => { this.setNumber("amount", e.target.value) }}
                        inputProps={{ min: `${this.props.formElements.amount.min_value}`, max: `${this.props.formElements.amount.max_value}`, step: "1" }}
                    />
                </Grid>
                <Grid item xs={6}><RecordSelectList className="wd-100"{...this.props.formElements.duration_unit} choices={this.props.formElements.duration_unit.choices} default_value={duration_of_audioItem.duration_unit} setSelectedvalue={this.setSelectListValueDurationUnit} />
                </Grid>
            </Grid>
        </div>

    }



}