import React from "react";
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import RelatedLR from "../EditorGenericComponents/RelatedLR";
import Tooltip from '@material-ui/core/Tooltip';
import { resourceRelationsObj } from "../Models/LrModel";
import messages from "./../../config/messages";


export default class LRAutocomplete extends React.Component {
    constructor(props) {
        super(props);
        this.state = { initialValueArray: props.initialValueArray || [] };
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            initialValueArray: nextProps.initialValueArray || []
        };
    }

    setValues = (action, index) => {
        const { initialValueArray } = this.state;
        switch (action) {
            case "addArrayItem":
                let filteredArray = initialValueArray.filter(item => {
                    if (item && item.resource_name && item.resource_name.hasOwnProperty("en") && item.resource_name["en"]) {
                        return true;
                    }
                    return false;
                })
                if (filteredArray.length !== initialValueArray.length) {
                    return;//do not add element if there are empty ones
                }
                initialValueArray.push(JSON.parse(JSON.stringify(resourceRelationsObj)));
                //this.setState({ initialValueArray }); //this.props.updateModel_Array(this.props.field, initialValueArray);
                break;
            case "removeArrayItem":
                initialValueArray.splice(index, 1);
                if (initialValueArray.length === 0) {
                    this.props.updateModel_Array(this.props.field, []);
                    return;
                }
                this.props.updateModel_Array(this.props.field, initialValueArray);
                break;
            default:
                break;
        }
        this.setState({ initialValueArray });
    }

    updateModel = (index, value) => {
        const { initialValueArray } = this.state;
        initialValueArray[index] = value;
        this.setState({ initialValueArray }, this.onBlur);
        //this.props.updateModel_Array(this.props.field, this.state.initialValueArray);
    }

    onBlur = () => {
        this.props.updateModel_Array(this.props.field, this.state.initialValueArray);
    }

    render() {
        const { initialValueArray } = this.state;
        initialValueArray.length === 0 && initialValueArray.push(JSON.parse(JSON.stringify(resourceRelationsObj)));
        return <div onBlur={this.onBlur}>
            <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1} >
                <Grid item sm={12}>
                    <Typography variant="h3" className="section-links" >{this.props.label} </Typography>
                    <Typography className="section-links" >{this.props.help_text} </Typography>
                </Grid>
            </Grid>
            {
                initialValueArray.map((item, index) => {
                    return <div key={index}>

                        {(item.resource_name === null || (item.resource_name && item.resource_name["en"] !== "")) &&
                            <Grid container direction="row" justifyContent="flex-end" alignItems="baseline" >
                                <Grid item><Tooltip title={`${messages.array_elements_remove} ${this.props.label}`}><Button className="inner-link-default--purple" onClick={(e) => this.setValues("removeArrayItem", index)}>{messages.array_elements_remove}</Button></Tooltip></Grid>
                            </Grid>
                        }

                        {((item.resource_name === null || item.resource_name["en"] === "") && index !== 0) &&
                            <Grid container direction="row" justifyContent="flex-end" alignItems="baseline" >
                                <Grid item><Tooltip title={`${messages.array_elements_remove} ${this.props.label}`}><Button className="inner-link-default--purple" onClick={(e) => this.setValues("removeArrayItem", index)}>{messages.array_elements_remove}</Button></Tooltip></Grid>
                            </Grid>
                        }

                        <Grid container className="mb2" direction="row" justifyContent="space-between" alignItems="baseline" >
                            <Grid item xs={12}><RelatedLR index={index} field={this.props.field} formStuff={this.props.formElements} initialValue={item} updateModel={this.updateModel} /></Grid>
                        </Grid>

                        {((item.resource_name === null || (item.resource_name && item.resource_name["en"] !== "")) && ((initialValueArray.length - 1) === index)) ?
                            <Grid container className="pt1" direction="row" justifyContent="flex-start" alignItems="baseline" >
                                <Grid item><Tooltip title={`${messages.array_elements_add} ${this.props.label}`}><Button className="inner-link-default--purple" onClick={(e) => this.setValues("addArrayItem", index)}>{messages.array_elements_add}</Button></Tooltip></Grid>
                            </Grid>
                            : <span></span>
                        }



                    </div>
                })
            }
        </div>
    }
}