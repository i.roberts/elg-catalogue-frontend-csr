import React from "react";
import axios from "axios";
import validator from 'validator';
import messages from "./../../config/messages";
import { LOOK_UP_LR } from "../../config/editorConstants";
import GenericSchemaParser from "../../parsers/GenericSchemaParser";
import LanguageSpecificText from "../editorCommonComponents/LanguageSpecificText";
import LRIdentifier from "../EditorServiceToolComponents/LRIdentifier";
import { resourceRelationsObj } from "../Models/LrModel";
import { lr_identifier } from "../Models/LrModel";
import { TextField } from "@material-ui/core";
//import Autocomplete, { createFilterOptions } from '@material-ui/lab/Autocomplete';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CircularProgress from "@material-ui/core/CircularProgress";
import parse from 'autosuggest-highlight/parse';
import match from 'autosuggest-highlight/match';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
//const filter = createFilterOptions();

const checkElg = obj => obj.lr_identifier_scheme === "http://w3id.org/meta-share/meta-share/elg";

export default class LRAutocompleteSingle extends React.Component {
    constructor(props) {
        super(props);
        this.state = { initialValue: props.initialValue || JSON.parse(JSON.stringify(resourceRelationsObj)), options: [], loading: false, VersionError: false, source: null };
        //this.isUnmound = false;
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            initialValue: nextProps.initialValue || JSON.parse(JSON.stringify(resourceRelationsObj))
        };
    }

    componentWillUnmount() {
        if (this.state.source) {
            if (this.state.source) {
                this.state.source.cancel("");
            }
        }
    }

    getLrChoices = (field, value) => {
        if (value.trim().length <= 2) {
            //this.setState({ options: []});
            return;
        }
        if (this.state.source) {
            this.state.source.cancel("");
        }
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        this.setState({ loading: true, source: source });
        axios.get(LOOK_UP_LR(value), { cancelToken: source.token }).then(res => {
            this.setState({ options: res.data.all, loading: false, source: null })//careful, this endpoint returns object "all"
        }).catch((err) => {
            console.log(err);
            this.setState({ loading: false, source: null, options: [] })
        });

    }

    setValues = (action, value) => {
        let { initialValue } = this.state;
        switch (action) {
            case "autoCompleteSelected":
                if (value.similarity) {
                    delete value["similarity"];
                }
                if (value.display_name) {
                    delete value["display_name"];
                }
                if (value.hasOwnProperty('editor-placeholder')) {
                    delete value['editor-placeholder'];
                }
                initialValue = value;
                this.setState({ initialValue, options: [] });
                this.props.updateModel(this.props.index, initialValue);
                break;
            case "removeItem":
                initialValue = null;
                this.setState({ initialValue, options: [] });
                this.props.updateModel(this.props.field, initialValue);
                break;
            default: break;
        }
        this.setState({ initialValue, options: [] });
    }

    autocompleteTextField = (field) => {
        const activeLanguage = this.props.activeLanguage || "en";
        const scheme_choices = this.props.formStuff.lr_identifier.formElements.lr_identifier_scheme.choices;
        return <span>
            <Autocomplete className={"wd-100"}
                freeSolo
                clearOnBlur
                autoHighlight
                value={this.state.initialValue}
                onChange={(event, newValue) => {
                    if (!newValue) {
                        return;
                    }
                    if (typeof newValue === "object") {
                        this.setValues("autoCompleteSelected", newValue);
                    } else {
                        this.setObjectGeneral(field, { "en": newValue });
                    }
                }}
                options={this.state.options}
                loading={this.state.loading}
                filterOptions={(options, params) => {
                    //const filtered = filter(options, params);
                    const filtered = options;
                    if (!this.state.loading) {
                        if (params.inputValue !== '') {
                            /*filtered.push({
                                ...JSON.parse(JSON.stringify(resourceRelationsObj)),
                                [field]: { 'en': params.inputValue },
                                display_name: `Missing ${params.inputValue} ? Add "${params.inputValue}"`,
                            });*/
                            filtered.splice(0, 0, {
                                ...JSON.parse(JSON.stringify(resourceRelationsObj)),
                                [field]: { 'en': params.inputValue },
                                display_name: `Missing ${params.inputValue} ? Add "${params.inputValue}"`,
                            });
                        }
                    }
                    if (filtered && filtered.length === 0 && this.state.loading) {
                        return [{ display_name: "Loading please wait...", }]
                    }
                    return filtered;
                }}
                //getOptionLabel={(option) => option.resource_name ? Object.values(option[field])[0] : ""}
                getOptionLabel={(option) => option.resource_name ? Object.keys(option.resource_name).includes("en") ? option.resource_name["en"] : Object.values(option.resource_name)[0] : ""}
                renderInput={(params) => (
                    <TextField {...params} label={this.props.formStuff[field].label} helperText={this.props.formStuff[field].help_text} variant="outlined"
                        placeholder={this.props.formStuff[field].placeholder || ""}
                        //onBlur={(e) => { this.setObjectGeneral(field, { "en": e.target.value }); }}
                        onChange={(e) => { this.getLrChoices(field, e.target.value) }}
                        InputProps={{
                            ...params.InputProps,
                            endAdornment: (
                                <React.Fragment>
                                    {this.state.loading ? (
                                        <CircularProgress color="inherit" size={20} />
                                    ) : null}
                                    {params.InputProps.endAdornment}
                                </React.Fragment>
                            )
                        }}
                    />)}
                renderOption={(option, { inputValue }) => {
                    if (option.display_name) {
                        const matches1 = match(option.display_name, inputValue);
                        const parts1 = parse(option.display_name, matches1);
                        return parts1.map((part, index) => (
                            <span key={index} style={{ fontWeight: part.highlight ? 700 : 400 }}>
                                {part.highlight ? '\u00A0' : ''}{part.text}
                            </span>
                        ))
                    }
                    //const matches = match(Object.values(option[field])[0], inputValue);
                    //const parts = parse(Object.values(option[field])[0], matches);
                    const matches = match(option[field][activeLanguage] || Object.values(option[field])[0], inputValue);
                    const parts = parse(option[field][activeLanguage] || Object.values(option[field])[0], matches);
                    return (
                        <div>
                            {
                                option[field] && <div>
                                    {parts.map((part, index) => (
                                        <span key={index} style={{ fontWeight: part.highlight ? 700 : 400 }}>
                                            {part.text}
                                        </span>
                                    ))}

                                </div>
                            }
                            {
                                option.lr_identifier && option.lr_identifier.map((item, index) => {
                                    return <div key={index}>
                                        {item.lr_identifier_scheme === "http://w3id.org/meta-share/meta-share/elg" ? "" :
                                            (`${item.value} - ${scheme_choices.filter(schemeChoice => schemeChoice.value === item.lr_identifier_scheme)[0].display_name}`)
                                        }
                                    </div>
                                })
                            }
                            {
                                option.version &&
                                <div >
                                    <span className="info_url">
                                        {<span>{option.version}</span>}
                                    </span>
                                </div>
                            }



                        </div>
                    );
                }
                }
            />
        </span>
    }

    setObjectGeneral = (field, valueObj) => {
        if (!valueObj) {
            return;
        }
        const { initialValue } = this.state;
        initialValue[field] = valueObj;
        if (initialValue.hasOwnProperty('editor-placeholder')) {
            delete initialValue['editor-placeholder'];
        }
        this.setState({ initialValue, options: [] });
    }

    updateModel_generic_array = (obj2update, valueArray) => {
        const { initialValue } = this.state;
        if (!initialValue[obj2update]) {
            initialValue[obj2update] = [];
        }
        initialValue[obj2update] = valueArray;
        if (initialValue.hasOwnProperty('editor-placeholder')) {
            delete initialValue['editor-placeholder'];
        }
        this.setState({ initialValue, options: [] }, this.onBlur);
    }

    onBlur = () => {
        const resource = this.state.initialValue;
        const exists_resource_name = (resource && resource.resource_name && resource.resource_name.hasOwnProperty("en") && resource.resource_name["en"]) ? true : false;
        const exists_identifier = (resource && resource.lr_identifier && resource.lr_identifier.length > 0) ? true : false;
        const exists_version = (resource && resource.version) ? true : false;
        if (exists_resource_name === false && exists_identifier === false && exists_version === false) {
            resource["editor-placeholder"] = true;
        } else {
            delete resource['editor-placeholder'];
        }
        this.setState({ options: [], loading: false });
        this.props.updateModel(this.props.index, resource);
    }

    ///TODO validation: string with max_length
    setGrantnumber = (event, index) => {
        const { initialValue } = this.state;
        if (!initialValue.version) {
            initialValue.version = "";
        }
        initialValue.version = event.target.value;
        this.setState({ initialValue });
    }
    //////  
    blur = (event, max_length) => {
        const valid = validator.isLength(event.target.value, { max: max_length });
        this.setState({ VersionError: !valid });
    }



    render() {
        const { VersionError, initialValue } = this.state;
        //console.log(this.props.formStuff)
        const { help_text: version_help_text, label: version_label, required: version_required, type: version_type, placeholder: version_placeholder = "", max_length: version_max_length } = GenericSchemaParser.getFormElement("version", this.props.formStuff.version);
        const version_default_value = initialValue.version || "";
        const VersionValidationError = "Value should be " + version_max_length + " characters length";
        const hide_help_text = initialValue.hasOwnProperty("pk") ? true : false;
        return <div onBlur={this.onBlur}>
            <Typography variant="h3" className="section-links" >{this.props.label} </Typography>
            <Typography className="section-links" >{this.props.help_text} </Typography>

            {!initialValue.hasOwnProperty("version") && ((Object.keys(initialValue).length === 0) || (initialValue.resource_name && !initialValue.resource_name["en"])) ?
                <div>
                    <div>{this.autocompleteTextField("resource_name")}</div>
                </div>
                :
                <div className="pb-3 inner--group nested--group">
                    <div onBlur={this.onBlur}>
                        <LanguageSpecificText
                            {...GenericSchemaParser.getFormElement("resource_name", this.props.formStuff.resource_name)}
                            help_text={hide_help_text ? '' : GenericSchemaParser.getFormElement("resource_name", this.props.formStuff.resource_name).help_text}
                            defaultValueObj={initialValue.resource_name || { "en": "" }}
                            field="resource_name"
                            disable={initialValue.pk >= 0 ? true : false}
                            setLanguageSpecificText={this.setObjectGeneral}
                        />
                    </div>

                    {initialValue.pk && ((initialValue.lr_identifier.length === 1 && initialValue.lr_identifier.some(checkElg)))
                        ?
                        (<></>)
                        :
                        (
                            <div className="inner--group nested--group">
                                <LRIdentifier
                                    key={JSON.stringify(initialValue) + 'generic_lr' + this.props.index}
                                    {...GenericSchemaParser.getFormElement("lr_identifier", this.props.formStuff.lr_identifier)}
                                    identifier_scheme="lr_identifier_scheme"
                                    scheme_choices={this.props.formStuff.lr_identifier.formElements.lr_identifier_scheme.choices}
                                    identifier_scheme_required={this.props.formStuff.lr_identifier.formElements.lr_identifier_scheme.required}
                                    identifier_scheme_label={this.props.formStuff.lr_identifier.formElements.lr_identifier_scheme.label}
                                    identifier_scheme_help_text={hide_help_text ? '' : this.props.formStuff.lr_identifier.formElements.lr_identifier_scheme.help_text}
                                    identifier_value_required={this.props.formStuff.lr_identifier.formElements.value.required}
                                    identifier_value_label={this.props.formStuff.lr_identifier.formElements.value.label}
                                    identifier_value_placeholder={hide_help_text ? '' : this.props.formStuff.lr_identifier.formElements.value.placeholder}
                                    identifier_value_help_text={hide_help_text ? '' : this.props.formStuff.lr_identifier.formElements.value.help_text}
                                    identifier_obj={JSON.parse(JSON.stringify(lr_identifier))}
                                    className="wd-100"
                                    default_valueArray={initialValue.lr_identifier}
                                    field="lr_identifier"
                                    disable={initialValue.pk ? true : false}
                                    updateModel_Identifier={this.updateModel_generic_array}
                                /> </div>
                        )

                    }



                    <div className="pt-3 pb-3">
                        <TextField className="wd-100" type={version_type} required={version_required} label={version_label}
                            helperText={hide_help_text ? '' : (VersionError ? `${VersionValidationError}` : version_help_text)} variant="outlined" value={version_default_value}
                            placeholder={version_placeholder || ""}
                            inputProps={{ name: 'version', maxLength: version_max_length }} onChange={(e) => this.setGrantnumber(e)}
                            onBlur={(e) => this.blur(e, version_max_length)}
                            error={VersionError}
                            disabled={initialValue.pk ? true : false} />
                    </div>

                    {!this.props.hideRemoveButton && <Button className="inner-link-default--purple" onClick={(e) => this.setValues("removeItem")}>{messages.array_elements_remove}</Button>}

                </div>
            }
        </div>
    }
}