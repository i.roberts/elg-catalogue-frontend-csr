import React from "react";
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
//import { ReactComponent as AddCircleOutlineIcon } from "./../../assets/elg-icons/editor/add-square.svg";
//import { ReactComponent as RemoveCircleOutlineIcon } from "./../../assets/elg-icons/editor/remove-square.svg";
import { datasetDistributionObj } from "../Models/CorpusModel";
import DatasetDistributionItem from "./DatasetDistributionItem";
import Tooltip from '@material-ui/core/Tooltip';
import messages from "./../../config/messages";
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

export default class DatasetDistributionArray extends React.Component {
    constructor(props) {
        super(props);
        this.state = { datasetDistributionArray: props.initialValueArray || [], expanded: { index: 0, isExpanded: true } }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            datasetDistributionArray: nextProps.initialValueArray || [],
        };
    }

    setValues = (action, datasetDistributionItemIndex, value) => {
        const { datasetDistributionArray } = this.state;
        switch (action) {
            case "addDatasetDistributionItem":
                datasetDistributionArray.push(JSON.parse(JSON.stringify(datasetDistributionObj)));
                this.setState({ expanded: { index: datasetDistributionArray.length - 1, isExpanded: true } });
                break;
            case "removeDatasetDistributionItem":
                datasetDistributionArray.splice(datasetDistributionItemIndex, 1);
                break;
            case "setDatasetDistributionItem":
                datasetDistributionArray[datasetDistributionItemIndex] = value;
                this.props.updateModel_array(this.props.field, datasetDistributionArray, this.props.lr_subclass);
                break;
            default: break;
        }
        this.setState({ datasetDistributionArray }, this.onBlur())
    }

    removeWarning = (e, index) => {
        e.stopPropagation();
        confirmAlert({
            message: messages.editor_remove_distribution(index),
            buttons: [
                {
                    label: 'Yes',
                    onClick: () => this.setValues("removeDatasetDistributionItem", index)
                },
                {
                    label: 'No',
                    onClick: () => { }
                }
            ]
        });
    }


    onBlur = () => {
        this.props.updateModel_array(this.props.field, this.state.datasetDistributionArray, this.props.lr_subclass);
    }

    render() {
        const { datasetDistributionArray } = this.state;
        datasetDistributionArray.length === 0 && datasetDistributionArray.push(JSON.parse(JSON.stringify(datasetDistributionObj)));
        return <div onBlur={this.onBlur}>

            {
                datasetDistributionArray.map((datasetDistributionItem, datasetDistributionItemIndex) => {
                    return <div key={datasetDistributionItemIndex} onBlur={() => this.onBlur()}>
                        <Accordion className="FunctionBox--accordions--editor" expanded={this.state.expanded.index === datasetDistributionItemIndex && this.state.expanded.isExpanded} onChange={() => {
                            if (this.state.expanded.index === datasetDistributionItemIndex) {
                                this.setState({ expanded: { index: datasetDistributionItemIndex, isExpanded: !this.state.expanded.isExpanded } }, this.onBlur)
                            } else {
                                this.setState({ expanded: { index: datasetDistributionItemIndex, isExpanded: true } }, this.onBlur)
                            }
                        }
                        }>
                            <AccordionSummary expandIcon={<ExpandMoreIcon className="purple--font" />} aria-controls="panel1a-content" id="panel1a-header" className="FunctionBox--accordions__editortitle">
                                <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1} >
                                    <Grid item sm={9}>
                                        <Typography variant="h3" className="section-links" >{this.props.label} {datasetDistributionItemIndex + 1}</Typography>
                                        <Typography className="section-links" >{this.props.help_text} </Typography>
                                    </Grid>
                                    {(datasetDistributionItem.dataset_distribution_form !== "" || datasetDistributionItemIndex > 0) &&
                                        <Grid item sm={3}><Tooltip title={`${messages.array_elements_remove} ${this.props.label}`}><Button className="inner-link-default--purple" onClick={(e) => this.removeWarning(e, datasetDistributionItemIndex)}>{messages.array_elements_remove} {` ${this.props.label}`} {datasetDistributionItemIndex ? datasetDistributionItemIndex + 1 : ""}</Button></Tooltip></Grid>
                                    }
                                </Grid>
                            </AccordionSummary>
                            <AccordionDetails>
                                <DatasetDistributionItem
                                    // key={JSON.stringify(datasetDistributionItem) + "_distribution_item_" + datasetDistributionItemIndex}
                                    {...this.props} datasetDistributionItem={datasetDistributionItem} datasetDistributionItemIndex={datasetDistributionItemIndex} setValues={this.setValues} />
                            </AccordionDetails>
                        </Accordion>
                        <div className="mb2 pt-3">
                            {/*datasetDistributionItem.dataset_distribution_form && <Tooltip title={`${messages.array_elements_remove} ${this.props.label}`}><Button className="inner-link-default--purple" onClick={(e) => this.setValues("removeDatasetDistributionItem", datasetDistributionItemIndex)}>{messages.array_elements_remove}</Button></Tooltip>*/}
                            {(datasetDistributionItem.dataset_distribution_form !== "" && ((datasetDistributionArray.length - 1) === datasetDistributionItemIndex)) ?
                                <Grid container className="pb1" direction="row" justifyContent="flex-start" alignItems="baseline" >
                                    <Grid item>
                                        <Tooltip title={`${messages.array_elements_add} ${this.props.label}`}><Button className="inner-link-default--purple" onClick={(e) => this.setValues("addDatasetDistributionItem", datasetDistributionItemIndex)}>{messages.array_elements_add}</Button></Tooltip>
                                    </Grid>
                                </Grid>
                                : <span></span>
                            }
                        </div>
                    </div>
                })
            }
        </div>
    }
}