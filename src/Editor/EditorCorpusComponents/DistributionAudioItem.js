import React from "react";
import AutocompleteRecommendedChoices from "../editorCommonComponents/AutocompleteRecommendedChoices";
import SizeArray from "./SizeArray";
import AudioFormatArray from "./AudioFormatArray";
import DurationOfAudioArray from "./DurationOfAudioArray";

export default class DistributionAudioItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = { distribution_audio_featureItem: props.distribution_audio_featureItem }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            distribution_audio_featureItem: nextProps.distribution_audio_featureItem
        };
    }

    updateModel__array = (obj2update, valueArray) => {
        const { distribution_audio_featureItem } = this.state;
        if (!distribution_audio_featureItem[obj2update]) {
            distribution_audio_featureItem[obj2update] = [];
        }
        distribution_audio_featureItem[obj2update] = valueArray;
        this.setState({ distribution_audio_featureItem }, this.onBlur());
    }


    onBlur = () => {
        const { distribution_audio_featureItem } = this.state
        const exists_duration_of_audio = (distribution_audio_featureItem.duration_of_audio && distribution_audio_featureItem.duration_of_audio.filter(item => !item.hasOwnProperty("editor-placeholder")).length) ? true : false;
        const exists_size = (distribution_audio_featureItem.size && distribution_audio_featureItem.size.filter(item => {
            if ((item.amount !== null && item.amount >= 0) ||
                item.size_unit ||
                (item.language && item.language.length) ||// needs change for full schema
                (item.domain && item.domain.length) ||// needs change for full schema
                (item.text_genre && item.text_genre.length) ||// needs change for full schema
                (item.audio_genre && item.audio_genre.length) ||// needs change for full schema
                (item.speech_genre && item.speech_genre.length) ||// needs change for full schema
                (item.image_genre && item.image_genre.length)// needs change for full schema
            ) {
                return true;
            } else { return false; }
        }
        ).length) ? true : false;
        const exists_data_format = (distribution_audio_featureItem.data_format && distribution_audio_featureItem.data_format.length) ? true : false;
        const exists_audio_format = (distribution_audio_featureItem.audio_format && distribution_audio_featureItem.audio_format.length) ? true : false;//needs change for full schema
        const exists_duration_of_effective_speech = (distribution_audio_featureItem.duration_of_effective_speech && distribution_audio_featureItem.duration_of_effective_speech.length) ? true : false;//needs change for full schema
        if (exists_size === false && exists_duration_of_audio === false && exists_data_format === false && exists_audio_format === false && exists_duration_of_effective_speech === false) {
            distribution_audio_featureItem["editor-placeholder"] = true;
        } else {
            delete distribution_audio_featureItem["editor-placeholder"];
        }
        this.props.updateModel("setdistribution_audio_featureItem", this.props.distribution_audio_featureItemIndex, distribution_audio_featureItem);
    }


    render() {
        const { distribution_audio_featureItem } = this.state;
        return <>
            <div className="pb-3"> <SizeArray {...this.props} {...this.props.formElements.size} initialValueArray={distribution_audio_featureItem.size || []} field="size" updateModel_array={this.updateModel__array} /></div>
            <div className="pb-3"> <AudioFormatArray {...this.props.formElements.audio_format} initialValueArray={distribution_audio_featureItem.audio_format || []} field="audio_format" updateModel_array={this.updateModel__array} /></div>
            <div className="pb-3"> <DurationOfAudioArray {...this.props.formElements.duration_of_audio} initialValueArray={distribution_audio_featureItem.duration_of_audio || []} field="duration_of_audio" updateModel_array={this.updateModel__array} /></div>
            <div className="pb-3"><AutocompleteRecommendedChoices className="wd-100" {...this.props.formElements.data_format} recommended_choices={this.props.formElements.data_format.choices} initialValuesArray={distribution_audio_featureItem.data_format || []} field="data_format" updateModel_array={this.updateModel__array} /></div>
            {/*to do duration_of_effective_speech*/}
        </>

    }



}