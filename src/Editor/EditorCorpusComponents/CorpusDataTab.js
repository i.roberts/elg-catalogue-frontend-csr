import React from "react";
//import axios from "axios";
//import ProgressBar from "../../componentsAPI/CommonComponents/ProgressBar";
//import Container from '@material-ui/core/Container';
//import GenericSchemaParser from "../../parsers/GenericSchemaParser";
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import VerticalTabPanel from '../../componentsAPI/CustomVerticalTabs/VerticalTabPanel';
import Grid from '@material-ui/core/Grid';
//import Button from '@material-ui/core/Button';
//import { CORPUS_FOURTH_SECTION_TABS_HEADERS } from "../../config/editorConstants";
import { switchIcon } from "../../config/editorConstants";
//import { Typography } from "@material-ui/core";
import DataComponent from "../editorCommonComponents/DataComponent";



function a11yProps(index) {
    return {
        id: `vertical-tab-${index}`,
        'aria-controls': `vertical-tabpanel-${index}`,
    };
}


export default class DataTab extends React.Component {
    constructor(props) {
        super(props);
        this.state = { tab: 0 };
    }

    toggleTab = (tabIndex) => {
        this.setState({ tab: tabIndex });
    }

    render() {

        //const data = this.props.schema;
        //const { model } = this.props;

        const DATA_TAB_HEADERS = ["DATA"];
        return <div>
            <form >
                <div className="tabs-main-container">
                    <div className="vertical-tabs-container-forms">
                        <Tabs value={this.state.tab} onChange={this.toggleTab} variant="scrollable" orientation="vertical" aria-label="vertical tabs example" className="vertical-tabs-forms">
                            {DATA_TAB_HEADERS.map((tab, index) => <Tab key={index} label={<><span>{switchIcon(tab)}</span><span> {tab} </span> </>}  {...a11yProps(index)} onClick={() => { this.toggleTab(index); }} />)}
                        </Tabs>
                        <VerticalTabPanel value={this.state.tab} index={0} className="vertical-tab-pannel">
                            <Grid container direction="row" alignItems="flex-start" spacing={2} className="vertical-tabs-inner">
                                <Grid item xs>
                                    <div className="pb-3 wd-100">
                                        <DataComponent {...this.props} />
                                    </div>
                                </Grid>
                            </Grid>
                        </VerticalTabPanel>
                    </div>
                </div>
            </form>
        </div >
    }
}