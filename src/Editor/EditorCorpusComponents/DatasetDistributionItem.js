import React from "react";
import Website from "../editorCommonComponents/Website";
import WebsiteList from "../editorCommonComponents/WebsiteListWithUpload";
import RecordSelectList from "../editorCommonComponents/RecordSelectList";
//import Typography from '@material-ui/core/Typography';
//import Grid from '@material-ui/core/Grid';
//import FreeText from "../editorCommonComponents/FreeText";
//import LanguageSpecificText from "../editorCommonComponents/LanguageSpecificText";
import AutocompleteChoicesChips from "../editorCommonComponents/AutocompleteChoicesChips";
////import DateComponent from "../editorCommonComponents/DateComponent";
//import IsDescribedBy from "./IsDescribedBy";
//import IsDescribedByAutocomplete from "./IsDescribedByAutocomplete";
import LicenceTerms from "../EditorServiceToolComponents/LicenceTerms";
//import DistributionRightsHolderAutocomplete from "../EditorServiceToolComponents/DistributionRightsHolderAutocomplete";
import Cost from "../editorCommonComponents/Cost";
import DistributionTextArray from "./DistributionTextArray";
import DistributionVideoArray from "./DistributionVideoArray";
import DistributionImageArray from "./DistributionImageArray";
import DistributionAudioArray from "./DistributionAudioArray";
import DistributionTextNumericalArray from "./DistributionTextNumericalArray";
//import DistributionUnspecifiedArray from "./DistributionUnspecifiedArray";
import DistributionUnspecifiedItem from "./DistributionUnspecifiedItem";
import AccessRightsStatementAutocomplete from "../editorCommonComponents/AccessRightsStatementAutocomplete";
import LRAutocomplete from "./LRAutocomplete";
import RecordRadioBoolean from "../editorCommonComponents/RecordRadioBoolean";
import SelectDataset from "../editorCommonComponents/SelectDataset";
import { distribution_unspecified_featureObj } from "../Models/CorpusModel";
import {
    CD_ROM, DVD_R, ACCESSIBLE_INTERFACE, ACCESSIBLE_QUERY, BLURAY, DOWNLOADABLE, HARD_DISK, OTHER,
    PAPER_COPY,
    UNSPECIFIED
} from "../Models/CorpusModel";
import { SUBCLASS_TYPE_MODEL } from "../../config/editorConstants";

export default class DatasetDistributionItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = { datasetDistributionItem: props.datasetDistributionItem }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            datasetDistributionItem: nextProps.datasetDistributionItem
        };
    }

    setDistributionForm = (event, selectedObjArray) => {
        const { datasetDistributionItem } = this.state;
        datasetDistributionItem.dataset_distribution_form = selectedObjArray && selectedObjArray.length > 0 ? selectedObjArray[0].value : null;
        this.setState({ datasetDistributionItem }, this.onBlur);
    }

    setPrivateResource = (e) => {
        const { datasetDistributionItem } = this.state;
        if (datasetDistributionItem.private_resource === null || datasetDistributionItem.private_resource === undefined) {
            datasetDistributionItem.private_resource = null;
        }
        datasetDistributionItem.private_resource = e.target.value === "true";
        this.setState({ datasetDistributionItem }, this.onBlur);
    }

    setDataset = (selectedObjArray) => {
        const { datasetDistributionItem } = this.state;
        datasetDistributionItem.dataset = selectedObjArray && selectedObjArray.length > 0 ? selectedObjArray[0] : null;
        this.setState({ datasetDistributionItem }, this.onBlur);
    }

    setObjectGeneral = (field, valueObj) => {
        const { datasetDistributionItem } = this.state;
        datasetDistributionItem[field] = valueObj;
        this.setState({ datasetDistributionItem }, this.onBlur);
    }

    updateModel__array = (obj2update, valueArray) => {
        const { datasetDistributionItem } = this.state;
        if (!datasetDistributionItem[obj2update]) {
            datasetDistributionItem[obj2update] = [];
        }
        datasetDistributionItem[obj2update] = valueArray;
        this.setState({ datasetDistributionItem }, this.onBlur);
    }

    setAccessRights = (field, valueObj) => {
        const { datasetDistributionItem } = this.state;
        datasetDistributionItem[field] = valueObj ? valueObj : [];
        this.setState({ datasetDistributionItem }, this.onBlur);
    }

    onBlur = () => {
        this.props.setValues("setDatasetDistributionItem", this.props.datasetDistributionItemIndex, this.state.datasetDistributionItem);
    }

    show_download_location = () => {
        const { datasetDistributionItem } = this.state;
        let show_download_loaction = false;
        if (datasetDistributionItem.dataset_distribution_form === DOWNLOADABLE) {
            show_download_loaction = true;
        }
        return show_download_loaction;
    }

    show_access_loaction = () => {
        const { datasetDistributionItem } = this.state;
        let show_access_loaction = false;
        if ([ACCESSIBLE_INTERFACE, ACCESSIBLE_QUERY, DOWNLOADABLE, OTHER, UNSPECIFIED, BLURAY, CD_ROM, HARD_DISK, DVD_R].includes(datasetDistributionItem.dataset_distribution_form)) {
            show_access_loaction = true;
        }
        return show_access_loaction;
    }

    req_access_location = () => {
        const { datasetDistributionItem } = this.state;
        let req_access_location = false;
        if ([ACCESSIBLE_INTERFACE, ACCESSIBLE_QUERY, OTHER, UNSPECIFIED, BLURAY, CD_ROM, HARD_DISK, DVD_R].includes(datasetDistributionItem.dataset_distribution_form)) {
            req_access_location = true;
        }
        return req_access_location;
    }

    render() {
        const for_info = false;
        const { datasetDistributionItem } = this.state;
        const { media_typeText, media_typeTextNumerical, media_typeImage, media_typeVideo, media_typeAudio } = this.props;

        const show_download_loaction = this.show_download_location();
        show_download_loaction ? void 0 : datasetDistributionItem["download_location"] = "";

        const show_access_loaction = this.show_access_loaction();
        const req_access_location = this.req_access_location();
        show_access_loaction ? void 0 : datasetDistributionItem["access_location"] = "";

        if ([CD_ROM, DVD_R, OTHER, ACCESSIBLE_INTERFACE, ACCESSIBLE_QUERY, BLURAY, HARD_DISK, OTHER, UNSPECIFIED].includes(datasetDistributionItem.dataset_distribution_form)) {
            datasetDistributionItem.dataset = null;
        }
        return <div onBlur={this.onBlur}>
            <div className="pt-3" ><RecordSelectList className="wd-100" {...this.props.formElements.dataset_distribution_form} choices={this.props.formElements.dataset_distribution_form.choices.filter(item => (this.props.model.described_entity.lr_subclass.lr_type === "LexicalConceptualResource" ? item.value !== PAPER_COPY : true))} default_value={datasetDistributionItem.dataset_distribution_form || ""} setSelectedvalue={this.setDistributionForm} /></div>
            <div className="pt-3 pb-3"><RecordRadioBoolean className="wd-100"
                required={false}
                label={this.props.formElements.private_resource.label}
                help_text={this.props.formElements.private_resource.help_text}
                default_value={datasetDistributionItem.private_resource} handleBooleanChange={this.setPrivateResource} />
            </div>
            {this.props.datasets && this.props.datasets.length > 0 && datasetDistributionItem.dataset_distribution_form === DOWNLOADABLE && <div className="pt-3" ><SelectDataset className="wd-100" datasets={this.props.datasets} initialValue={datasetDistributionItem.dataset} setSelectedvalue={this.setDataset} /></div>}
            {show_download_loaction && <div className="pt-3"><Website key={datasetDistributionItem.download_location + "_download_location_" + this.props.datasetDistributionItemIndex} className="wd-100"  {...this.props.formElements.download_location} website={datasetDistributionItem.download_location || ""} field="download_location" updateModel_website={this.setObjectGeneral} /></div>}
            {show_access_loaction && <div className="pt-3"><Website
                key={datasetDistributionItem.access_location + "_access_location_" + this.props.datasetDistributionItemIndex}
                label={this.props.formElements.access_location.label}
                type={this.props.formElements.access_location.type}
                help_text={this.props.formElements.access_location.help_text}
                className="wd-100" website={datasetDistributionItem.access_location || ""}
                required={req_access_location}
                field="access_location"
                updateModel_website={this.setObjectGeneral} />
            </div>}
            <div className="pt-3"><WebsiteList key={datasetDistributionItem.samples_location + "_samples_location_" + this.props.datasetDistributionItemIndex} className="wd-100" model={this.props.model} {...this.props.formElements.samples_location} default_value_Array={datasetDistributionItem.samples_location || []} field="samples_location" updateModel_website={this.updateModel__array} /></div>

            {/*<div className="pt-3"><LanguageSpecificText {...this.props.formElements.attribution_text} defaultValueObj={datasetDistributionItem.attribution_text} field="attribution_text" setLanguageSpecificText={this.setObjectGeneral} /></div>
            <div className="pt-3"><LanguageSpecificText {...this.props.formElements.copyright_statement} defaultValueObj={datasetDistributionItem.copyright_statement} field="copyright_statement" setLanguageSpecificText={this.setObjectGeneral} /></div>
            <div className="pt-3"><DateComponent className="wd-100" {...this.props.formElements.availability_start_date} initialValue={datasetDistributionItem.availability_start_date} field="availability_start_date" updateModel={this.setString} /></div>
            <div className="pt-3"><DateComponent className="wd-100" {...this.props.formElements.availability_end_date} initialValue={datasetDistributionItem.availability_end_date} field="availability_end_date" updateModel={this.setString} /></div>*/}
            {/*<div className="pt-3"><IsDescribedBy className="wd-100" {...this.props} index={this.props.datasetDistributionItemIndex} {...this.props.formElements.is_described_by} initialValueArray={datasetDistributionItem.is_described_by || []} field="is_described_by" updateModel_Array={this.updateModel__array} /></div>*/}
            {/*showItem_access_location && <div className="pt-3"><LRAutocomplete {...this.props.formElements.is_accessed_by} initialValueArray={datasetDistributionItem.is_accessed_by || []} field="is_accessed_by" updateModel_Array={this.setObjectGeneral} /> </div>}
            {showItem_access_location && <div className="pt-3"><LRAutocomplete {...this.props.formElements.is_displayed_by} initialValueArray={datasetDistributionItem.is_displayed_by || []} field="is_displayed_by" updateModel_Array={this.setObjectGeneral} /> </div>}
            {showItem_access_location && <div className="pt-3"><LRAutocomplete {...this.props.formElements.is_queried_by} initialValueArray={datasetDistributionItem.is_queried_by || []} field="is_queried_by" updateModel_Array={this.setObjectGeneral} /> </div>*/}

            {[ACCESSIBLE_QUERY, ACCESSIBLE_INTERFACE].includes(datasetDistributionItem.dataset_distribution_form) && <div className="pt-3"><LRAutocomplete {...this.props.formElements.is_queried_by} initialValueArray={datasetDistributionItem.is_queried_by || []} field="is_queried_by" updateModel_Array={this.setObjectGeneral} /> </div>}

            {media_typeText && <div className="pt-3"><DistributionTextArray {...this.props} datasetDistributionItemIndex={this.props.datasetDistributionItemIndex} {...this.props.formElements.distribution_text_feature} initialValueArray={datasetDistributionItem.distribution_text_feature || []} field="distribution_text_feature" updateModel_array={this.updateModel__array} /></div>}
            {media_typeVideo && <div className="pt-3"><DistributionVideoArray {...this.props} datasetDistributionItemIndex={this.props.datasetDistributionItemIndex} {...this.props.formElements.distribution_video_feature} initialValueArray={datasetDistributionItem.distribution_video_feature || []} field="distribution_video_feature" updateModel_array={this.updateModel__array} /></div>}
            {media_typeTextNumerical && <div className="pt-3"><DistributionTextNumericalArray datasetDistributionItemIndex={this.props.datasetDistributionItemIndex} {...this.props.formElements.distribution_text_numerical_feature} initialValueArray={datasetDistributionItem.distribution_text_numerical_feature || []} field="distribution_text_numerical_feature" updateModel_array={this.updateModel__array} /></div>}
            {media_typeImage && <div className="pt-3"><DistributionImageArray {...this.props} datasetDistributionItemIndex={this.props.datasetDistributionItemIndex} {...this.props.formElements.distribution_image_feature} initialValueArray={datasetDistributionItem.distribution_image_feature || []} field="distribution_image_feature" updateModel_array={this.updateModel__array} /></div>}
            {media_typeAudio && <div className="pt-3"><DistributionAudioArray {...this.props} datasetDistributionItemIndex={this.props.datasetDistributionItemIndex} {...this.props.formElements.distribution_audio_feature} initialValueArray={datasetDistributionItem.distribution_audio_feature || []} field="distribution_audio_feature" updateModel_array={this.updateModel__array} /></div>}
            {/*{(this.props.under_construction || this.props.model.described_entity.lr_subclass.ld_subclass === SUBCLASS_TYPE_MODEL) && <div className="pt-3"><DistributionUnspecifiedArray {...this.props} datasetDistributionItemIndex={this.props.datasetDistributionItemIndex} {...this.props.formElements.distribution_unspecified_feature} initialValueArray={datasetDistributionItem.distribution_unspecified_feature || []} field="distribution_unspecified_feature" updateModel_array={this.updateModel__array} /></div>}*/}
            {(for_info || this.props.model.described_entity.lr_subclass?.ld_subclass === SUBCLASS_TYPE_MODEL) && <div className="pt-3"><DistributionUnspecifiedItem {...this.props} datasetDistributionItemIndex={this.props.datasetDistributionItemIndex} {...this.props.formElements.distribution_unspecified_feature} distribution_unspecified_featureItem={datasetDistributionItem.distribution_unspecified_feature || JSON.parse(JSON.stringify(distribution_unspecified_featureObj))} field="distribution_unspecified_feature" updateModel={this.setObjectGeneral} /></div>}

            {/*<div className="pt-3"><DistributionRightsHolderAutocomplete datasetDistributionItemIndex={this.props.datasetDistributionItemIndex} {...this.props.formElements.distribution_rights_holder} initialValueArray={datasetDistributionItem.distribution_rights_holder || []} field="distribution_rights_holder" updateModel_Array={this.updateModel__array} /></div>*/}

            <div className="pt-3"><LicenceTerms {...this.props.formElements.licence_terms} initialValueArray={datasetDistributionItem.licence_terms || []} field="licence_terms" updateModel_Array={this.updateModel__array} /></div>
            <div className="pt-3">
                <Cost
                    key={JSON.stringify(datasetDistributionItem.cost ? datasetDistributionItem.cost : '') + "_datasetDistributionItem_cost_" + this.props.datasetDistributionItemIndex}
                    {...this.props.formElements.cost} initialValue={datasetDistributionItem.cost} field="cost" updateModel={this.setObjectGeneral}
                />
            </div>
            <div className="pt-3">
                <AccessRightsStatementAutocomplete
                    {...this.props.formElements.access_rights}
                    initialValue={datasetDistributionItem.access_rights || []}
                    field="access_rights"
                    setModelField={this.setAccessRights}
                />
            </div>
            <div className="pt-3"><AutocompleteChoicesChips className="wd-100"  {...this.props.formElements.membership_institution} initialValuesArray={datasetDistributionItem.membership_institution || []} field="membership_institution" updateModel_array={this.updateModel__array} /></div>

        </div>
    }
}