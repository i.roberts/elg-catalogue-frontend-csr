import React from "react";
import ProgressBar from "./../../componentsAPI/CommonComponents/ProgressBar";
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import VerticalTabPanel from '../../componentsAPI/CustomVerticalTabs/VerticalTabPanel';
import Grid from '@material-ui/core/Grid';
//import Typography from '@material-ui/core/Typography';
//import Button from '@material-ui/core/Button';
import organizationSchemaParser from "../../parsers/organizationSchemaParser";
import RecordSelectList from "../editorCommonComponents/RecordSelectList";
import RecordRadioBoolean from "../editorCommonComponents/RecordRadioBoolean";
import FreeTextList from "../editorCommonComponents/FreeTextList";
import WebsiteList from "../editorCommonComponents/WebsiteList";
import Logo from "../editorCommonComponents/Logo";
import EmailList from "../editorCommonComponents/EmailList";
import DomainAutocomplete from "../editorCommonComponents/DomainAutocomplete";
import SocialMedia from "../editorCommonComponents/SocialMedia";
import LanguageSpecificTextRichEditor from "../editorCommonComponents/LanguageSpecificTextRichEditor";
import OrganizationAutocomplete from "../EditorProjectComponents/OrganizationAutocomplete";
import GenericSchemaParser from "./../../parsers/GenericSchemaParser";
import AddressSet from "./AddressSet";
import AddressSetList from "./AddressSetList";
import OrganizationIdentifier from "./OrganizationIdentifier";
import LanguageSpecificText from "../editorCommonComponents/LanguageSpecificText";
import LanguageSpecificTextList from "../editorCommonComponents/LanguageSpecificTextList";
import { ORGANIZATION_TABS_HEADERS } from "../../config/editorConstants";
import LtArea from "../editorCommonComponents/LtArea";
import AutocompleteChoicesChips from "../editorCommonComponents/AutocompleteChoicesChips";
import { switchIcon } from "../../config/editorConstants";
import LanguageSpecificTextListLookup from "../editorCommonComponents/LanguageSpecificTextListLookup";
//import AutocompleteOneChoice from "../editorCommonComponents/AutocompleteOneChoice";
//import GenericOrganization from "../EditorGenericComponents/GenericOrganization";
//import { generic_organization_obj } from "../Models/GenericModels";
//import messages from "./../../config/messages";

function a11yProps(index) {
    return {
        id: `vertical-tab-${index}`,
        'aria-controls': `vertical-tabpanel-${index}`,
    };
}
export default class CreateOrganization extends React.Component {
    constructor(props) {
        super(props);
        this.state = { tab: 0 };
    }

    toggleTab = (tabIndex) => {
        this.setState({ tab: tabIndex });
    }

    setObjectGeneral = (field, valueObj) => {
        const { model } = this.props;
        model.described_entity[field] = valueObj;
        this.props.updateModel(model);
    }

    updateModel_generic_array = (obj2update, valueArray) => {
        const { model } = this.props;
        if (!model.described_entity[obj2update]) {
            model.described_entity[obj2update] = [];
        }
        model.described_entity[obj2update] = valueArray;
        this.props.updateModel(model);
    }

    setSelectLegalStatus = (event, selectedObjArray) => {
        const { model } = this.props;
        if (selectedObjArray && selectedObjArray.length > 0) {
            model.described_entity.organization_legal_status = selectedObjArray[0].value;
        } else {
            model.described_entity.organization_legal_status = null;
        }
        this.props.updateModel(model);
    }

    setStartupBoolean = (e) => {
        const { model } = this.props;
        if (model.described_entity.startup === null || model.described_entity.startup === undefined) {
            model.described_entity.startup = null;
        }
        model.described_entity.startup = e.target.value === "true";
        this.props.updateModel(model);
    }

    onDescriptionChange = () => {
        this.props.onDescriptionChange();
    } 

    render() {
        if (!this.props.schema) {
            return <ProgressBar />
        }
        const { model } = this.props;

        const data = this.props.schema;
        const logo_default_value = model.described_entity.logo || "";
        const { help_text: legal_status_help_text, label: legal_status_label, required: legal_status_required, type: legal_status_type, choices: legal_status_choices } = organizationSchemaParser.getOrganizationLegalStatus(data);
        const legal_status_default_value = model.described_entity.organization_legal_status || "";
        const { help_text: startup_help_text, label: startup_label, required: startup_required, type: startup_type } = organizationSchemaParser.getStartup(data);
        const startup_default_value = model.described_entity.startup;
        //rules with this approach we override any user input
        legal_status_default_value !== "http://w3id.org/meta-share/meta-share/sme" ? delete model.described_entity["startup"] : void 0;
        model.described_entity.organization_legal_status !== "http://w3id.org/meta-share/meta-share/academicInstitution" ? delete model.described_entity["discipline"] : void 0;
        !(model.described_entity.head_office_address && model.described_entity.head_office_address.country) ? delete model.described_entity["other_office_address"] : void 0;

        return <div>

            <div className="editor-main-card-container">

                <form >
                    <div className="tabs-main-container">
                        <div className="vertical-tabs-container-forms">
                            <Tabs value={this.state.tab} onChange={this.toggleTab} variant="scrollable" orientation="vertical" aria-label="vertical tabs example" className="vertical-tabs-forms">
                                {ORGANIZATION_TABS_HEADERS.map((tab, index) => <Tab key={index} label={<><span>{switchIcon(tab)}</span><span> {tab} </span></>}  {...a11yProps(index)} onClick={() => { this.toggleTab(index); }} />)}
                            </Tabs>
                            <VerticalTabPanel value={this.state.tab} index={0} className="vertical-tab-pannel">
                                <Grid container direction="row" alignItems="flex-start" spacing={2} className="vertical-tabs-inner">
                                    <Grid item xs>
                                        <div className="pb-3"><LanguageSpecificText {...GenericSchemaParser.getFormElement("organization_name", data.organization_name)} defaultValueObj={model.described_entity.organization_name || { "en": "" }} field="organization_name" setLanguageSpecificText={this.setObjectGeneral} /></div>
                                        <div className="pb-3"><OrganizationIdentifier   {...organizationSchemaParser.getOrganizationIdentifier(data)} default_valueArray={model.described_entity.organization_identifier || []} field="organization_identifier" updateModel_Identifier={this.updateModel_generic_array} /></div>
                                        <div className="pb-3"><LanguageSpecificTextList  {...GenericSchemaParser.getFormElement("organization_short_name", data.organization_short_name)} default_valueArray={model.described_entity.organization_short_name || []} field="organization_short_name" setLanguageSpecifictextList={this.setObjectGeneral} /></div>
                                        <div className="pb-3"><LanguageSpecificTextList  {...organizationSchemaParser.getOrganizationAltName(data)} default_valueArray={model.described_entity.organization_alternative_name || []} field="organization_alternative_name" setLanguageSpecifictextList={this.setObjectGeneral} /></div>
                                        {/*<div className="pb-3"><LanguageSpecificText  {...organizationSchemaParser.getOrganizationBio(data)} defaultValueObj={model.described_entity.organization_bio || { "en": "" }} field="organization_bio" multiline={true} maxRows={6} setLanguageSpecificText={this.setObjectGeneral} /></div>*/}
                                        <div className="pb-3"><LanguageSpecificTextRichEditor  onDescriptionChange={this.onDescriptionChange} {...GenericSchemaParser.getFormElement("organization_bio", data.organization_bio)} defaultValueObj={model.described_entity.organization_bio || { "en": "" }} field="organization_bio" multiline={true} rowsMax={6} setLanguageSpecificText={this.setObjectGeneral} /></div>

                                        <div className="pb-3"><Logo className="wd-100" key={"organization_described_entity_logo" + model.described_entity.logo} {...organizationSchemaParser.getLogo(data)} logo={logo_default_value} field="logo" keycloak={this.props.keycloak} model={model} updateModel_logo={this.setObjectGeneral} /></div>
                                    </Grid>
                                </Grid>
                            </VerticalTabPanel>
                            <VerticalTabPanel value={this.state.tab} index={1} className="vertical-tab-pannel">
                                <Grid container direction="row" alignItems="flex-start" spacing={2} className="vertical-tabs-inner">
                                    <Grid item xs>
                                        <div className="pb-3"><RecordSelectList className="wd-100" type={legal_status_type} required={legal_status_required} label={legal_status_label} help_text={legal_status_help_text} choices={legal_status_choices} default_value={legal_status_default_value} setSelectedvalue={this.setSelectLegalStatus} /></div>
                                        {model.described_entity.organization_legal_status === "http://w3id.org/meta-share/meta-share/sme" && <div className="pb-3"><RecordRadioBoolean className="wd-100" type={startup_type} required={startup_required} label={startup_label} help_text={startup_help_text} default_value={startup_default_value} handleBooleanChange={this.setStartupBoolean} /></div>}
                                        <div className="pb-3">
                                            <AutocompleteChoicesChips className="wd-100"
                                                {...GenericSchemaParser.getFormElement("organization_role", data.organization_role)}
                                                initialValuesArray={model.described_entity.organization_role || []}
                                                field="organization_role"
                                                lr_subclass={false}
                                                updateModel_array={this.updateModel_generic_array}
                                            />
                                        </div>
                                        <div><LtArea {...organizationSchemaParser.getLtAreaKeywords(data)} placeholder={GenericSchemaParser.getFormElement("lt_area", data.lt_area).placeholder} default_valueArray={model.described_entity.lt_area || []} updateModel_ltArea={this.setObjectGeneral} /></div>
                                        {/*<div><LanguageSpecificTextList  {...organizationSchemaParser.getservicesOfferedKeywords(data)} default_valueArray={model.described_entity.service_offered || []} field="service_offered" setLanguageSpecifictextList={this.setObjectGeneral} /></div>*/}
                                        <div><LanguageSpecificTextListLookup  {...organizationSchemaParser.getservicesOfferedKeywords(data)} default_valueArray={model.described_entity.service_offered || []} field="service_offered" field_type="service_offered" entity_type="organization" setLanguageSpecifictextList={this.updateModel_generic_array} /></div>
                                        {model.described_entity.organization_legal_status === "http://w3id.org/meta-share/meta-share/academicInstitution" && <div className="pb-3"><LanguageSpecificTextList   {...organizationSchemaParser.getDisciplineKeywords(data)} default_valueArray={model.described_entity.discipline || []} field="discipline" setLanguageSpecifictextList={this.setObjectGeneral} /></div>}
                                        <div><DomainAutocomplete {...organizationSchemaParser.getDomainKeywords(data)} default_valueArray={model.described_entity.domain ? JSON.parse(JSON.stringify(model.described_entity.domain)) : []} updateModel_Domain={this.updateModel_generic_array} /></div>
                                        {/*<div><LanguageSpecificTextList  {...organizationSchemaParser.getKeywords(data)} default_valueArray={model.described_entity.keyword || []} field="keyword" setLanguageSpecifictextList={this.setObjectGeneral} /></div>*/}
                                        <div><LanguageSpecificTextListLookup  {...GenericSchemaParser.getFormElement("keyword", data.keyword)} default_valueArray={model.described_entity.keyword || []} field="keyword" field_type="keyword" entity_type="organization" setLanguageSpecifictextList={this.updateModel_generic_array} /></div>
                                        {/*<div className="pb-3"><MemberOfAssociationAutocomplete  {...organizationSchemaParser.get_member_of_association(data)} default_valueArray={model.described_entity.member_of_association || []} updateModel_member_of_association={this.updateModel_generic_array} /></div>*/}
                                        <div><OrganizationAutocomplete {...GenericSchemaParser.getFormElement("member_of_association", data.member_of_association)} initialValueArray={model.described_entity.member_of_association ? JSON.parse(JSON.stringify(model.described_entity.member_of_association)) : []} field="member_of_association" updateModel_Array={this.setObjectGeneral} /></div>
                                        <div><OrganizationAutocomplete {...GenericSchemaParser.getFormElement("replaces_organization", data.replaces_organization)} initialValueArray={model.described_entity.replaces_organization ? JSON.parse(JSON.stringify(model.described_entity.replaces_organization)) : []} field="replaces_organization" updateModel_Array={this.setObjectGeneral} /></div>
                                    </Grid>
                                </Grid>
                            </VerticalTabPanel>
                            <VerticalTabPanel value={this.state.tab} index={2} className="vertical-tab-pannel">
                                <Grid container direction="row" alignItems="flex-start" spacing={2} className="vertical-tabs-inner">
                                    <Grid item xs>
                                        <div className="pb-3"><EmailList className="wd-100" {...organizationSchemaParser.getOrganizationEmails(data)} default_value_Array={model.described_entity.email ? JSON.parse(JSON.stringify(model.described_entity.email)) : []} field="email" updateModel_email={this.updateModel_generic_array} /></div>
                                        <div className="pb-3"><WebsiteList className="wd-100"  {...organizationSchemaParser.getOrganizationWebsites(data)} default_value_Array={model.described_entity.website ? JSON.parse(JSON.stringify(model.described_entity.website)) : []} field="website" updateModel_website={this.updateModel_generic_array} /></div>
                                        <div className="pb-3"><AddressSet {...organizationSchemaParser.GetHeadOfficeAddress(data)} other_address={model.described_entity.other_office_address} default_valueObj={model.described_entity.head_office_address || {}} field="head_office_address" updateModel_address_set_obj={this.setObjectGeneral} /></div>
                                        {model.described_entity.head_office_address && model.described_entity.head_office_address.country && <div className="pb-3"><AddressSetList {...organizationSchemaParser.GetOtherOfficeAddress(data)} default_valueArray={model.described_entity.other_office_address || []} field="other_office_address" updateModel_address_set={this.updateModel_generic_array} /></div>}
                                        <div className="pb-3"><FreeTextList className="wd-100" {...organizationSchemaParser.getTelephoneNumbers(data)} default_value_Array={model.described_entity.telephone_number ? JSON.parse(JSON.stringify(model.described_entity.telephone_number)) : []} field="telephone_number" updateModel_Array={this.updateModel_generic_array} /></div>
                                        <div className="pb-3"><FreeTextList className="wd-100" {...organizationSchemaParser.getFaxNumbers(data)} default_value_Array={model.described_entity.fax_number ? JSON.parse(JSON.stringify(model.described_entity.fax_number)) : []} field="fax_number" updateModel_Array={this.updateModel_generic_array} /></div>
                                        <div className="pb-3"><SocialMedia {...organizationSchemaParser.getSocialMedia(data)} default_valueArray={model.described_entity.social_media_occupational_account ? JSON.parse(JSON.stringify(model.described_entity.social_media_occupational_account)) : []} field="social_media_occupational_account" updateModel_SocialMedia={this.updateModel_generic_array} /></div>
                                    </Grid>
                                </Grid>
                            </VerticalTabPanel>
                        </div>
                    </div>
                </form>
            </div>

        </div >
    }
}