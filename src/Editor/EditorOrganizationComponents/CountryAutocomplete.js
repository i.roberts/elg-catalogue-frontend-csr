import React from "react";
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import parse from 'autosuggest-highlight/parse';
import match from 'autosuggest-highlight/match';
import Chip from '@material-ui/core/Chip';

export default class CountryAutocomplete extends React.Component {
    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.default_value !== this.props.default_value;
    }
    render() {
        const { required, label, help_text, choices, default_value = "" } = this.props;
        const choiceFilteredArray = choices.filter(item => item.value === default_value);
        const default_value_formated = { value: default_value, display_name: default_value };
        if (choiceFilteredArray.length > 0) {
            default_value_formated.display_name = choiceFilteredArray[0].display_name;
        }
        return <Autocomplete
            required={required}
            autoHighlight
            value={default_value_formated}
            onChange={(event, newValue) => {
                if (newValue && newValue.value) {
                    this.props.setSelection(newValue);
                }
            }}
            options={choices.sort((a, b) => -b.display_name[0].toUpperCase().localeCompare(a.display_name[0].toUpperCase()))}
            groupBy={(option) => option.display_name[0].toUpperCase()}
            getOptionLabel={(option) => option.display_name || ""}
            getOptionSelected={(option, value) => option.value === value.value}
            renderInput={(params) => (
                <TextField {...params} required={required} label={label} variant="outlined" helperText={help_text} />
            )}
            renderOption={(option, { inputValue }) => {
                const matches = match(option.display_name, inputValue);
                const parts = parse(option.display_name, matches);
                return (
                    <div>
                        {parts.map((part, index) => (
                            <span key={index} style={{ fontWeight: part.highlight ? 700 : 400 }}>
                                {part.text}
                            </span>
                        ))}
                    </div>
                );
            }}
            renderTags={(value, getTagProps) =>
                value.map((option, index) => (
                    <Chip variant="outlined" label={option.display_name} {...getTagProps({ index })} />
                ))
            }
        />
    }
}