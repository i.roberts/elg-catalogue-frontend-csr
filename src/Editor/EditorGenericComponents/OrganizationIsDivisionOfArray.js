import React from "react";
import Typography from '@material-ui/core/Typography';
import { generic_organization_obj } from "../Models/GenericModels";
import OrganizationIsDivisionOf2 from "./OrganizationIsDivisionOf";
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
import messages from "./../../config/messages";

export default class OrganizationIsDivisionOfArray extends React.Component {
    constructor(props) {
        super(props);
        this.state = { initialValueArray: props.initialValueArray || [] };
    }

    /*static getDerivedStateFromProps(nextProps, prevState) {
        return {
            initialValueArray: nextProps.initialValueArray || [],
        };
    }*/

    setValue = (action, index) => {
        const { initialValueArray } = this.state;
        switch (action) {
            case "fill_in_object":
                initialValueArray.push(JSON.parse(JSON.stringify(generic_organization_obj)));
                break;
            case "remove_object":
                if (initialValueArray.length === 1) {
                    this.props.updateDevisionOf([]);
                    return;
                }
                initialValueArray.splice(index, 1);
                break;
            default:
                break;
        }
        this.setState({ initialValueArray }, this.onBlur);
    }

    updateDevisionOf = (index, value) => {
        const { initialValueArray } = this.state;
        initialValueArray[index] = value;
        this.setState({ initialValueArray }, this.onBlur);
    }

    onBlur = () => {
        this.props.updateDevisionOf(this.state.initialValueArray);
    }

    render() {
        const { initialValueArray } = this.state;
        return <div onBlur={this.onBlur} className="pb-3 inner--group nested--group">

            <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1}>

                <Grid item sm={11}>
                    {((!this.props.disable) || (this.props.disable && initialValueArray.length > 0)) && <Typography variant="h3" className="section-links" style={{ "marginLeft": "2%" }}> {this.props.child_to_add_parents_name} is division of</Typography>}
                </Grid>

                <Grid item sm={1}>
                    {(initialValueArray.length === 0 && !this.props.disable) && <Tooltip title={`${messages.group_elements_create} division of ${this.props.child_to_add_parents_name}`}><span> <Button disabled={this.props.disable} className="inner-link-default--purple" onClick={(e) => this.setValue("fill_in_object")}>{messages.group_elements_create}</Button></span></Tooltip>}
                </Grid>

            </Grid>

            {
                initialValueArray.map((item, itemIndex) => {
                    return <div key={itemIndex} className="ml-20" style={{ "marginLeft": "2%" }}>

                        <OrganizationIsDivisionOf2
                            {...this.props}
                            disable={item.hasOwnProperty("pk")}
                            initialValue={item}
                            itemIndex={itemIndex}
                            updateDevisionOf={this.updateDevisionOf}
                        />

                        <Grid container direction="row" justifyContent="flex-start" alignItems="baseline" >
                            {
                                !this.props.disable && <Grid item>
                                    <span>
                                        <Tooltip title={`${messages.array_elements_remove} ${(item && item.organization_name && item.organization_name["en"]) ? item.organization_name["en"] : ''} division of ${this.props.child_to_add_parents_name || ""}`}>
                                            <span>
                                                <Button disabled={this.props.disable} className="inner-link-default--purple" onClick={(e) => this.setValue("remove_object", itemIndex)}>{messages.array_elements_remove}</Button>
                                            </span>
                                        </Tooltip>
                                    </span>
                                    {
                                        (itemIndex === initialValueArray.length - 1 && item && item.organization_name && item.organization_name["en"]) &&
                                        <span>
                                            <Tooltip title={`${messages.array_elements_add} ${initialValueArray.length >= 1 ? 'another' : ''} division of ${this.props.child_to_add_parents_name}`}>
                                                <span>
                                                    <Button className="inner-link-default--purple" onClick={(e) => this.setValue("fill_in_object")}>{messages.array_elements_add}</Button>
                                                </span>
                                            </Tooltip>
                                        </span>
                                    }
                                </Grid>
                            }
                        </Grid>

                    </div>
                })
            }
        </div >
    }
}