import React from "react";
import { withRouter } from "react-router-dom";
import { Helmet } from "react-helmet";
import axios from "axios";
import Grid from '@material-ui/core/Grid';
import ProgressBar from "../componentsAPI/CommonComponents/ProgressBar";
import { EDITOR_MODELS_LR_SCHEMA, EDITOR_MODELS_SCHEMA_LR_SUBCLASS, EDITOR_MODELS_SCHEMA_MEDIA_PART, CORPUS_TOP_TABS_HEADERS_TOOLTIPS } from "../config/editorConstants";
import Container from '@material-ui/core/Container';
import { empty_corpus } from "./Models/CorpusModel";
//import { ldMediaUnspecifiedPartObj } from "./Models/LdModel";
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import HorizontalTabPanel from "../componentsAPI/CustomVerticalTabs/HorizontalTabPanel"
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import CorpusFirstStep from "./EditorCorpusComponents/CorpusFirstStep";
import CorpusSecondStep from "./EditorCorpusComponents/CorpusSecondStep";
import CorpusThirdStep from "./EditorCorpusComponents/CorpusThirdStep";
import CorpusFourthStep from "./EditorCorpusComponents/CorpusFourthStep";
import DisplayModel from "./editorCommonComponents/DisplayModel";
import LookupCorpusName from "./EditorCorpusComponents/LookupCorpusName";
import DashboardAppBar from "../DashboardComponents/DashboardAppBar";
import ValidationYupErrors from "./editorCommonComponents/ValidationYupErrors";
import Tooltip from '@material-ui/core/Tooltip';
import CorpusDataTab from "./EditorCorpusComponents/CorpusDataTab";
import {
  CORPUS_TOP_TABS_HEADERS,
  EDITOR_DRAFT_NEW_RECORD,
  CORPUS_FIRST_SECTION_TABS_HEADERS,
  CORPUS_SECOND_SECTION_TABS_HEADERS,
  CORPUS_THIRD_SECTION_TABS_HEADERS,
  CORPUS_FOURTH_SECTION_TABS_HEADERS
} from "../config/editorConstants";
import DataComponentDialogueWraper from "./editorCommonComponents/DataComponentDialogueWraper";
import UserWarning from "./editorCommonComponents/UserWarning";

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

class CreateCorpus extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      schema: null, schema_lr_subclass: null, schema_text_part: null, schema_audio_part: null, schema_video_part: null, schema_text_numerical: null, schema_image_part: null,
      keycloak: props.keycloak, model: props.model ? JSON.parse(JSON.stringify(props.model)) : JSON.parse(JSON.stringify(empty_corpus)), tab: 0, backend_error_response: null, lookupCorpusName: props.model ? false : true,
      under_construction: props.model ? (props.model.management_object && props.model.management_object.under_construction && props.model.management_object.under_construction ? true : false) : null,
      yupError: null,
      source: null, loading: false, showDataComponentDialogueWraper: false,
      tabInSection: 0, yupClickError: null, randomValue2ForceRerender: 0,
      showWarning: false
    };
  }

  componentWillUnmount = () => {
    if (this.state.source) {
      this.state.source.cancel("");
    }
  }

  componentDidMount() {
    if (this.state.source) {
      this.state.source.cancel("");
    }
    const CancelToken = axios.CancelToken;
    const source = CancelToken.source();
    this.setState({ source: source });
    const corpusRequest = axios.options(EDITOR_MODELS_LR_SCHEMA("corpus"), { cancelToken: source.token });
    const corpusSubclassRequest = axios.options(EDITOR_MODELS_SCHEMA_LR_SUBCLASS("corpus"), { cancelToken: source.token });
    const corpusTextPartRequest = axios.options(EDITOR_MODELS_SCHEMA_MEDIA_PART("corpus_text_part"), { cancelToken: source.token });
    const corpusAudioPartRequest = axios.options(EDITOR_MODELS_SCHEMA_MEDIA_PART("corpus_audio_part"), { cancelToken: source.token });
    const corpusVideoPartRequest = axios.options(EDITOR_MODELS_SCHEMA_MEDIA_PART("corpus_video_part"), { cancelToken: source.token });
    const corpusImagePartRequest = axios.options(EDITOR_MODELS_SCHEMA_MEDIA_PART("corpus_image_part"), { cancelToken: source.token });
    const corpusTextNumericalPartRequest = axios.options(EDITOR_MODELS_SCHEMA_MEDIA_PART("corpus_text_numerical_part"), { cancelToken: source.token });
    axios.all([corpusRequest, corpusSubclassRequest, corpusTextPartRequest, corpusAudioPartRequest, corpusVideoPartRequest, corpusImagePartRequest, corpusTextNumericalPartRequest])
      .then(axios.spread((...responses) => {
        const corpusResponse = responses[0]
        const subclassResponse = responses[1]
        const textPartResponse = responses[2];
        const audioPartResponse = responses[3];
        const videoPartResponse = responses[4];
        const imagePartResponse = responses[5];
        const textNumericalPartResponse = responses[6];
        this.setState({
          schema: corpusResponse.data.actions.POST, schema_lr_subclass: subclassResponse.data.actions.POST, schema_text_part: textPartResponse.data.actions.POST,
          schema_audio_part: audioPartResponse.data.actions.POST, schema_video_part: videoPartResponse.data.actions.POST, schema_image_part: imagePartResponse.data.actions.POST,
          schema_text_numerical: textNumericalPartResponse.data.actions.POST, source: null
        })
      }))
      .catch(errors => { console.log(errors); this.setState({ source: null }); })
  }

  handleYupErrorClick = (item) => {
    let parts = item && item.split(">");
    if (parts && parts.length >= 3) {
      const sectionIndex = CORPUS_TOP_TABS_HEADERS.findIndex(item => item.toLowerCase() === parts[1].trim().toLowerCase()) || 0;
      const tabInSection = this.getTab(sectionIndex, parts[2]) || 0;
      this.setState({ tab: sectionIndex, tabInSection: tabInSection, yupClickError: item, randomValue2ForceRerender: Math.random() });
    }
  }

  getTab = (sectionIndex, tabToMatch) => {
    switch (sectionIndex) {
      case 0:
        return CORPUS_FIRST_SECTION_TABS_HEADERS.findIndex(item => item.toLowerCase() === tabToMatch.trim().toLowerCase()) || 0;
      case 1:
        return CORPUS_SECOND_SECTION_TABS_HEADERS.findIndex(item => item.toLowerCase() === tabToMatch.trim().toLowerCase()) || 0;
      case 2:
        return CORPUS_THIRD_SECTION_TABS_HEADERS.findIndex(item => item.toLowerCase() === tabToMatch.trim().toLowerCase()) || 0;
      case 3:
        return CORPUS_FOURTH_SECTION_TABS_HEADERS.findIndex(item => item.toLowerCase() === tabToMatch.trim().toLowerCase()) || 0;
      default: return 0;
    }
  }

  handleUnderConstruction = (value) => {
    this.setState({ under_construction: value });
  }

  /*handle_for_info = (value) => {
    const model = this.state.model;
    if (!value) {
      model.described_entity.lr_subclass.unspecified_part = null;
    } else if (value) {
      model.described_entity.lr_subclass.unspecified_part = JSON.parse(JSON.stringify(ldMediaUnspecifiedPartObj));
    }
    this.updateModel(model);
  }*/

  set_backend_error_response = (error) => {
    this.setState({ backend_error_response: error })
  }

  set_backend_successful_response = (response) => {
    this.setState({ model: response, backend_error_response: null })
  }

  set_yup_error = (yupError) => {
    this.setState({ yupError });
  }

  updateModel = (model) => {
    this.setState({ model });
  }

  toggleTab = (index) => {
    this.setState({ tab: index, tabInSection: 0, yupClickError: null });
  };

  saveDraft = () => {
    if (this.state.source) {
      this.state.source.cancel("");
    }
    const CancelToken = axios.CancelToken;
    const source = CancelToken.source();
    this.setState({ source: source, loading: true });
    axios.post(EDITOR_DRAFT_NEW_RECORD, this.state.model, {
      headers: {
        'UNDER-CONSTRUCTION': this.state.under_construction === true ? true : false,
        //'FUNCTIONAL-SERVICE': this.props.functional_service === true ? true : false
      },
      cancelToken: source.token
    }).then((res) => {
      //console.log(JSON.stringify(res));
      let under_construction = false;
      if (res.data.management_object) {
        res.data.management_object.under_construction === true ? under_construction = true : under_construction = false;
      }
      this.setState({ under_construction: under_construction, source: null, laoding: false, model: res.data, showDataComponentDialogueWraper: true });
    }).catch((err) => {
      console.log(JSON.stringify(err));
      this.props.history.push("/createResource");
    });
  }


  hideLookUpByName = (corpusName, restProperties) => {
    const { model } = this.state;
    if (restProperties && restProperties.pk) {
      //model.pk = restProperties.pk;
      model.described_entity.pk = restProperties.pk;//add pk of generic record inside described_entity
      delete restProperties.pk;
    }
    model.described_entity.resource_name = { "en": corpusName };
    model.described_entity = { ...model.described_entity, ...restProperties };
    this.setState({ lookupCorpusName: false, model: model }, this.saveDraft);
  }

  handleDescriptionChange = () => {
    //this.setState({showWarning: true});
    const { model } = this.state;
    const { description = "" } = model.described_entity || {};
    (description && description["en"] && description["en"].length < 50) ? this.setState({ showWarning: true }) : this.setState({ showWarning: false })
  }

  render() {
    const { lookupCorpusName } = this.state;
    if (lookupCorpusName) {
      return <LookupCorpusName hideLookUpByName={this.hideLookUpByName} keycloak={this.props.keycloak} />
    }

    if (!this.state.model.hasOwnProperty("pk")) {
      return <ProgressBar />
    }

    if (this.state.showDataComponentDialogueWraper) {
      return <DataComponentDialogueWraper {...this.props}  {...this.state} hideDataComponentDialogueWraper={() => { this.setState({ showDataComponentDialogueWraper: false }) }} />
    }

    if (!this.state.schema || !this.state.schema_lr_subclass) {
      return <ProgressBar />
    }

    const data = this.state.schema;




    return (
      <>
        <Helmet>
          <title>ELG - Create Corpus</title>
        </Helmet>
        <DashboardAppBar />
        <div className="editor-container-white pb-2">
          <Container maxWidth="xl">
            <div className="empty"></div>
            <Typography className="dashboard-title-box pb-05">Create a new corpus</Typography>
            <div>
              {this.state.backend_error_response && <div>
                <h3>Error</h3>
                <div className=" boxed">
                  <Paper elevation={13} >
                    <code>
                      <pre id="special">
                        {JSON.stringify(this.state.backend_error_response, null, 2)}
                      </pre>
                    </code>
                  </Paper>
                </div>
              </div>
              }
            </div>
            {!this.state.backend_error_response && <ValidationYupErrors key={this.state.yupError ? JSON.stringify(this.state.yupError) : "1"} yupError={this.state.yupError} requiredFields={[]} handleYupErrorClick={this.handleYupErrorClick} />}
            {<UserWarning showWarning={this.state.showWarning} />}
          </Container>


          <div className="editor-actions-header">
            <Container maxWidth="xl">
              <Grid container direction="row" justifyContent="space-between" alignItems="center" className="grays--offwhite">
                <Grid item xs={7}>
                  <Tabs value={this.state.tab} onChange={this.toggleTab} variant="scrollable" orientation="horizontal" aria-label="simple tabs example" className="simple-tabs-forms">
                    {CORPUS_TOP_TABS_HEADERS.map((tab, index) => <Tab key={index} label={<Tooltip title={CORPUS_TOP_TABS_HEADERS_TOOLTIPS[index]}><div className="pt-2"> {tab}</div></Tooltip>}  {...a11yProps(index)} onClick={() => { this.toggleTab(index); }} />)}
                  </Tabs>
                </Grid>
                <Grid item container xs={5} alignItems="center" justifyContent="flex-end" className="grays--offwhite">
                  <DisplayModel
                    data={data}
                    schema_lr_subclass={this.state.schema_lr_subclass}
                    schema_text_part={this.state.schema_text_part}
                    schema_audio_part={this.state.schema_audio_part}
                    schema_video_part={this.state.schema_video_part}
                    schema_text_numerical={this.state.schema_text_numerical}
                    schema_image_part={this.state.schema_image_part}
                    handleUnderConstruction={this.handleUnderConstruction}
                    under_construction={this.state.under_construction}
                    model={this.state.model} set_backend_error_response={this.set_backend_error_response} set_backend_successful_response={this.set_backend_successful_response} set_yup_error={this.set_yup_error} />
                </Grid>
              </Grid>
            </Container>
          </div>
          <Container maxWidth="xl">
            <div className="editor-main-card-container">
              <HorizontalTabPanel value={this.state.tab} index={0} className="horizontal-tab-pannel">
                <CorpusFirstStep onDescriptionChange={this.handleDescriptionChange} key={`section-0-${this.state.tabInSection}-${this.state.yupError || ""}-${this.state.yupClickError || ""} - ${this.state.randomValue2ForceRerender}`} {...this.props}  {...this.state} updateModel={this.updateModel} settabInSection={(tab) => { this.setState({ tabInSection: tab }) }} />
              </HorizontalTabPanel>
              <HorizontalTabPanel value={this.state.tab} index={1} className="horizontal-tab-pannel">
                <CorpusSecondStep key={`section-1-${this.state.tabInSection}-${this.state.yupError || ""}-${this.state.yupClickError || ""}- ${this.state.randomValue2ForceRerender}`} {...this.props}  {...this.state} updateModel={this.updateModel} settabInSection={(tab) => { this.setState({ tabInSection: tab }) }} />
              </HorizontalTabPanel>
              <HorizontalTabPanel value={this.state.tab} index={2} className="horizontal-tab-pannel">
                <CorpusThirdStep key={`section-2-${this.state.tabInSection}-${this.state.yupError || ""}-${this.state.yupClickError || ""}- ${this.state.randomValue2ForceRerender}`}{...this.props}  {...this.state} updateModel={this.updateModel} settabInSection={(tab) => { this.setState({ tabInSection: tab }) }} />
              </HorizontalTabPanel>
              <HorizontalTabPanel value={this.state.tab} index={3} className="horizontal-tab-pannel">
                <CorpusFourthStep key={`section-3-${this.state.tabInSection}-${this.state.yupError || ""}-${this.state.yupClickError || ""}- ${this.state.randomValue2ForceRerender}`} {...this.props}  {...this.state} updateModel={this.updateModel} settabInSection={(tab) => { this.setState({ tabInSection: tab }) }} />
              </HorizontalTabPanel>
              <HorizontalTabPanel value={this.state.tab} index={4} className="horizontal-tab-pannel">
                <CorpusDataTab key={`section-4-${this.state.tabInSection}-${this.state.yupError || ""}-${this.state.yupClickError || ""}- ${this.state.randomValue2ForceRerender}`}{...this.props}  {...this.state} updateModel={this.updateModel} settabInSection={(tab) => { this.setState({ tabInSection: tab }) }} />
              </HorizontalTabPanel>
            </div>
          </Container>

          <div className="editor-actions-bottom">
            <Container maxWidth="xl">
              <Grid container direction="row" justifyContent="flex-end" alignItems="center" className="pb-3 pt-3 grays--offwhite">
                <Grid item container xs={12} alignItems="center">
                  <DisplayModel
                    data={data}
                    schema_lr_subclass={this.state.schema_lr_subclass}
                    schema_text_part={this.state.schema_text_part}
                    schema_audio_part={this.state.schema_audio_part}
                    schema_video_part={this.state.schema_video_part}
                    schema_text_numerical={this.state.schema_text_numerical}
                    schema_image_part={this.state.schema_image_part}
                    handleUnderConstruction={this.handleUnderConstruction}
                    under_construction={this.state.under_construction}
                    model={this.state.model} set_backend_error_response={this.set_backend_error_response} set_backend_successful_response={this.set_backend_successful_response} hide_checkboxes={true} set_yup_error={this.set_yup_error} />
                </Grid>
              </Grid>
            </Container>
          </div>
          <div>
            {this.state.backend_error_response && <div style={{ marginBottom: "100px", paddingBottom: "100px" }}>
              <h3>Error</h3>
              <div className=" boxed">
                <Paper elevation={13} >
                  <code>
                    <pre id="special">
                      {JSON.stringify(this.state.backend_error_response, null, 2)}
                    </pre>
                  </code>
                </Paper>
              </div>
            </div>
            }
          </div>
        </div>
      </>
    );
  }
}

export default withRouter(CreateCorpus)