import React from "react";
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import RecordSelectList from "../editorCommonComponents/RecordSelectList";
import GenericPerson from "../EditorGenericComponents/GenericPerson";
import GenericOrganization from "../EditorGenericComponents/GenericOrganization";
import GenericGroup from "../EditorGenericComponents/GenericGroup";
import { generic_person_obj, generic_organization_obj, generic_group_obj } from "../Models/GenericModels";
import messages from "./../../config/messages"; 

function ActorChoice(props) {
    return <div>
        <div><RecordSelectList className="wd-100" {...props} default_value=""
            choices={["Person", "Organization", "Group"].map((item) => { return { "display_name": item, "value": item, index: props.index } })}
            setSelectedvalue={props.setActor} /></div>
    </div>
}

export default class ActorTypeAutocomplete extends React.Component {
    constructor(props) {
        super(props);
        this.state = { initialValueArray: props.initialValueArray || [] };
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            initialValueArray: nextProps.initialValueArray || []
        };
    }

    setValues = (action, index) => {
        const { initialValueArray } = this.state;
        switch (action) {
            case "addArrayItem":
                if (initialValueArray.filter(item => item.actor_type !== "actor_type").length !== initialValueArray.length) {
                    return;
                }
                initialValueArray.push({ actor_type: "actor_type" });
                //this.props.updateModel_Array(this.props.field, initialValueArray);
                this.setState({ initialValueArray });
                break;
            case "removeArrayItem":
                initialValueArray.splice(index, 1);
                if (initialValueArray.length === 0) {
                    this.props.updateModel_Array(this.props.field, []);
                    return;
                }
                this.props.updateModel_Array(this.props.field, initialValueArray);
                break;
            default:
                break;
        }
        this.setState({ initialValueArray });
        //this.props.updateModel_Array(this.props.field, initialValueArray);
    }

    setActor = (e, val) => {
        const { initialValueArray } = this.state;
        //initialValueArray[val[0].index] = { actor_type: val[0].value };
        if (val[0].value === "Person") {
            initialValueArray[val[0].index] = JSON.parse(JSON.stringify(generic_person_obj));
        } else if (val[0].value === "Organization") {
            initialValueArray[val[0].index] = JSON.parse(JSON.stringify(generic_organization_obj));
        } else if (val[0].value === "Group") {
            initialValueArray[val[0].index] = JSON.parse(JSON.stringify(generic_group_obj));
        }
        this.setState({ initialValueArray }, this.onBlur);
    }

    updateModel = (index, value) => {
        const { initialValueArray } = this.state;
        initialValueArray[index] = value;
        this.setState(initialValueArray);
        this.props.updateModel_Array(this.props.field, this.state.initialValueArray);
    }

    onBlur = () => {
        //const array = this.state.initialValueArray.filter(item => item.actor_type !== "actor_type");
        const array = this.state.initialValueArray;
        this.props.updateModel_Array(this.props.field, array.length ? array : []);
        //this.props.updateModel_Array(this.props.field, this.state.initialValueArray);
    }

    render() {
        const { initialValueArray } = this.state;
        if (this.props.disabled && initialValueArray.length === 0) {
            return <div></div>;
        }
        initialValueArray.length === 0 && initialValueArray.push({ actor_type: "actor_type" });
        return <div onBlur={this.onBlur}>
            <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1} >
                <Grid item sm={12}>
                    <Typography variant="h3" className="section-links" >{this.props.label} </Typography>
                    <Typography className="section-links" >{this.props.help_text} </Typography>
                </Grid>
                <Grid item sm={1}>
                    {/*initialValueArray.length === 0 && <Button className="inner-link-outlined--purple" onClick={(e) => this.setValues("addArrayItem")}><CreateIcon /></Button>*/}
                    {/*initialValueArray.length !== 0 && <Button className="inner-link-outlined--purple" onClick={(e) => this.setValues("addArrayItem")}><AddCircleOutlineIcon /></Button>*/}
                </Grid>
            </Grid>
            {
                initialValueArray.map((item, index) => {
                    if (item.actor_type === "actor_type") {
                        return <div key={index}>
                            {!this.props.disabled && index !== 0 && <Grid container  direction="row" justifyContent="flex-end" alignItems="baseline" >
                                <Grid item><Button className="inner-link-default--purple" onClick={(e) => this.setValues("removeArrayItem", index)}>{messages.array_elements_remove}</Button></Grid>
                            </Grid>}
                            {!this.props.disabled && <ActorChoice {...this.props.formElements.actor_type} index={index} setActor={this.setActor} />}                            
                        </div>
                    } else if (item.actor_type === "Person") {
                        // key={JSON.stringify(item ? item : "") + "_distribution_rights_holder_" + index + "_" + this.props.softwareDistributionItemIndex} 
                        return <div key={index}>
                            {!this.props.disabled && (item.surname === null || item.surname || (item.surname && item.surname["en"] === "")) &&
                                <Grid container  direction="row" justifyContent="flex-end" alignItems="baseline" >
                                    <Grid item><Button className="inner-link-default--purple" onClick={(e) => this.setValues("removeArrayItem", index)}>{messages.array_elements_remove}</Button></Grid>
                                    </Grid>}                                           
                                    <GenericPerson index={index} initialValue={item} updateModel={this.updateModel} />
                            <div className="mb1">
                                {(!this.props.disabled && !(item.surname && item.surname["en"] === "") && ((this.state.initialValueArray.length - 1) === index)) ? 
                                    <Grid container className="pb1 pt1" direction="row" justifyContent="flex-start" alignItems="baseline" >
                                        <Grid item><Button className="inner-link-default--purple" onClick={(e) => this.setValues("addArrayItem", index)}>{messages.array_elements_add}</Button></Grid>
                                    </Grid>: <div style={{ paddingBottom: "1em" }}></div>
                                }
                            </div>
                        </div>
                    } else if (item.actor_type === "Organization") {
                        // key={JSON.stringify(item ? item : "") + "_distribution_rights_holder_" + index + "_" + this.props.softwareDistributionItemIndex} 
                        return <div key={index}>
                            {!this.props.disabled && (item.organization_name === null || item.organization_name || (item.organization_name && item.organization_name["en"] === "")) &&
                                <Grid container  direction="row" justifyContent="flex-end" alignItems="baseline" >
                                    <Grid item><Button className="inner-link-default--purple" onClick={(e) => this.setValues("removeArrayItem", index)}>{messages.array_elements_remove}</Button></Grid>
                                </Grid>}                               
                            <GenericOrganization index={index} initialValue={item} updateModel={this.updateModel} />                            
                            <div className="mb1">
                                {(!this.props.disabled && !(item.organization_name && item.organization_name["en"] === "") && ((this.state.initialValueArray.length - 1) === index)) ? 
                                        <Grid container className="pb1 pt1" direction="row" justifyContent="flex-start" alignItems="baseline" >
                                        <Grid item><Button className="inner-link-default--purple" onClick={(e) => this.setValues("addArrayItem", index)}>{messages.array_elements_add}</Button></Grid>
                                        </Grid>
                                        : <div style={{ paddingBottom: "1em" }}></div>
                                }
                            </div>
                        </div>
                    } else if (item.actor_type === "Group") {
                        // key={JSON.stringify(item ? item : "") + "_distribution_rights_holder_" + index + "_" + this.props.softwareDistributionItemIndex} 
                        return <div key={index}>
                            {!this.props.disabled && (item.organization_name === null || item.organization_name || (item.organization_name && item.organization_name["en"] === "")) && <Grid container direction="row" justifyContent="flex-end" alignItems="baseline" >
                                    <Grid item><Button className="inner-link-default--purple" onClick={(e) => this.setValues("removeArrayItem", index)}>{messages.array_elements_remove}</Button></Grid>
                                    </Grid>}                                                                
                            <GenericGroup index={index} initialValue={item} updateModel={this.updateModel} />
                            <div className="mb1">
                                {(!this.props.disabled && !(item.organization_name && item.organization_name["en"] === "") && ((this.state.initialValueArray.length - 1) === index)) ? 
                                    <Grid container className="pb1 pt1" direction="row" justifyContent="flex-start" alignItems="baseline" >
                                        <Grid item><Button className="inner-link-default--purple" onClick={(e) => this.setValues("addArrayItem", index)}>{messages.array_elements_add}</Button></Grid>
                                    </Grid> : <div style={{ paddingBottom: "1em" }}></div>
                                }
                            </div>
                        </div>
                    }
                    return <div key={index}></div>
                })
            }
        </div>
    }
}