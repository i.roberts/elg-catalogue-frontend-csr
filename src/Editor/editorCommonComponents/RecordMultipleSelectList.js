import React from "react";
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
//import Input from '@material-ui/core/Input';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import Chip from '@material-ui/core/Chip';
import MenuItem from '@material-ui/core/MenuItem';

export default class RecordMultipleSelectList extends React.Component {
  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.default_valueArray.length !== nextProps.default_valueArray.length) {
      return true;
    }
    for (let index = 0; index < this.props.default_valueArray.length; index++) {
      if (this.props[index] !== nextProps[index]) {
        return true;
      }
    }
    return false;
  }
  render() {
    const { required, label, help_text, choices, disabled, default_valueArray = [],className } = this.props;
    return <div className="mb1"><span>
      <FormControl variant="outlined" required={required} className={className}>
        <InputLabel htmlFor="select-native-simple">{label}</InputLabel>
        <Select 
          labelId="demo-mutiple-chip-label"
          id="demo-mutiple-chip"
          multiple
          disabled={disabled}
          value={default_valueArray}
          onChange={(e) => this.props.setMultiplevalueSelections(choices.filter(item => e.target.value.includes(item.display_name)))}
          input={<OutlinedInput id="select-multiple-chip" />}
          renderValue={(selected) => (
            <div style={{ display: 'flex', flexWrap: 'wrap', margin: 2 }}>
              {selected.map((value, index) => (
                <Chip key={index} label={value} />
              ))}
            </div>
          )}
        >
          {choices.map((name, index) => (
            <MenuItem key={index} value={name.display_name} styles={{ maxHeight: 48 * 4.5 + 8, width: 250 }}>
              {name.display_name}
            </MenuItem>
          ))}
        </Select>
        <FormHelperText>{help_text}</FormHelperText>
      </FormControl></span>
    </div>
  }
}