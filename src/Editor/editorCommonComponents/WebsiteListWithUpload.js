import React from "react";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { ReactComponent as AddCircleOutlineIcon } from "./../../assets/elg-icons/editor/add-square.svg";
import { ReactComponent as RemoveCircleOutlineIcon } from "./../../assets/elg-icons/editor/remove-square.svg";
import validator from 'validator';
import Grid from '@material-ui/core/Grid';
import FilePondDocument from "./FilePondDocument";

//import Autocomplete from '@material-ui/lab/Autocomplete';

const websiteValidationError = "Enter a valid website e.g. https://www.example.com \n";
const options = { protocols: ['http', 'https', 'ftp'], require_tld: true, require_protocol: true, require_host: true, require_valid_protocol: true, allow_underscores: false, host_whitelist: false, host_blacklist: false, allow_trailing_dot: false, allow_protocol_relative_urls: false, disallow_auth: false }

export default class WebsiteList extends React.Component {
    constructor(props) {
        super(props);
        this.state = { websiteArray: props.default_value_Array || [], errorArray: [], stopWriting: true };
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            websiteArray: nextProps.default_value_Array || [],
            stopWriting: true,
            errorArray: []
        };
    }

    componentDidMount() {
        const { errorArray } = this.state;
        //const websiteArray = this.props.default_value_Array;
        const { websiteArray } = this.state;
        for (let index = 0; index < websiteArray.length; index++) {
            const valid = validator.isURL(websiteArray[index] || "", options);
            errorArray[index] = !valid;
        }
        this.setState({ errorArray, stopWriting: true });
    }

    setWebsite = (event, index, addRemoveWebsite) => {
        const { errorArray } = this.state;
        //const websiteArray = this.props.default_value_Array;
        const { websiteArray } = this.state;
        const action = event.target.name || addRemoveWebsite;
        switch (action) {
            case "websiteValue": websiteArray[index] = event.target.value; break;
            case "addWebsite":
                if (websiteArray.filter(site => site).length < websiteArray.length) {
                    return; //do not add a new website if there is an empty one
                }
                websiteArray.push("");
                errorArray.push("");
                break;
            case "removeWebsite":
                websiteArray.splice(index, 1);
                errorArray.splice(index, 1);
                this.setState({ websiteArray, errorArray }, this.blur);
                return;
            default: break;
        }
        this.setState({ websiteArray, errorArray, stopWriting: false });
        //this.props.updateModel_website("website", websiteArray);
        //this.props.updateModel_website(this.props.field || "website", websiteArray);
    }

    setDocument = (index, url) => {
        const { websiteArray } = this.state;
        websiteArray[index] = encodeURI(url);//index="samples_location"
        this.setState({ websiteArray }, this.blur);
    }

    blur = (e, index) => {
        const { errorArray } = this.state;
        //const websiteArray = this.props.default_value_Array;
        const { websiteArray } = this.state;
        const valid = validator.isURL(websiteArray[index] || "", options);
        errorArray[index] = !valid;
        this.setState({ errorArray, stopWriting: true });
        const filteredArray = websiteArray.filter(item => item);
        if (websiteArray.length === 1) {
            this.props.updateModel_website((this.props.field !== null && this.props.field !== undefined) ? this.props.field : "website", filteredArray);
            return;
        }
        this.props.updateModel_website((this.props.field !== null && this.props.field !== undefined) ? this.props.field : "website", filteredArray);
    }

    render() {
        //const{default_value_Array = []}=this.props;
        const { type, required, label, help_text, className, disable, placeholder } = this.props;

        const default_value_Array = this.state.websiteArray;
        const default_value = default_value_Array[0] || "";
        const hasError = default_value ? (!validator.isURL(default_value || "", options)) : false;
        return <div>
            <Grid container direction="row" justifyContent="space-between" alignItems="center" >
                <Grid item xs={(default_value) ? 11 : 9}>
                    <TextField className={className}
                        type={type}
                        error={hasError && this.state.stopWriting}
                        required={required}
                        disabled={disable || (default_value && default_value.indexOf("syseleven.net") >= 0)}
                        label={label}
                        placeholder={placeholder ? placeholder : ""}
                        variant="outlined"
                        helperText={hasError && this.state.stopWriting ? `${websiteValidationError}` : help_text}
                        inputProps={{ name: 'websiteValue' }}
                        value={default_value}
                        onChange={(e) => this.setWebsite(e, 0)}
                        onBlur={(e) => this.blur(e, 0)}
                    />
                </Grid>
                {!default_value ? <Grid item xs={2}>
                    <FilePondDocument model={this.props.model} index={0} setdocument={this.setDocument} />
                </Grid> :
                    <div></div>
                }
                <Grid item xs={1}>
                    <Button disabled={disable} onClick={(e) => this.setWebsite(e, 0, "addWebsite")}><AddCircleOutlineIcon className="small-icon" /></Button>
                    {default_value && <Button disabled={disable} onClick={(e) => this.setWebsite(e, 0, "removeWebsite")}><RemoveCircleOutlineIcon className="small-icon" /></Button>}
                </Grid>
            </Grid>
            {
                default_value_Array.map((item, index) => {
                    if (index === 0) {
                        return <div key={index}></div>
                    }
                    const hasError = item ? (!validator.isURL(item || "", options)) : false
                    return <Grid container key={index} direction="row" justifyContent="space-between" alignItems="center" >
                        <Grid item className="pt1" key={index} xs={item ? 11 : 9}>
                            <TextField className={className}
                                type={type}
                                error={hasError && this.state.stopWriting}
                                required={required}
                                disabled={disable || (item && item.indexOf("syseleven.net") >= 0)}
                                label={label}
                                placeholder={placeholder ? placeholder : ""}
                                variant="outlined"
                                helperText={hasError && this.state.stopWriting ? `${websiteValidationError}` : help_text}
                                inputProps={{ name: 'websiteValue' }}
                                value={item}
                                onChange={(e) => this.setWebsite(e, index)}
                                onBlur={(e) => this.blur(e, index)}
                            />
                        </Grid>
                        {!item ? <Grid item xs={2}>
                            <FilePondDocument model={this.props.model} index={index} setdocument={this.setDocument} />
                        </Grid> :
                            <div></div>
                        }

                        <Grid item xs={1}><Button disabled={disable} onClick={(e) => this.setWebsite(e, index, "removeWebsite")}><RemoveCircleOutlineIcon className="small-icon" /></Button></Grid>
                    </Grid>
                })
            }

        </div>
    }
}