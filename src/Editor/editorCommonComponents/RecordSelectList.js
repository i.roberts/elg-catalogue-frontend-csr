import React from "react";
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import { UNSPECIFIED } from "../../config/editorConstants";

export default class RecordSelectList extends React.Component {

  render() {
    let { required, label, help_text, default_value = "", choices, className, disabled = false } = this.props;
    const default_value_display_name = choices.filter(item => item.value === default_value).length > 0 ? choices.filter(item => item.value === default_value)[0].display_name : "";
    if (default_value_display_name !== "unspecified") {
      choices = choices.filter(item => item && item.value !== UNSPECIFIED);
    }
    return <div><span>
      <FormControl variant="outlined" required={required} className={className}>
        <InputLabel htmlFor="select-native-simple">{label}</InputLabel>
        <Select
          disabled={disabled}
          native
          value={default_value_display_name}
          onChange={(e) => { this.props.setSelectedvalue(e, choices.filter(item => item.display_name === e.target.value), this.props.field, this.props.lr_subclass) }}
          label={label}
          inputProps={{ name: 'value' }}
        >
          <option aria-label="None" value="" />
          {choices.map((item, index) => <option key={index} value={item.display_name}>{item.display_name}</option>)}
        </Select>
        <FormHelperText>{help_text}</FormHelperText>
      </FormControl>
    </span>
    </div>
  }
}