import React from "react";
import axios from "axios";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { domainObj, domain_identifier } from "../Models/OrganizationModel";
import Grid from '@material-ui/core/Grid';
import { LOOK_UP_DOMAIN } from "../../config/editorConstants";
//import Autocomplete, { createFilterOptions } from '@material-ui/lab/Autocomplete';
import Autocomplete from '@material-ui/lab/Autocomplete';
import parse from 'autosuggest-highlight/parse';
import match from 'autosuggest-highlight/match';
import IdentifierSingle from "./IdentifierSingle";
import LanguageSpecificText from "./LanguageSpecificText";
import CircularProgress from "@material-ui/core/CircularProgress";
import messages from "./../../config/messages";

//const filter = createFilterOptions();
const checkElg = obj => obj.domain_classification_scheme === "http://w3id.org/meta-share/meta-share/elg";

export default class DomainAutocomplete extends React.Component {

    constructor(props) {
        super(props);
        this.state = { domainInitialArray: this.props.default_valueArray || [], domainChoices: [], loading: false, source: null };
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            domainInitialArray: nextProps.default_valueArray || [],
        };
    }

    componentWillUnmount() {
        if (this.state.source) {
            if (this.state.source) {
                this.state.source.cancel("");
            }
        }
    }

    getDomainChoices = (inputDomain) => {
        if (inputDomain.trim().length <= 2) {
            //this.setState({ domainChoices: [] });
            return;
        }
        if (this.state.source) {
            this.state.source.cancel("");
        }
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        this.setState({ loading: true, source: source });
        axios.get(LOOK_UP_DOMAIN(inputDomain), { cancelToken: source.token })
            .then((res) => {
                this.setState({ domainChoices: res.data, loading: false, source: null })
            }).catch((err) => {
                console.log(err);
                this.setState({ loading: false, source: null, domainChoices: [] })
            });
    }

    setDomain = (action, domainIndex, autocompleteSelectedObj) => {
        const domain = this.state.domainInitialArray;
        var oldDomainObj = (domain && domain.length >= domainIndex) ? domain[domainIndex] : {};
        var { category_label = {}, domain_identifier = {} } = oldDomainObj || "";
        this.setState({ domainChoices: [] });
        switch (action) {
            case "categoryValue":
                if (!autocompleteSelectedObj) {
                    return;
                }
                category_label["en"] = autocompleteSelectedObj;
                if (oldDomainObj.hasOwnProperty('editor-placeholder')) {
                    delete oldDomainObj['editor-placeholder'];
                }
                break;
            case "addDomain":
                const filteredArray = domain.filter(item => {
                    if (item && item.category_label) {
                        return item.category_label["en"];
                    }
                    return false;
                });
                if (filteredArray.length !== domain.length) {
                    return;//do not add element if there are empty values
                }
                domain.push(JSON.parse(JSON.stringify(domainObj)));
                this.props.updateModel_Domain('domain', domain);
                return domain;
            case "removeDomain":
                domain.splice(domainIndex, 1);
                if (domain && domain.length === 0) {
                    this.props.updateModel_Domain('domain', []);
                    return;
                }
                this.props.updateModel_Domain('domain', domain);
                return domain;
            case "autoCompleteSelected":
                if (autocompleteSelectedObj.similarity) {
                    delete autocompleteSelectedObj["similarity"];
                }
                if (autocompleteSelectedObj.display_name) {
                    delete autocompleteSelectedObj["display_name"];
                }
                if (autocompleteSelectedObj.hasOwnProperty('editor-placeholder')) {
                    delete autocompleteSelectedObj['editor-placeholder'];
                }
                domain[domainIndex] = autocompleteSelectedObj;
                this.props.updateModel_Domain('domain', domain);
                return domain;
            default: break;
        }
        oldDomainObj = { ...oldDomainObj, category_label: category_label, domain_identifier: domain_identifier }
        domain[domainIndex] = oldDomainObj;
        this.props.updateModel_Domain('domain', domain);
        return domain;
    }

    setLanguageSpecificText = (domainIndex, obj) => {
        const domain = this.state.domainInitialArray;
        domain[domainIndex].category_label = obj;
        if (domain[domainIndex].hasOwnProperty('editor-placeholder')) {
            delete domain[domainIndex]['editor-placeholder'];
        }
        this.setState({ domainChoices: [] });
        this.props.updateModel_Domain('domain', domain);
    }

    updateIdentifier = (domainIndex, obj) => {
        const domain = this.state.domainInitialArray;
        domain[domainIndex].domain_identifier = obj;
        if (domain[domainIndex].hasOwnProperty('editor-placeholder')) {
            delete domain[domainIndex]['editor-placeholder'];
        }
        this.setState({ domainChoices: [] });
        this.props.updateModel_Domain('domain', domain);
    }

    autocompleteTextField = (domain, domainIndex, languageKey) => {
        const activeLanguage = this.props.activeLanguage || "en";
        return <span>
            <Autocomplete className={"wd-100"}
                freeSolo
                clearOnBlur
                autoHighlight
                loading={this.state.loading}
                value={domain || ""}
                onChange={(event, newValue) => {
                    if (!newValue) {
                        return;
                    }
                    if (typeof newValue === "object") {
                        this.setDomain("autoCompleteSelected", domainIndex, newValue);
                    } else {
                        this.setDomain("categoryValue", domainIndex, newValue);
                    }
                }}
                options={this.state.loading ? [] : this.state.domainChoices}
                filterOptions={(options, params) => {
                    //const filtered = filter(options, params);
                    const filtered = options;
                    if (!this.state.loading) {
                        if (params.inputValue !== '') {
                            /*filtered.push({
                                category_label: { 'en': params.inputValue },
                                display_name: `Missing  ${this.props.label}? Add "${params.inputValue}"`,
                            });*/
                            filtered.splice(0, 0, {
                                category_label: { 'en': params.inputValue },
                                display_name: `Missing ${params.inputValue} ? Add "${params.inputValue}"`,
                            });
                        }
                    }
                    if (filtered && filtered.length === 0 && this.state.loading) {
                        return [{ display_name: "Loading please wait...", }]
                    }
                    return filtered;
                }}
                //getOptionLabel={(option) => option.category_label ? Object.values(option.category_label)[0] : option}
                getOptionLabel={(option) => option.category_label ? Object.keys(option.category_label).includes("en") ? option.category_label["en"] : Object.values(option.category_label)[0] : ""}
                renderInput={(params) => (
                    <TextField {...params} label={this.props.category_label} helperText={this.props.category_help_text} placeholder={this.props.category_placeholder ? this.props.category_placeholder : ""} variant="outlined"
                        //onBlur={(e) => { this.setDomain("categoryValue", domainIndex, e.target.value); }}
                        onChange={(e) => { this.getDomainChoices(e.target.value) }}
                        InputProps={{
                            ...params.InputProps,
                            endAdornment: (
                                <React.Fragment>
                                    {this.state.loading ? (
                                        <CircularProgress color="inherit" size={20} />
                                    ) : null}
                                    {params.InputProps.endAdornment}
                                </React.Fragment>
                            )
                        }}
                    />)}
                renderOption={(option, { inputValue }) => {
                    if (option.display_name) {
                        const matches1 = match(option.display_name, inputValue);
                        const parts1 = parse(option.display_name, matches1);
                        return parts1.map((part, index) => (
                            <span key={index} style={{ fontWeight: part.highlight ? 1000 : 400 }}>
                                {part.highlight ? '\u00A0' : ''}{part.text}
                            </span>
                        ))
                    }
                    //const matches = match(Object.values(option.category_label)[0], inputValue);
                    //const parts = parse(Object.values(option.category_label)[0], matches);
                    const matches = match(option.category_label[activeLanguage] || Object.values(option.category_label)[0], inputValue);
                    const parts = parse(option.category_label[activeLanguage] || Object.values(option.category_label)[0], matches);
                    return (
                        <div>
                            {
                                option.category_label && <div>
                                    {parts.map((part, index) => (
                                        <span key={index} style={{ fontWeight: part.highlight ? 1000 : 400 }}>
                                            {part.text}
                                        </span>
                                    ))}
                                </div>
                            }
                            {
                                option.domain_identifier && <div>
                                    {option.domain_identifier.domain_classification_scheme === "http://w3id.org/meta-share/meta-share/ELG_domainClassification" ? "" :
                                        (`${option.domain_identifier.value} - ${this.props.domain_choices.filter(domainChoice => domainChoice.value === option.domain_identifier.domain_classification_scheme)[0].display_name}`)
                                    }
                                </div>
                            }
                        </div>
                    );
                }
                }
            />
            <div className="pt-3 form-block-area">
                {/*{<Button className="inner-link-outlined--purple" onClick={(e) => this.setDomain("addDomain", domainIndex)}><AddCircleOutlineIcon className="xsmall-icon" /></Button>}*/}
                {domainIndex >= 1 && <Grid container className="pb1" direction="row" justifyContent="flex-end" alignItems="baseline" >
                    <Grid item>{<Button className="inner-link-default--purple" onClick={(e) => this.setDomain("removeDomain", domainIndex)}>{messages.array_elements_remove}</Button>}</Grid>
                </Grid>
                }
            </div>
        </span>
    }

    onBlur = () => {
        this.setState({ domainChoices: [], loading: false });
        if (this.state.domainInitialArray.length === 0) {
            this.props.updateModel_Domain('domain', []);
        } else if (this.state.domainInitialArray.length === 1) {
            if (!this.state.domainInitialArray[0].hasOwnProperty('pk')) {
                if (this.state.domainInitialArray[0].category_label && !this.state.domainInitialArray[0].category_label["en"]) {
                    this.props.updateModel_Domain('domain', []);
                }
            }
        }

    }

    render() {
        const {
            label, help_text,
            category_help_text, category_label, category_required, category_type, category_placeholder,
            domain_choices, domain_help_text, domain_label, domain_read_only, domain_required,
            value_label, value_required, value_placeholder, value_help_text = "",
            domain_identifier_label, domain_identifier_help_text
        } = this.props;
        if (this.state.domainInitialArray.length === 0) {
            this.state.domainInitialArray.push(JSON.parse(JSON.stringify(domainObj)));
        }
        return <div onBlur={this.onBlur}>
            <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1}>
                <Grid item sm={10}>
                    <Typography variant="h3" className="section-links" >{label} </Typography>
                    <Typography className="section-links" >{help_text} </Typography>
                </Grid>
                <Grid item sm={1}>
                    {this.state.domainInitialArray.length === 0 && <Button className="inner-link-default--purple" onClick={(e) => this.setDomain("addDomain", 0)}>{messages.array_elements_add}</Button>}
                </Grid>
            </Grid>
            {
                this.state.domainInitialArray.map((domain, domainIndex) => {
                    const hide_help_text = domain.hasOwnProperty("pk") ? true : false;
                    return <div key={domainIndex}>
                        {//||!domain.hasOwnProperty("pk")
                            domain.category_label && !Object.values(domain.category_label)[0] ? this.autocompleteTextField(domain, domainIndex) :
                                <>
                                    <div className="pb-3 inner--group nested--group">
                                        <Grid container className="pb1" direction="row" justifyContent="flex-end" alignItems="baseline" >
                                            <Grid item>{<Button className="inner-link-default--purple" onClick={(e) => this.setDomain("removeDomain", domainIndex)}>{"Remove"}</Button>}</Grid>
                                        </Grid>
                                        <div>
                                            <LanguageSpecificText
                                                defaultValueObj={domain.category_label || { "en": "" }}
                                                field={domainIndex}//don't care about category_label, instead track domainIndex
                                                type={category_type}
                                                required={category_required}
                                                label={category_label}
                                                help_text={hide_help_text ? '' : category_help_text}
                                                placeholder={category_placeholder ? category_placeholder : ""}
                                                disable={domain.pk ? true : false}
                                                setLanguageSpecificText={this.setLanguageSpecificText}
                                            />
                                        </div>
                                        <div>
                                            {(domain.pk && (!domain.domain_identifier || (domain.domain_identifier.length === 1 && domain.domain_identifier.some(checkElg))))
                                                ?
                                                (<></>)
                                                :
                                                (
                                                    <IdentifierSingle
                                                        initialIdentifierObj={domain.domain_identifier || {}}
                                                        field={domainIndex}
                                                        Identifier_Label={domain_identifier_label}
                                                        Identifier_help_text={hide_help_text ? '' : domain_identifier_help_text}
                                                        identifier_scheme_choices={domain_choices}
                                                        identifier_scheme_help_text={hide_help_text ? '' : domain_help_text}
                                                        identifier_scheme_label={domain_label}
                                                        identifier_scheme_read_only={domain_read_only}
                                                        identifier_scheme_required={domain_required}
                                                        identifier_scheme_type={""}
                                                        identifier_value_label={value_label}
                                                        identifier_value_required={value_required}
                                                        identifier_value_type={""}
                                                        identifier_value_placeholder={hide_help_text ? '' : value_placeholder ? value_placeholder : ""}
                                                        identifier_value_read_only={domain_read_only}
                                                        identifier_value_helptext={hide_help_text ? '' : value_help_text}
                                                        classification_scheme="domain_classification_scheme"
                                                        scheme_choice_to_hide="ELG domain classification"
                                                        scheme_choice_uri_to_hide="http://w3id.org/meta-share/meta-share/ELG_domainClassification"
                                                        disable={domain.pk ? true : false}
                                                        identifier_obj={domain_identifier}
                                                        updateModel_Identifier={this.updateIdentifier}
                                                    />)}
                                        </div>


                                        <div className="pt-3 form-block-area">
                                            {((domain.category_label !== "") && ((this.state.domainInitialArray.length - 1) === domainIndex)) ?
                                                <Grid container className="pb1" direction="row" justifyContent="flex-start" alignItems="baseline" >
                                                    <Grid item><Button className="inner-link-default--purple" onClick={(e) => this.setDomain("addDomain", domainIndex)}>{messages.array_elements_add}</Button></Grid>
                                                </Grid>
                                                : <div style={{ paddingBottom: "1em" }}></div>
                                            }
                                        </div>

                                    </div>
                                </>
                        }
                        <div style={{ marginTop: "1em" }} />
                    </div>
                })
            }

        </div >


    }
}