import React from "react";
import axios from "axios";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { subjectObj, subject_identifier } from "../Models/GenericModels";
import Grid from '@material-ui/core/Grid';
import CircularProgress from "@material-ui/core/CircularProgress";
import { LOOK_UP_SUBJECT } from "../../config/editorConstants";
//import Autocomplete, { createFilterOptions } from '@material-ui/lab/Autocomplete';
import Autocomplete from '@material-ui/lab/Autocomplete';
import parse from 'autosuggest-highlight/parse';
import match from 'autosuggest-highlight/match';
import IdentifierSingle from "./IdentifierSingle";
import LanguageSpecificText from "../editorCommonComponents/LanguageSpecificText";
import messages from "./../../config/messages";
//const filter = createFilterOptions();
const checkElg = obj => obj.subject_classification_scheme === "http://w3id.org/meta-share/meta-share/elg";
export default class RecordSubjectAutocomplete extends React.Component {

    constructor(props) {
        super(props);
        this.state = { subjectInitialArray: this.props.default_valueArray || [], subjectChoices: [], loading: false, source: null };
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            subjectInitialArray: nextProps.default_valueArray || [],
        };
    }

    componentWillUnmount() {
        if (this.state.source) {
            if (this.state.source) {
                this.state.source.cancel("");
            }
        }
    }

    getSubjectChoices = (inputSubject) => {
        if (inputSubject.trim().length <= 2) {
            //this.setState({ subjectChoices: []});
            return;
        }
        if (this.state.source) {
            this.state.source.cancel("");
        }
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        this.setState({ loading: true, source: source });
        axios.get(LOOK_UP_SUBJECT(inputSubject), { cancelToken: source.token })
            .then((res) => {
                this.setState({ subjectChoices: res.data, loading: false, source: null })
            }).catch((err) => {
                console.log(err);
                this.setState({ loading: false, source: null, subjectChoices: [] })
            });
    }

    setSubject = (action, subjectIndex, autocompleteSelectedObj) => {
        const subject = this.state.subjectInitialArray;
        var oldsubjectObj = (subject && subject.length >= subjectIndex) ? subject[subjectIndex] : {};
        var { category_label = {}, subject_identifier = {} } = oldsubjectObj || "";
        this.setState({ subjectChoices: [] });
        switch (action) {
            case "categoryValue":
                if (!autocompleteSelectedObj) {
                    return;
                }
                category_label["en"] = autocompleteSelectedObj;
                if (oldsubjectObj.hasOwnProperty('editor-placeholder')) {
                    delete oldsubjectObj['editor-placeholder'];
                }
                break;
            case "addsubject":
                const filteredArray = subject.filter(item => {
                    if (item && item.category_label) {
                        return item.category_label["en"];
                    }
                    return false;
                });
                if (filteredArray.length !== subject.length) {
                    return;//do not add element if there are empty values
                }
                subject.push(JSON.parse(JSON.stringify(subjectObj)));
                this.props.updateModel_Subject('subject', subject);
                return subject;
            case "removesubject":
                subject.splice(subjectIndex, 1);
                if (subject.length === 0) {
                    this.props.updateModel_Subject('subject', []);
                } else {
                    this.props.updateModel_Subject('subject', subject);
                }
                return subject;
            case "autoCompleteSelected":
                if (autocompleteSelectedObj.similarity) {
                    delete autocompleteSelectedObj["similarity"];
                }
                if (autocompleteSelectedObj.display_name) {
                    delete autocompleteSelectedObj["display_name"];
                }
                if (autocompleteSelectedObj.hasOwnProperty('editor-placeholder')) {
                    delete autocompleteSelectedObj['editor-placeholder'];
                }
                subject[subjectIndex] = autocompleteSelectedObj;
                this.props.updateModel_Subject('subject', subject);
                return subject;
            default: break;
        }
        oldsubjectObj = { ...oldsubjectObj, category_label: category_label, subject_identifier: subject_identifier }
        subject[subjectIndex] = oldsubjectObj;
        this.props.updateModel_Subject('subject', subject);
        return subject;
    }


    setLanguageSpecificText = (subjectIndex, obj) => {
        const subject = this.state.subjectInitialArray;
        subject[subjectIndex].category_label = obj;
        if (subject[subjectIndex].hasOwnProperty('editor-placeholder')) {
            delete subject[subjectIndex]['editor-placeholder'];
        }
        this.setState({ subjectChoices: [] });
        this.props.updateModel_Subject('subject', subject);
    }

    updateIdentifier = (subjectIndex, obj) => {
        const subject = this.state.subjectInitialArray;
        subject[subjectIndex].subject_identifier = obj;
        if (subject[subjectIndex].hasOwnProperty('editor-placeholder')) {
            delete subject[subjectIndex]['editor-placeholder'];
        }
        this.setState({ subjectChoices: [] });
        this.props.updateModel_Subject('subject', subject);
    }


    autocompleteTextField = (subject, subjectIndex, languageKey) => {
        const activeLanguage = this.props.activeLanguage || "en";
        return <span>
            <Autocomplete className={"wd-100"}
                freeSolo
                clearOnBlur
                autoHighlight
                loading={this.state.loading}
                value={subject || ""}
                onChange={(event, newValue) => {
                    if (!newValue) {
                        return;
                    }
                    if (typeof newValue === "object") {
                        this.setSubject("autoCompleteSelected", subjectIndex, newValue);
                    } else {
                        this.setSubject("categoryValue", subjectIndex, newValue);
                    }
                }}
                options={this.state.loading ? [] : this.state.subjectChoices}
                filterOptions={(options, params) => {
                    //const filtered = filter(options, params);
                    const filtered = options;
                    if (!this.state.loading) {
                        if (params.inputValue !== '') {
                            /*filtered.push({
                                category_label: { 'en': params.inputValue },
                                display_name: `Missing ${this.props.label}? Add "${params.inputValue}"`,
                            });*/
                            filtered.splice(0, 0, {
                                category_label: { 'en': params.inputValue },
                                display_name: `Missing ${params.inputValue}? Add "${params.inputValue}"`,
                            });
                        }
                    }
                    if (filtered && filtered.length === 0 && this.state.loading) {
                        return [{ display_name: "Loading please wait...", }]
                    }
                    return filtered;
                }}
                //getOptionLabel={(option) => option.category_label ? Object.values(option.category_label)[0] : option}
                getOptionLabel={(option) => option.category_label ? Object.keys(option.category_label).includes("en") ? option.category_label["en"] : Object.values(option.category_label)[0] : ""}
                renderInput={(params) => (
                    <TextField {...params} label={this.props.category_label} help_text={this.props.category_help_text} variant="outlined" placeholder={this.props.category_placeholder ? this.props.category_placeholder : ""}
                        //onBlur={(e) => { this.setSubject("categoryValue", subjectIndex, e.target.value); }}
                        onChange={(e) => { this.getSubjectChoices(e.target.value) }}
                        InputProps={{
                            ...params.InputProps,
                            endAdornment: (
                                <React.Fragment>
                                    {this.state.loading ? (
                                        <CircularProgress color="inherit" size={20} />
                                    ) : null}
                                    {params.InputProps.endAdornment}
                                </React.Fragment>
                            )
                        }}
                    />)}
                renderOption={(option, { inputValue }) => {
                    if (option.display_name) {
                        const matches1 = match(option.display_name, inputValue);
                        const parts1 = parse(option.display_name, matches1);
                        return parts1.map((part, index) => (
                            <span key={index} style={{ fontWeight: part.highlight ? 1000 : 400 }}>
                                {part.highlight ? '\u00A0' : ''}{part.text}
                            </span>
                        ))
                    }
                    //const matches = match(Object.values(option.category_label)[0], inputValue);
                    //const parts = parse(Object.values(option.category_label)[0], matches);
                    const matches = match(option.category_label[activeLanguage] || Object.values(option.category_label)[0], inputValue);
                    const parts = parse(option.category_label[activeLanguage] || Object.values(option.category_label)[0], matches);
                    return (
                        <div>
                            {
                                option.category_label && <div>
                                    {parts.map((part, index) => (
                                        <span key={index} style={{ fontWeight: part.highlight ? 1000 : 400 }}>
                                            {part.text}
                                        </span>
                                    ))}
                                </div>
                            }
                            {
                                option.subject_identifier && <div>
                                    {option.subject_identifier.subject_classification_scheme === "http://w3id.org/meta-share/meta-share/ELG_subjectClassification" ? "" :
                                        (`${option.subject_identifier.value} - ${this.props.subject_choices.filter(subjectChoice => subjectChoice.value === option.subject_identifier.subject_classification_scheme)[0].display_name}`)
                                    }
                                </div>
                            }
                        </div>
                    );
                }
                }
            />
            <div className="pt-3 form-block-area">
                {/* {<Button className="inner-link-outlined--purple" onClick={(e) => this.setSubject("addsubject", subjectIndex)}><AddCircleOutlineIcon className="xsmall-icon" /></Button>}*/}
                {subjectIndex >= 1 && <Grid container className="pb1" direction="row" justifyContent="flex-end" alignItems="baseline" >
                    <Grid item>{<Button className="inner-link-default--purple" onClick={(e) => this.setSubject("removesubject", subjectIndex)}>{messages.array_elements_remove}</Button>}</Grid>
                </Grid>
                }
            </div>

        </span>
    }

    onBlur = () => {
        this.setState({ subjectChoices: [], loading: false });
        if (this.state.subjectInitialArray.length === 0) {
            this.props.updateModel_Subject('subject', []);
        } else if (this.state.subjectInitialArray.length === 1) {
            if (!this.state.subjectInitialArray[0].hasOwnProperty('pk')) {
                if (this.state.subjectInitialArray[0].category_label && !this.state.subjectInitialArray[0].category_label["en"]) {
                    this.props.updateModel_Subject('subject', []);
                }
            }
        }

    }

    render() {
        const {
            label, help_text,
            category_help_text, category_label, category_required, category_type, category_placeholder,
            subject_choices, subject_help_text, subject_label, subject_read_only, subject_required,
            value_label, value_required, value_placeholder, value_help_text,
            subject_identifier_label, subject_identifier_help_text
        } = this.props;
        if (this.state.subjectInitialArray.length === 0) {
            this.state.subjectInitialArray.push(JSON.parse(JSON.stringify(subjectObj)));
        }
        return <div onBlur={this.onBlur}>
            <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1}>
                <Grid item sm={10}>
                    <Typography variant="h3" className="section-links" >{label} </Typography>
                    <Typography className="section-links" >{help_text} </Typography>
                </Grid>
                <Grid item sm={1}>
                    {this.state.subjectInitialArray.length === 0 && <Button className="inner-link-default--purple" onClick={(e) => this.setSubject("addsubject", 0)}>{messages.array_elements_add}</Button>}
                </Grid>
            </Grid>
            {
                this.state.subjectInitialArray.map((subject, subjectIndex) => {
                    const hide_help_text = subject.hasOwnProperty("pk") ? true : false;
                    return <div key={subjectIndex}>
                        {
                            subject.category_label && !Object.values(subject.category_label)[0] ? this.autocompleteTextField(subject, subjectIndex) :
                                <div className="pb-3 inner--group nested--group">
                                    <Grid container className="pb1" direction="row" justifyContent="flex-end" alignItems="baseline" >
                                        <Grid item>{<Button className="inner-link-default--purple" onClick={(e) => this.setSubject("removesubject", subjectIndex)}>{messages.array_elements_remove}</Button>}</Grid>
                                    </Grid>
                                    <div>
                                        <LanguageSpecificText
                                            defaultValueObj={subject.category_label || { "en": "" }}
                                            field={subjectIndex}//don't care about category_label, insead track subjectIndex
                                            type={category_type}
                                            required={category_required}
                                            label={category_label}
                                            help_text={hide_help_text ? '' : category_help_text}
                                            placeholder={category_placeholder ? category_placeholder : ""}
                                            disable={subject.pk ? true : false}
                                            setLanguageSpecificText={this.setLanguageSpecificText}
                                        />
                                    </div>
                                    <div>
                                        {(subject.pk && (!subject.subject_identifier || (subject.subject_identifier.length === 1 && subject.subject_identifier.some(checkElg))))
                                            ?
                                            (<></>)
                                            :
                                            (
                                                <IdentifierSingle
                                                    initialIdentifierObj={subject.subject_identifier || {}}
                                                    field={subjectIndex}
                                                    Identifier_Label={subject_identifier_label}
                                                    Identifier_help_text={hide_help_text ? '' : subject_identifier_help_text}
                                                    identifier_scheme_choices={subject_choices}
                                                    identifier_scheme_help_text={hide_help_text ? '' : subject_help_text}
                                                    identifier_scheme_label={subject_label}
                                                    identifier_scheme_read_only={subject_read_only}
                                                    identifier_scheme_required={subject_required}
                                                    identifier_scheme_type={""}
                                                    identifier_value_label={value_label}
                                                    identifier_value_required={value_required}
                                                    identifier_value_type={""}
                                                    identifier_value_placeholder={hide_help_text ? '' : value_placeholder ? value_placeholder : ""}
                                                    identifier_value_helptext={hide_help_text ? '' : value_help_text}
                                                    identifier_value_read_only={subject_read_only}
                                                    classification_scheme="subject_classification_scheme"
                                                    scheme_choice_to_hide="ELG subject classification"
                                                    scheme_choice_uri_to_hide="http://w3id.org/meta-share/meta-share/ELG_subjectClassification"
                                                    disable={subject.pk ? true : false}
                                                    identifier_obj={subject_identifier}
                                                    updateModel_Identifier={this.updateIdentifier}
                                                />)}
                                    </div>


                                    <div className="pt-3 form-block-area">
                                        {((subject.category_label !== "") && ((this.state.subjectInitialArray.length - 1) === subjectIndex)) ?
                                            <Grid container className="pb1" direction="row" justifyContent="flex-start" alignItems="baseline" >
                                                <Grid item><Button className="inner-link-default--purple" onClick={(e) => this.setSubject("addsubject", subjectIndex)}>{messages.array_elements_add}</Button></Grid>
                                            </Grid>
                                            : <div style={{ paddingBottom: "1em" }}></div>
                                        }
                                    </div>
                                </div>
                        }
                        <div style={{ marginTop: "1em" }} />
                    </div>
                })
            }

        </div >


    }
}