import React from "react";
import TextField from '@material-ui/core/TextField';

export default function FreeText(props) {
    const [value, setSelectedValue] = React.useState(props.initialValue ? props.initialValue : "");

    React.useEffect(() => {
        setSelectedValue(props.initialValue || "")
    }, [props.initialValue])

    const onblur = () => {
        props.updateModel(props.field, value, props.lr_subclass);
    }

    return <div className="mb2">
        <TextField className={props.className}
            //error={props.required && ((value !== null || value !== undefined) ? !value : true)}
            multiline={props.multiline}
            minRows={props.maxRows ? props.maxRows : void 0}
            maxRows={props.maxRows ? props.maxRows : void 0}
            type={props.type}
            required={props.required}
            label={props.label}
            placeholder={props.placeholder ? props.placeholder : ""}
            variant="outlined"
            helperText={props.help_text}
            value={value}
            disabled={props.disabled}
            inputProps={{ maxLength: props.maxLength || 99999999999 }}
            onChange={(e) => setSelectedValue(e.target.value)}
            onBlur={onblur}
        />
    </div>
}