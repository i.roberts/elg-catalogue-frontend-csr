import React from "react";
import axios from "axios";
import { FilePond, registerPlugin } from 'react-filepond';
import 'filepond/dist/filepond.min.css';
import { PROXY_2_S3, getAuthorizationHeader } from "../../config/constants";
import { toast } from "react-toastify";
import { keycloak } from "../../App";
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.css';
registerPlugin(FilePondPluginImagePreview);
export default class FilePondDocument extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            files: []
        };
    }

    processS3Upload = (fieldName, file, metadata, load, error, progress, abort, transfer, options) => {
        const formData = new FormData();
        formData.append("file", file, file.name);
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();

        axios.put(PROXY_2_S3, file, {
            headers: {
                'Content-Type': file.type ? file.type : 'application/octet-stream',
                'Authorization': getAuthorizationHeader(keycloak),
                'ELG-RESOURCE-ID': this.props.model && this.props.model.pk ? this.props.model.pk : 0,
                'scope': "public",
                'filetype': "document",
                'x-amz-acl': "public-read",
                'filename': encodeURI(file.name)
                //'filename': file.name
            },
            cancelToken: source.token,
            progress: (e) => {
                progress(e.lengthComputable, e.loaded, e.total);
            }
        }).then(response => {
            load();
            this.setState({ files: [] });
            this.handleS3Proxy(response);
        }).catch(err => {
            console.log("error, ", err);
            this.setState({ files: [] });
            if (axios.isCancel(err)) {
                console.log('Request canceled', err.message);
            } else {
            } this.handleS3ProxyError(err)
        });
        return {
            abort: () => {
                source.cancel('Operation canceled by the user.');
                abort();
            }
        };
    }

    handleS3Proxy = (response) => {
        toast.success("File has been successfully uploaded.", { autoClose: 3500 });
        const { headers = "" } = response;
        const location = headers ? headers.location || "" : "";
        this.props.setdocument(this.props.index, location);
    }

    handleS3ProxyError = (responseError) => {
        toast.error("File upload failed.", { autoClose: 3500 });
        console.log(JSON.stringify(responseError));
    }

    handleInit = () => {
        //console.log("FilePond instance has initialised", this.pond);
    }

    render() {
        return <FilePond
            credits={false}
            stylePanelLayout="circle"
            ref={ref => (this.pond = ref)}
            files={this.state.files}
            name="file"
            allowMultiple={false}
            allowDrop={false}
            labelIdle={'<span class="filepond--label-action"> Browse </span>'}
            server={
                {
                    url: PROXY_2_S3,
                    process: (fieldName, file, metadata, load, error, progress, abort, transfer, options) => { this.processS3Upload(fieldName, file, metadata, load, error, progress, abort, transfer, options) },
                    revert: null,
                    load: null,
                    restore: null,
                    fetch: null,
                }
            }
            onupdatefiles={fileItems => {
                this.setState({
                    files: fileItems.map(fileItem => fileItem.file)
                });
            }}
        //oninit={() => this.handleInit()}
        >
        </FilePond>
    }
}