import React from "react";
import TextField from '@material-ui/core/TextField';
import validator from 'validator';

const emailValidationError = "Enter a valid email address e.g. example@elg.eu \n";

export default class Email extends React.Component {
    constructor(props) {
        super(props);
        this.state = { email: props.default_value || "", stopWriting: true };
    }

    setEmail = (value) => {
        this.setState({ email: value });
        //this.props.updateModel_email((this.props.field !== null && this.props.field !== undefined) ? this.props.field : "email", this.state.email);
    }

    blur = () => {
        this.setState({ stopWriting: true });
        this.props.updateModel_email((this.props.field !== null && this.props.field !== undefined) ? this.props.field : "email", this.state.email);
    }

    render() {
        const { type, required, label, help_text, className, disable, placeholder } = this.props;;
        let hasError = this.state.email ? (!validator.isEmail(this.state.email || "")) : false;
        return <span className="mb2">
            <TextField className={className}
                type={type}
                error={hasError}
                required={required}
                placeholder={placeholder ? placeholder : ""}
                label={label}
                variant="outlined"
                helperText={hasError && this.state.stopWriting ? `${emailValidationError}` : help_text}
                disabled={disable}
                value={this.state.email}
                onChange={(e) => this.setEmail(e.target.value)}
                onBlur={(e) => this.blur()}
            />
        </span>
    }
}