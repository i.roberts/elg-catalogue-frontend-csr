import React from "react";
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Website from "./Website";
import Email from "./Email";
import { additional_info_obj } from "../Models/LrModel";
import { ReactComponent as AddCircleOutlineIcon } from "./../../assets/elg-icons/editor/add-square.svg";
import { ReactComponent as RemoveCircleOutlineIcon } from "./../../assets/elg-icons/editor/remove-square.svg";
import Radio from '@material-ui/core/Radio';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';

export default class AdditionalInfoArray extends React.Component {
    constructor(props) {
        super(props);
        this.state = { initialValuesArray: this.props.initialValuesArray || [], };
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            initialValuesArray: nextProps.initialValuesArray || [],
        };
    }

    setElement = (index, action) => {
        const { initialValuesArray } = this.state;
        switch (action) {
            case "addElement":
                if (initialValuesArray.filter(item => item.email || item.landing_page).length !== initialValuesArray.length) {
                    return; //do not add element if there are blank ones
                }
                const item = JSON.parse(JSON.stringify(additional_info_obj));
                item.selection = "landing_page";
                initialValuesArray.push(JSON.parse(JSON.stringify(item)));
                break;
            case "removeElement":
                initialValuesArray.splice(index, 1);
                break;
            default: break;
        }
        this.setState({ initialValuesArray });
    }

    updateModel_email = (index, value) => {
        const { initialValuesArray } = this.state;
        const item = initialValuesArray[index];
        item.email = value || "";
        item.landing_page = null;
        if (item.email === null) {
            item.email = "";
        }
        initialValuesArray[index] = item;
        this.setState({ initialValuesArray });
    }

    updateModel_website = (index, value) => {
        const { initialValuesArray } = this.state;
        const item = initialValuesArray[index];
        item.landing_page = value || "";
        item.email = null;
        if (item.landing_page === null) {
            item.landing_page = "";
        }
        initialValuesArray[index] = item;
        this.setState({ initialValuesArray });
    }

    handleChange = (e, index) => {
        const { initialValuesArray } = this.state;
        const item = initialValuesArray[index];
        switch (e.target.value) {
            case "email":
                item.email = "";
                item.landing_page = null;
                item.selection = "email";
                break;
            case "landing_page":
                item.landing_page = "";
                item.email = null;
                item.selection = "landing_page";
                break;
            default:
                break;
        }
        initialValuesArray[index] = item;
        this.setState({ initialValuesArray });
    }

    onBlur = () => {
        let { initialValuesArray } = this.state;
        initialValuesArray = initialValuesArray.map(item => {
            if (item.selection) {
                // delete item["selection"];
            }
            return item;
        });
        this.props.updateModel(this.props.field, initialValuesArray);
        //this.props.updateModel(this.props.field, this.state.initialValuesArray);
    }

    render() {
        let { initialValuesArray } = this.state;
        if (initialValuesArray.length === 0) {
            initialValuesArray.push(JSON.parse(JSON.stringify(additional_info_obj)));
            initialValuesArray[0].selection = "landing_page";
        }
        initialValuesArray = initialValuesArray.map(item => {
            item.email = item.email === "" ? null : item.email;
            item.landing_page = item.landing_page === "" ? null : item.landing_page;
            if (item.email === item.landing_page === null) {
                item.landing_page = "";
            }
            if (item.email === item.landing_page === undefined) {
                item.landing_page = "";
            }
            if (item.email !== null && item.email !== undefined) {
                item.selection = "email";
            } else if (item.landing_page !== null && item.landing_page !== undefined) {
                item.selection = "landing_page";
            }
            return item;
        })

        return <div className="pb-3 ">
            <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1} >
                <Grid item sm={11}>
                    <Typography variant="h3" className="section-links" >{this.props.label}</Typography>
                    <Typography className="section-links" >{this.props.help_text} </Typography>
                </Grid>
            </Grid>
            <Grid container direction="row" alignItems="flex-start" spacing={2} className="vertical-tabs-inner">
                <Grid item xs>
                    {this.state.initialValuesArray.map((item, index) => {
                        if (item.pk >= 0) {
                            if (!item.selection) {
                                item.selection = item.email ? "email" : "landing_page";
                            }
                        }
                        return <div key={index + "_" + JSON.stringify(item)} onBlur={this.onBlur}>
                            <Grid container direction="row" alignItems="flex-start" spacing={2} className="vertical-tabs-inner">
                            <Grid item xs={10}>   
                            <FormControlLabel
                                value="landing_page"
                                label="landing_page"
                                control={<Radio
                                    checked={item.selection === "landing_page"}
                                    onChange={(e) => this.handleChange(e, index)}
                                    value="landing_page"
                                    name="landing_page"
                                />
                                }
                            />
                            <FormControlLabel
                                value="email"
                                label="email"
                                control={
                                    <Radio
                                        checked={item.selection === "email"}
                                        onChange={(e) => this.handleChange(e, index)}
                                        value="email"
                                        name="email"
                                    />
                                }
                            />
                            <div>
                                {item.selection === "email" && <Email
                                    className="wd-100"
                                    {...this.props.formElements.email}
                                    required={true}
                                    default_value={item.email || ""}
                                    field={index}//track index instead of field
                                    updateModel_email={this.updateModel_email}
                                />}
                            </div>
                            <div>
                                {item.selection === "landing_page" && <Website
                                    className="wd-100"
                                    {...this.props.formElements.landing_page}
                                    required={true}
                                    website={item.landing_page || ""}
                                    field={index}//track index instead of field
                                    updateModel_website={this.updateModel_website}
                                />
                                }
                            </div>
                            </Grid> 
                            <Grid item xs={2}>
                                <Button onClick={(e) => this.setElement(index, "addElement")}><AddCircleOutlineIcon className="small-icon" /></Button>
                                <Button onClick={(e) => this.setElement(index, "removeElement")}><RemoveCircleOutlineIcon className="small-icon" /></Button>
                            </Grid>

                            </Grid>
                        </div>
                    })}
                </Grid>
            </Grid>
        </div>
    }
}