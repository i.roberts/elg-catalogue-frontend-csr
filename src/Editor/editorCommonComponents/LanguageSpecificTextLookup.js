import React from "react";
import axios from "axios";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import CircularProgress from "@material-ui/core/CircularProgress";
import Autocomplete, { createFilterOptions } from '@material-ui/lab/Autocomplete';
import parse from 'autosuggest-highlight/parse';
import match from 'autosuggest-highlight/match';
import { LOOK_UP_KEYWORD, LOOK_UP_SERVICE_OFFERED } from "../../config/editorConstants";
import { ReactComponent as AddCircleOutlineIcon } from "./../../assets/elg-icons/editor/add-square.svg";
import { ReactComponent as RemoveCircleOutlineIcon } from "./../../assets/elg-icons/editor/remove-square.svg";
import EulanguageSelection from "./EuLanguagesSelection";
import Tooltip from '@material-ui/core/Tooltip';
import messages from "../../config/messages";

const filter = createFilterOptions();

export default class LanguageSpecificTextLookup extends React.Component {

    constructor(props) {
        super(props);
        this.state = { stateObj: this.props.defaultValueObj ? this.props.defaultValueObj : { "en": "" }, errorObj: { "en": "" }, options: [], loading: false, source: null };
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            stateObj: nextProps.defaultValueObj ? nextProps.defaultValueObj : { "en": "" },
            errorObj: { "en": "" }
        };
    }

    componentWillUnmount() {
        if (this.state.source) {
            this.state.source.cancel("");
        }
    }

    setValues = (event, langIndex, action2Perform, languageKey, oldLanguageValueObj, value) => {
        const obj = this.state.stateObj;
        const action = event.target.name || action2Perform;
        switch (action) {
            case "textValue":
                obj[languageKey] = value;
                break;
            case "changeLanguage":
                if (Object.keys(obj).includes(languageKey)) {
                    return;
                }
                const oldkey = oldLanguageValueObj.key;
                delete Object.assign(obj, { [languageKey]: obj[oldkey] })[oldkey];
                break;
            case "addLanguage":
                if (Object.values(obj).filter(val => val).length !== Object.values(obj).length) {
                    return; //do not add language field if there is an empty value field
                }
                if (Object.keys(obj).filter(val => val).length !== Object.keys(obj).length) {
                    return; //do not add language field if there is an empty language key
                }
                obj[""] = "";
                break;
            case "removeLanguage":
                delete obj[languageKey];
                break;
            default: return;
        }
        this.setState({ stateObj: obj, options: [] });
    }

    onBlur = () => {
        const { errorObj } = this.state;
        this.props.required && Object.keys(this.state.stateObj).forEach(key => this.state.stateObj[key] ? errorObj[key] = "" : errorObj[key] = "Required field");
        if (!this.state.stateObj["en"]) {
            this.props.setLanguageSpecificText(this.props.field, null);
            return;
        }
        this.props.setLanguageSpecificText(this.props.field, this.state.stateObj);
    }

    getOptions = (inputValue, languageTag) => {
        if (inputValue.trim().length <= 2) {
            return;
        }
        if (this.state.source) {
            this.state.source.cancel("");
        }
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        this.setState({ loading: true, source: source });
        if (this.props.field_type === "keyword") {
            const params = `${this.props.entity_type}=${encodeURI(inputValue)}&lang=${languageTag}`;
            axios.get(LOOK_UP_KEYWORD(params), { cancelToken: source.token })
                .then((res) => {
                    this.setState({ options: res.data, loading: false, source: null })
                }).catch((err) => {
                    console.log(err);
                    this.setState({ loading: false, source: null, options: [] })
                });
        } else if (this.props.field_type === "service_offered") {
            const params = `query=${encodeURI(inputValue)}&lang=${languageTag}`;
            axios.get(LOOK_UP_SERVICE_OFFERED(params), { cancelToken: source.token })
                .then((res) => {
                    this.setState({ options: res.data, loading: false, source: null })
                }).catch((err) => {
                    console.log(err);
                    this.setState({ loading: false, source: null, options: [] })
                });
        } else {
            console.log("unknown field type", this.props.field_type);
        }
    }

    autocompleteTextField = (initValue, languageTag) => {
        return <span>
            <Autocomplete className={"wd-100"}
                disabled={!languageTag}
                freeSolo
                clearOnBlur
                autoHighlight
                loading={this.state.loading}
                value={initValue || null}
                onChange={(event, newValue) => {
                    if (!newValue) {
                        return;
                    }
                    if (typeof newValue === "object") {
                        this.setValues(event, 0, "textValue", languageTag, null, newValue.keyword || newValue.service_offered);
                    } else {
                        this.setValues(event, 0, "textValue", languageTag, null, newValue);
                    }
                }}
                options={this.state.loading ? [] : this.state.options}
                filterOptions={(options, params) => {
                    const filtered = filter(options, params);
                    if (!this.state.loading) {
                        if (params.inputValue !== '') {
                            if (this.props.field_type === "keyword") {
                                filtered.splice(0, 0, {
                                    keyword: params.inputValue,
                                    display_name: `Missing ${params.inputValue}? Add "${params.inputValue}"`,
                                });
                            } else if (this.props.field_type === "service_offered") {
                                filtered.splice(0, 0, {
                                    service_offered: params.inputValue,
                                    display_name: `Missing ${params.inputValue}? Add "${params.inputValue}"`,
                                });
                            }

                        }
                    }
                    if (filtered && filtered.length === 0 && this.state.loading) {
                        return [{ display_name: "Loading please wait...", }]
                    }
                    return filtered;
                }}
                getOptionLabel={(option) => {
                    if (this.props.field_type === "keyword") {
                        return option.keyword ? option.keyword : option
                    } else {
                        return option.service_offered ? option.service_offered : option
                    }
                }
                }
                renderInput={(params) => (
                    <TextField
                        {...params}
                        required={this.props.required}
                        label={this.props.label}
                        help_text={this.props.help_text}
                        placeholder={this.props.placeholder || ""}
                        variant="outlined"
                        onChange={(e) => { this.getOptions(e.target.value, languageTag) }}
                        InputProps={{
                            ...params.InputProps,
                            endAdornment: (
                                <React.Fragment>
                                    {this.state.loading ? (
                                        <CircularProgress color="inherit" size={20} />
                                    ) : null}
                                    {params.InputProps.endAdornment}
                                </React.Fragment>
                            )
                        }}
                    />)}
                renderOption={(option, { inputValue }) => {
                    if (option.display_name) {
                        const matches1 = match(option.display_name, inputValue);
                        const parts1 = parse(option.display_name, matches1);
                        return parts1.map((part, index) => (
                            <span key={index} style={{ fontWeight: part.highlight ? 700 : 400 }}>
                                {part.highlight ? '\u00A0' : ''}{part.text}
                            </span>
                        ))
                    }
                    const matches = match(option.keyword || option.service_offered, inputValue);
                    const parts = parse(option.keyword || option.service_offered, matches);
                    return (
                        <div>
                            {
                                (option.keyword || option.service_offered) && <div>
                                    {parts.map((part, index) => (
                                        <span key={index} style={{ fontWeight: part.highlight ? 700 : 400 }}>
                                            {part.text}
                                        </span>
                                    ))}
                                </div>
                            }
                        </div>
                    );
                }
                }
            />
        </span>
    }

    render() {
        const { help_text, className, disable } = this.props;
        const defaultLangArray = Object.keys(this.state.stateObj) || ["en"];
        const disabledLanguagesArray = [];
        return <div onBlur={this.onBlur}>
            {defaultLangArray.map((lang, langIndex) => {
                disabledLanguagesArray.push(lang);
                return <div className="inline--container" key={langIndex}>
                    <span className="wd-100">
                        {this.autocompleteTextField(this.state.stateObj[lang] || "", lang)}
                    </span>
                    <span className="pl10 wd-60">
                        <EulanguageSelection className={className} showHelpText={help_text ? true : false} lang={lang} languageIndex={langIndex} disable={disable} setValues={this.setValues} disabledLanguagesArray={disabledLanguagesArray} />
                    </span>
                    {langIndex === 0 && <Button disabled={disable} onClick={(e) => { this.setValues(e, langIndex, "addLanguage") }}><Tooltip title={messages.multilingual_elements_add_icon_hover}><AddCircleOutlineIcon className="small-icon" /></Tooltip></Button>}
                    {lang !== "en" && <Button disabled={disable} onClick={(e) => { this.setValues(e, langIndex, "removeLanguage", lang) }}><Tooltip title={messages.multilingual_elements_remove_icon_hover}><RemoveCircleOutlineIcon className="small-icon" /></Tooltip></Button>}
                </div>
            })}
        </div>
    }
}