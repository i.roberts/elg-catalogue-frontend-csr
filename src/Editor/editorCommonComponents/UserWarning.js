import React from "react";
import { Alert, AlertTitle } from '@material-ui/lab';
//import Chip from '@material-ui/core/Chip';
import IconButton from '@material-ui/core/IconButton';
import Collapse from '@material-ui/core/Collapse';
//import Button from '@material-ui/core/Button';
//import CloseIcon from '@material-ui/icons/Close';
import CancelIcon from '@material-ui/icons/Cancel';
import LiveHelpIcon from '@material-ui/icons/LiveHelp';

export default class UserWarning extends React.Component {
    constructor(props) {
        super(props);
        this.state = { open: true }
        
    }


    DescriptionMinLengthInfo = (description_messages) => {
        description_messages.push(<div className="mt1">Please, add a longer description (more than 50 characters)</div>);
    }

    render() {         
        const description_messages = []; 
        const showWarning = this.props.showWarning;       
        this.DescriptionMinLengthInfo(description_messages);

        return <>
           
           {showWarning && <div>
                <Collapse in={this.state.open}>
                    <Alert
                        //variant="standard"
                        //variant="outlined"
                        //variant="filled"
                        severity="warning"
                        action={
                            <IconButton
                                aria-label="close"
                                color="inherit"
                                size="small"
                                onClick={() => {
                                    this.setState({ open: false })
                                }}
                            >
                                <CancelIcon />
                            </IconButton>
                        }
                    >
                        <AlertTitle>Warning</AlertTitle>
                        <ol>
                            {description_messages.length > 0 && description_messages.map((item, index) => {
                                return <span key={index}>{item}</span>
                            })}
                        </ol>
                    </Alert>
                </Collapse>
                
                {!this.state.open && <LiveHelpIcon onClick={(e) => { this.setState({ open: true }) }} />}
            </div>
        }
        </>
    }
}