import React from "react";
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import messages from "./../../config/messages";
import Button from '@material-ui/core/Button';
import { languageModelObj } from "../Models/LrModel";
import FreeText from "../editorCommonComponents/FreeText";
import AutocompleteOneChoice from "../editorCommonComponents/AutocompleteOneChoice";
//import LanguageIdLookup from "../editorCommonComponents/LanguageIdLookup";
import Virtualize from "../editorCommonComponents/Virtualize";
import LanguageSpecificText from "../editorCommonComponents/LanguageSpecificText";
//import VariantAutocomplete from "./VariantAutocomplete";
import VariantAutocompleteArray from "./VariantAutocompleteArray";
import ScriptAutocomplete from "./ScriptAutocomplete";
import Tooltip from '@material-ui/core/Tooltip';
import OpenInNewIcon from '@material-ui/icons/OpenInNew';

export default class LanguageModel extends React.Component {

    constructor(props) {
        super(props);
        this.state = { initialValuesArray: props.initialValuesArray || [] }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            initialValuesArray: nextProps.initialValuesArray || []
        };
    }

    setValues = (action, itemIndex, value) => {
        const { initialValuesArray } = this.state;
        switch (action) {
            case "addArrayItem":
                const filteredArray = initialValuesArray.filter(item => item.language_tag);
                if (filteredArray.length !== initialValuesArray.length) {
                    return; //do not add element if there are empty ones
                }
                initialValuesArray.push(JSON.parse(JSON.stringify(languageModelObj)));
                break;
            case "removeArrayItem":
                initialValuesArray.splice(itemIndex, 1);
                break;
            default: break;
        }
        this.setState({ initialValuesArray }, this.onBlur);
    }

    setLanguageTag = (index, value) => {
        const { initialValuesArray } = this.state;
        initialValuesArray[index].language_tag = value;
        this.setState({ initialValuesArray });
    }

    setGlottologCode = (index, value) => {
        const { initialValuesArray } = this.state;
        initialValuesArray[index].glottolog_code = value;
        this.setState({ initialValuesArray });
    }

    setLanguageId = (index, value) => {
        const { initialValuesArray } = this.state;
        initialValuesArray[index].language_id = value;
        initialValuesArray[index].script_id = null;
        initialValuesArray[index].variant_id = [];
        initialValuesArray[index].glottolog_code = "";
        this.setState({ initialValuesArray }, this.onBlur);
    }

    setScriptId = (index, value) => {
        const { initialValuesArray } = this.state;
        initialValuesArray[index].script_id = value;
        initialValuesArray[index].variant_id = [];
        this.setState({ initialValuesArray });
    }

    setRegionId = (index, value) => {
        const { initialValuesArray } = this.state;
        initialValuesArray[index].region_id = value;
        this.setState({ initialValuesArray });
    }

    setVariantId = (index, value) => {
        const { initialValuesArray } = this.state;
        initialValuesArray[index].variant_id = value;
        this.setState({ initialValuesArray });
    }

    set_language_variety_name = (index, value) => {
        const { initialValuesArray } = this.state;
        initialValuesArray[index].language_variety_name = value;
        this.setState({ initialValuesArray });
    }

    onBlur = () => {
        //id-script(if not suppressed)-region(UPPERCASE)-variants
        const initialValuesArray = this.state.initialValuesArray.map(item => {
            item.language_tag = item.language_id;
            if (item.script_id) {
                item.language_tag += '-' + item.script_id;
            }
            if (item.region_id) {
                item.language_tag += '-' + item.region_id.toUpperCase();
            }
            if (item.variant_id && item.variant_id.length) {
                let variant = "";
                for (let i = 0; i < item.variant_id.length; i++) {
                    variant += '-' + item.variant_id[i];
                }
                item.language_tag += variant;
            }
            return item;
        })
        this.props.updateModel_array(this.props.field, initialValuesArray);
    }

    render() {
        const { initialValuesArray } = this.state;
        if (initialValuesArray.length === 0) {
            this.props.required && initialValuesArray.push(JSON.parse(JSON.stringify(languageModelObj)));
        }
        const glotolog_help_text = <span>
            Select "Uncoded languages" as Language value to use this field; for values, see The glottolog code (languoid, cf. <a target="_blank" rel="noopener noreferrer" href="https://glottolog.org/glottolog/language"> https://glottolog.org/glottolog/language</a> <OpenInNewIcon style={{ width: 10, height: 10 }} /> )
        </span>
        return <div className={`"pb-3 inner--group" ${this.props.group_className}`}>
            <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1}>
                <Grid item sm={10}>
                    <Typography variant="h3" className="section-links" >{this.props.label} </Typography>
                    <Typography className="section-links" >{this.props.help_text} </Typography>
                </Grid>
                <Grid item sm={1}>
                    {initialValuesArray.length === 0 && <Button className="inner-link-default--purple" onClick={(e) => this.setValues("addArrayItem")}>{messages.group_elements_create}</Button>}
                    {/*initialValuesArray.length !== 0 && <Button className="inner-link-outlined--purple" onClick={(e) => this.setValues("addArrayItem")}><AddCircleOutlineIcon /></Button>*/}
                </Grid>
            </Grid>
            {
                initialValuesArray.map((arrayItem, arrayItemIndex) => {
                    const is_glottolog_code_editable = arrayItem.language_id === "mis";
                    return <div key={arrayItemIndex} onBlur={this.onBlur}>
                        {(arrayItem.language_id && arrayItem.language_id !== "") &&
                            <Grid container className="pb1" direction="row" justifyContent="flex-end" alignItems="baseline" >
                                <Grid item><Tooltip title={`${messages.array_elements_remove} ${this.props.label}`}><Button className="inner-link-default--purple" onClick={(e) => this.setValues("removeArrayItem", arrayItemIndex)}>{messages.array_elements_remove}</Button></Tooltip></Grid>
                            </Grid>
                        }
                        {(!arrayItem.language_id && arrayItemIndex >= 1) &&
                            <Grid container className="pb1" direction="row" justifyContent="flex-end" alignItems="baseline" >
                                <Grid item><Tooltip title={`${messages.array_elements_remove} ${this.props.label}`}><Button className="inner-link-default--purple" onClick={(e) => this.setValues("removeArrayItem", arrayItemIndex)}>{messages.array_elements_remove}</Button></Tooltip></Grid>
                            </Grid>
                        }

                        <div className="mtl20">
                            {false && <FreeText
                                {...this.props.formElements.language_tag}
                                required={false}
                                disabled={true}
                                className={this.props.className}
                                initialValue={arrayItem.language_tag || ""}
                                field={arrayItemIndex}//don't care about field, track index instead
                                updateModel={this.setLanguageTag}
                            />}
                            <FreeText
                                {...this.props.formElements.glottolog_code}
                                help_text={glotolog_help_text}
                                required={is_glottolog_code_editable}
                                disabled={!is_glottolog_code_editable}
                                className={this.props.className}
                                initialValue={arrayItem.glottolog_code || ""}
                                field={arrayItemIndex}//don't care about field, track index instead
                                maxLength={10}
                                updateModel={this.setGlottologCode}
                            />
                            <Virtualize
                                {...this.props.formElements.language_id}
                                className={this.props.className}
                                initialValue={arrayItem.language_id || ""}
                                field={arrayItemIndex}//don't care about field, track index instead
                                updateModel_String={this.setLanguageId}
                            />
                            {arrayItem.language_id && <ScriptAutocomplete
                                {...this.props.formElements.script_id}
                                initialValue={arrayItem.script_id || ""}
                                {...arrayItem}
                                field={arrayItemIndex}//don't care about field, track index instead
                                updateModel_String={this.setScriptId}
                            />}
                            {arrayItem.language_id && <AutocompleteOneChoice
                                {...this.props.formElements.region_id}
                                initialValue={arrayItem.region_id || ""}
                                field={arrayItemIndex}//don't care about field, track index instead
                                updateModel_String={this.setRegionId}
                            />}
                            {arrayItem.language_id && <div className="pb-3"><VariantAutocompleteArray
                                {...this.props.formElements.variant_id}
                                initialValuesArray={arrayItem.variant_id || []}
                                {...arrayItem}
                                field={arrayItemIndex}//don't care about field, track index instead
                                updateModel_array={this.setVariantId}
                            /></div>}
                            {arrayItem.language_id && <LanguageSpecificText
                                {...this.props.formElements.language_variety_name}
                                defaultValueObj={arrayItem.language_variety_name || { "en": "" }}
                                field={arrayItemIndex}//don't care about field, track index instead
                                setLanguageSpecificText={this.set_language_variety_name}
                            />}
                            <div className="mb2">
                                {/*<Button className="inner-link-default--purple" onClick={(e) => this.setValues("removeArrayItem", arrayItemIndex)}>{messages.array_elements_remove}</Button>*/}
                                {/*<Button className="inner-link-default--purple" onClick={(e) => this.setValues("addArrayItem")}>{messages.array_elements_add}</Button>*/}

                                {((arrayItem.language_id && arrayItem.language_id !== "") && ((initialValuesArray.length - 1) === arrayItemIndex)) ?
                                    <Grid container className="pb1" direction="row" justifyContent="flex-start" alignItems="baseline" >
                                        <Grid item>
                                            <Tooltip title={`${messages.array_elements_add} ${this.props.label}`}><Button className="inner-link-default--purple" onClick={(e) => this.setValues("addArrayItem")}>{messages.array_elements_add}</Button></Tooltip>
                                        </Grid>
                                    </Grid>
                                    : <span></span>
                                }
                            </div>
                            <div style={{ marginTop: "1em" }} />
                        </div>
                    </div>
                })
            }
        </div>
    }
}