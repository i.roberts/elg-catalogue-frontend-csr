import React from "react";
import FreeText from "../editorCommonComponents/FreeText";
import LanguageSpecificText from "../editorCommonComponents/LanguageSpecificText";
import RecordSelectList from "../editorCommonComponents/RecordSelectList";
//import OnOfSwitch from "../editorCommonComponents/OnOfSwitch";
//import AutocompleteChoicesChips from "../editorCommonComponents/AutocompleteChoicesChips";
import AutocompleteRecommendedChoices from "../editorCommonComponents/AutocompleteRecommendedChoices";
import RecordRadioBoolean from "../editorCommonComponents/RecordRadioBoolean";
import ParameterEnumerationArray from "./ParameterEnumerationArray";

export default class ParameterItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = { parameterItem: props.parameterItem }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            parameterItem: nextProps.parameterItem
        };
    }

    setObjectGeneral = (field, valueObj) => {
        const { parameterItem } = this.state;
        parameterItem[field] = valueObj;
        this.setState({ parameterItem }, this.onBlur);
    }

    setString = (field, value) => {
        const { parameterItem } = this.state;
        parameterItem[field] = value;
        this.setState({ parameterItem }, this.onBlur);
    }


    setSelectListparameter_type = (event, selectedObjArray) => {
        const { parameterItem } = this.state;
        parameterItem.parameter_type = (selectedObjArray && selectedObjArray.length > 0) ? selectedObjArray[0].value : null;
        this.setState({ parameterItem });
    }

    updateModel__array = (obj2update, valueArray) => {
        const { parameterItem } = this.state;
        if (!parameterItem[obj2update]) {
            parameterItem[obj2update] = [];
        }
        parameterItem[obj2update] = valueArray;
        this.setState({ parameterItem }, this.onBlur());
    }


    //updateBoolean = (field, boolean) => {
    //    const { parameterItem } = this.state;
    //    parameterItem[field] = Boolean(boolean);
    //     this.setState({ parameterItem });
    // }

    setmulti_valueBoolean = (e) => {
        const { parameterItem } = this.state;
        if (parameterItem.multi_value === null || parameterItem.multi_value === undefined) {
            parameterItem.multi_value = null;
        }
        parameterItem.multi_value = e.target.value === "true";
        this.setState({ parameterItem }, this.onBlur);
    }


    setoptionalBoolean = (e) => {
        const { parameterItem } = this.state;
        if (parameterItem.optional === null || parameterItem.optional === undefined) {
            parameterItem.optional = null;
        }
        parameterItem.optional = e.target.value === "true";
        this.setState({ parameterItem }, this.onBlur);
    }

    onBlur = () => {
        const { parameterItem } = this.state;
        const exists_parameter_name = parameterItem.parameter_name ? true : false;
        const exist_parameter_label = parameterItem.parameter_label && parameterItem.parameter_label.hasOwnProperty("en") && parameterItem.parameter_label["en"] ? true : false;
        const exist_parameter_description = parameterItem.parameter_description && parameterItem.parameter_description.hasOwnProperty("en") && parameterItem.parameter_description["en"] ? true : false;
        const exist_parameter_type = parameterItem.parameter_type ? true : false;
        const exist_default_value = parameterItem.default_value ? true : false;
        const exist_data_format = (parameterItem.data_format && parameterItem.data_format.length) ? true : false;
        const exist_optional = (parameterItem.optional === true || parameterItem.optional === false) ? true : false;
        const exist_multi_value = (parameterItem.multi_value === true || parameterItem.multi_value === false) ? true : false;
        const exist_enumeration_value = parameterItem.enumeration_value && parameterItem.enumeration_value.length > 0 ? true : false;
        if (exists_parameter_name === false && exist_parameter_description === false && exist_parameter_label === false && exist_parameter_type === false && exist_default_value === false && exist_data_format === false && exist_optional === false && exist_multi_value === false && exist_enumeration_value === false) {
            parameterItem["editor-placeholder"] = true;
        } else {
            delete parameterItem["editor-placeholder"];
        }
        this.props.updateModel("setparameterItem", this.props.parameterItemIndex, parameterItem);
    }

    render() {
        const { parameterItem } = this.state;
        //console.log(this.props)
        return <div onBlur={this.onBlur}>
            <div className="pt-3 pb-3"><FreeText className="wd-100" {...this.props.formElements.parameter_name} initialValue={parameterItem.parameter_name} field="parameter_name" lr_subclass={true} updateModel={this.setString} /></div>
            <div className="pt-3 pb-3"><LanguageSpecificText {...this.props.formElements.parameter_label} defaultValueObj={parameterItem.parameter_label || { "en": "" }} field="parameter_label" multiline={false} setLanguageSpecificText={this.setObjectGeneral} /></div>
            <div className="pt-3 pb-3"><LanguageSpecificText {...this.props.formElements.parameter_description} defaultValueObj={parameterItem.parameter_description || { "en": "" }} field="parameter_description" multiline={true} maxRows={3} setLanguageSpecificText={this.setObjectGeneral} /></div>
            <div className="pt-3 pb-3"><RecordSelectList className="wd-100"{...this.props.formElements.parameter_type} choices={this.props.formElements.parameter_type.choices} default_value={parameterItem.parameter_type} setSelectedvalue={this.setSelectListparameter_type} /></div>
            {/*<div className="pt-3 pb-3"><OnOfSwitch {...this.props.formElements.optional} default_value={parameterItem.optional} field="optional" updateBoolean={this.updateBoolean} /></div>
            <div className="pt-3 pb-3"><OnOfSwitch {...this.props.formElements.multi_value} default_value={parameterItem.multi_value} field="multi_value" updateBoolean={this.updateBoolean} /></div>*/}
            <div className="pt-3 pb-3"><RecordRadioBoolean className="wd-100"
                label={this.props.formElements.optional.label}
                help_text={this.props.formElements.optional.help_text}
                required={this.props.formElements.optional.required}
                default_value={parameterItem.optional} handleBooleanChange={this.setoptionalBoolean} /></div>

            <div className="pt-3 pb-3"><RecordRadioBoolean className="wd-100"
                label={this.props.formElements.multi_value.label}
                help_text={this.props.formElements.multi_value.help_text}
                required={this.props.formElements.multi_value.required}
                default_value={parameterItem.multi_value} handleBooleanChange={this.setmulti_valueBoolean} /></div>

            <div className="pt-3 pb-3"><FreeText className="wd-100" {...this.props.formElements.default_value} initialValue={parameterItem.default_value || ""} field="default_value" lr_subclass={true} updateModel={this.setString} /></div>
            {<div className="pt-3 pb-3"><AutocompleteRecommendedChoices className="wd-100" {...this.props.formElements.data_format} recommended_choices={this.props.formElements.data_format.choices} initialValuesArray={parameterItem.data_format || []} field="data_format" lr_subclass={true} updateModel_array={this.updateModel__array} /></div>}

            <div className="pt-3 pb-3">
                <ParameterEnumerationArray
                    {...this.props.formElements.enumeration_value}
                    initialValueArray={parameterItem.enumeration_value || []}
                    field="enumeration_value"
                    updateModel_array={this.updateModel__array} />

            </div>

        </div>
    }

}