import React from "react";
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
//import { ReactComponent as AddCircleOutlineIcon } from "./../../assets/elg-icons/editor/add-bold.svg";
//import { ReactComponent as RemoveCircleOutlineIcon } from "./../../assets/elg-icons/editor/remove-bold.svg";
import { parameterEnumerationObj } from "../Models/ToolModel";
import ParameterEnumerationItem from "./ParameterEnumerationItem";
import Tooltip from '@material-ui/core/Tooltip';
import messages from "./../../config/messages";

export default class ParameterEnumerationArray extends React.Component {
    constructor(props) {
        super(props);
        this.state = { enumerationArray: props.initialValueArray || [] }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            enumerationArray: nextProps.initialValueArray || [],
        };
    }

    setValues = (action, enumerationItemIndex, value) => {
        const { enumerationArray } = this.state;
        switch (action) {
            case "addenumerationItem":
                const filteredArray = enumerationArray.filter(item => item && item.value_name && item.value_label && item.value_description);
                if (filteredArray.length !== enumerationArray.length) {
                    return;//do not add element if required filds are blank
                }
                enumerationArray.push(JSON.parse(JSON.stringify(parameterEnumerationObj)));
                break;
            case "removeenumerationItem":
                enumerationArray.splice(enumerationItemIndex, 1);
                break;
            case "setenumerationItem":
                enumerationArray[enumerationItemIndex] = value;
                this.props.updateModel_array(this.props.field, enumerationArray);
                break;
            default: break;
        }
        this.setState({ enumerationArray }, this.onBlur)
    }

    onBlur = () => {
        this.props.updateModel_array(this.props.field, this.state.enumerationArray);
    }


    render() {
        const { enumerationArray } = this.state;
        //console.log(this.props)

        return <div onBlur={this.onBlur} className="pb-3 inner--group nested--group">
            <Grid container direction="row" alignItems="center" justifyContent="space-between" spacing={1} >
                <Grid item sm={12}>
                    <Typography variant="h3" className="section-links" >{this.props.label} </Typography>
                    <Typography className="section-links" >{this.props.help_text} </Typography>
                </Grid>

                {(enumerationArray.length === 0) &&
                    <Grid item sm={1}>
                        <Tooltip title={`${messages.group_elements_create} ${this.props.label}`}><Button className="inner-link-default--purple" onClick={(e) => this.setValues("addenumerationItem")}>{messages.group_elements_create}</Button></Tooltip>
                    </Grid>
                }
            </Grid>
            {
                enumerationArray.map((enumerationItem, enumerationItemIndex) => {
                    return <div key={enumerationItemIndex} onBlur={() => this.onBlur()}>
                        <ParameterEnumerationItem  {...this.props} enumerationItem={enumerationItem} enumerationItemIndex={enumerationItemIndex} updateModel={this.setValues} />
                        <div className="mb2 pt-3">
                            {enumerationItem.value_name && <Tooltip title={`${messages.array_elements_remove} ${this.props.label}`}><Button className="inner-link-default--purple" onClick={(e) => this.setValues("removeenumerationItem", enumerationItemIndex)}>{messages.array_elements_remove}</Button></Tooltip>}
                            {enumerationItem.value_name && <Tooltip title={`${messages.array_elements_add} ${this.props.label}`}><Button className="inner-link-default--purple" onClick={(e) => this.setValues("addenumerationItem")}>{messages.array_elements_add}</Button></Tooltip>}
                        </div>
                        <div style={{ marginTop: "5em" }} /></div>
                })
            }
        </div>
    }
}