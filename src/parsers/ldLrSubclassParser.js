class LdLrSubclassParser {
    constructor() {
        this.getLrtype = this.getLrtype.bind(this);
        this.getldsubclass = this.getldsubclass.bind(this);
        this.getmodelfunction = this.getmodelfunction.bind(this);
        this.getmodeltype = this.getmodeltype.bind(this);
        this.getBaseItem = this.getBaseItem.bind(this);
        this.getOrder = this.getOrder.bind(this);
        this.getPerplexity = this.getPerplexity.bind(this);
        this.getIsFactored = this.getIsFactored.bind(this);
        this.getEncodingLevel = this.getEncodingLevel.bind(this);
        //this.getCompliesWith = this.getCompliesWith.bind(this);
        this.getTheoreticModel = this.getTheoreticModel.bind(this);
        this.getSmoothing = this.getSmoothing.bind(this);
        this.getInterpolated = this.getInterpolated.bind(this);
        this.getFactor = this.getFactor.bind(this);
        this.getrunning_environment_details = this.getrunning_environment_details.bind(this);
        this.getRequiredHardware = this.getRequiredHardware.bind(this);
        this.getRequiresSoftware = this.getRequiresSoftware.bind(this);
        this.getFormalism = this.getFormalism.bind(this);
        this.getgrammatical_phenomena_coverage = this.getgrammatical_phenomena_coverage.bind(this);
        this.getld_task = this.getld_task.bind(this);
        this.getweighted_grammar = this.getweighted_grammar.bind(this);
        this.getmodel_variant = this.getmodel_variant.bind(this);
        this.getml_framework = this.getml_framework.bind(this);
        this.getmethod = this.getmethod.bind(this);
        this.getalgorithm_details = this.getalgorithm_details.bind(this);
        this.getalgorithm = this.getalgorithm.bind(this);
        this.gettraining_corpus_details = this.gettraining_corpus_details.bind(this);
        this.gettraining_process_details = this.gettraining_process_details.bind(this);
        this.getbias_details = this.getbias_details.bind(this);
        this.getdevelopment_framework = this.getdevelopment_framework.bind(this);
    }

    getTheoreticModel(data, lang) {
        let theoretic_model = [];
        if (data.language_description_subclass && data.language_description_subclass.field_value.theoretic_model && data.language_description_subclass.field_value.theoretic_model.field_value.length > 0) {
            data.language_description_subclass.field_value.theoretic_model.field_value.forEach((keyword, index) => {
                theoretic_model.push(keyword[lang] || keyword[Object.keys(keyword)[0]]);
            });
            const theoretic_modelLabel = data.language_description_subclass.field_value.theoretic_model.field_label[lang] || data.language_description_subclass.field_value.theoretic_model.field_label["en"];
            return { label: theoretic_modelLabel, value: theoretic_model };
        }
        return null;
    }


    getEncodingLevel(data, lang) {
        let encodinglevelArray = [];
        if (data.language_description_subclass && data.language_description_subclass.field_value.encoding_level && data.language_description_subclass.field_value.encoding_level.field_value.length > 0) {
            data.language_description_subclass.field_value.encoding_level.field_value.forEach((keyword, index) => {
                encodinglevelArray.push(keyword.label[lang] || keyword.label[Object.keys(keyword.label)[0]]);
            });
            const encodingLabel = data.language_description_subclass.field_value.encoding_level.field_label[lang] || data.language_description_subclass.field_value.encoding_level.field_label["en"];
            return { label: encodingLabel, value: encodinglevelArray };
        }
        return null;
    }

    /*getCompliesWith(data, lang) {
        let complieswithArray = [];
        if (data.language_description_subclass && data.language_description_subclass.field_value.complies_with && data.language_description_subclass.field_value.complies_with.field_value.length > 0) {
            data.language_description_subclass.field_value.complies_with.field_value.forEach((keyword, index) => {
                complieswithArray.push(keyword.label[lang] || keyword.label[Object.keys(keyword.label)[0]]);
            });
            const compliesWithLabel = data.language_description_subclass.field_value.complies_with.field_label[lang] || data.language_description_subclass.field_value.complies_with.field_label["en"];
            return { label: compliesWithLabel, value: complieswithArray };
        }
        return null;
    }*/

    getLrtype(data, lang) {
        let lr_type = data.lr_type;
        if (lr_type) {
            lr_type = data.lr_type.field_value;
            const lr_type_label = data.lr_type.field_label[lang] || data.lr_type.field_label["en"];
            return { label: lr_type_label, value: lr_type };
        }
        return lr_type || "";
    }

    getldsubclass(data, lang) {
        let language_description_subclass = data && data.field_value.language_description_subclass;
        if (language_description_subclass) {
            language_description_subclass = data.field_value.language_description_subclass.field_value.ld_subclass_type.field_value;
            const language_description_subclass_label = data.field_value.language_description_subclass.field_value.ld_subclass_type.field_label[lang] || data.field_value.language_description_subclass.field_value.ld_subclass_type.field_label["en"];
            return { label: language_description_subclass_label, value: language_description_subclass };
        }
        return language_description_subclass || "";
    }

    getmodeltype(data, lang) {
        let language_description_subclass = data && data.field_value.language_description_subclass;
        let model_typeArray = [];
        if (language_description_subclass && language_description_subclass.field_value.model_type && language_description_subclass.field_value.model_type.field_value.length > 0) {
            language_description_subclass.field_value.model_type.field_value.forEach((keyword, index) => {
                if (keyword && keyword.hasOwnProperty("label")) {
                    model_typeArray.push(keyword.label[lang] || keyword.label[Object.keys(keyword.label)[0]]);
                } else if (keyword) {
                    model_typeArray.push(keyword.value);
                }

            });
            const model_typeLabel = language_description_subclass.field_value.model_type.field_label[lang] || language_description_subclass.field_value.model_type.field_label["en"];
            return { label: model_typeLabel, value: model_typeArray };
        }
        return null;
    }

    getmodelfunction(data, lang) {
        let language_description_subclass = data && data.field_value.language_description_subclass;
        let model_functionArray = [];
        if (language_description_subclass && language_description_subclass.field_value.model_function && language_description_subclass.field_value.model_function.field_value.length > 0) {
            language_description_subclass.field_value.model_function.field_value.forEach((keyword, index) => {
                if (keyword && keyword.hasOwnProperty("label")) {
                    model_functionArray.push(keyword.label[lang] || keyword.label[Object.keys(keyword.label)[0]]);
                } else if (keyword) {
                    model_functionArray.push(keyword.value);
                }
            });
            const model_functionLabel = language_description_subclass.field_value.model_function.field_label[lang] || language_description_subclass.field_value.model_function.field_label["en"];
            return { label: model_functionLabel, value: model_functionArray };
        }
        return null;
    }


    getOrder(data, lang) {
        let order = data.language_description_subclass && data.language_description_subclass.field_value.n_gram_model && data.language_description_subclass.field_value.n_gram_model.field_value.order;
        if (order) {
            order = data.language_description_subclass.field_value.n_gram_model.field_value.order.field_value;
            const orderLabel = data.language_description_subclass.field_value.n_gram_model.field_value.order.field_label[lang] || data.language_description_subclass.field_value.n_gram_model.field_value.order.field_label["en"];
            return { label: orderLabel, value: order };
        }
        return order || "";
    }

    getmodel_variant(data, lang) {
        let model_variant = data.language_description_subclass && data.language_description_subclass.field_value.model_variant;
        if (model_variant) {
            model_variant = data.language_description_subclass.field_value.model_variant.field_value;
            const modelLabel = data.language_description_subclass.field_value.model_variant.field_label[lang] || data.language_description_subclass.field_value.model_variant.field_label["en"];
            return { label: modelLabel, value: model_variant };
        }
        return model_variant || "";
    }

    getml_framework(data, lang) {
        let ml_framework = data.language_description_subclass && data.language_description_subclass.field_value.ml_framework1;
        if (ml_framework) {
            ml_framework = data.language_description_subclass.field_value.ml_framework1.label[lang] || data.language_description_subclass.field_value.ml_framework1.label["en"];
            const ml_framework_label = data.language_description_subclass.field_value.ml_framework1.field_label[lang] || data.language_description_subclass.field_value.ml_framework1.field_label["en"];
            return { label: ml_framework_label, value: ml_framework };
        }
        return ml_framework || "";
    }


    getdevelopment_framework(data, lang) {
        let development_framework_values = [];
        if (data.language_description_subclass && data.language_description_subclass.field_value.development_framework && data.language_description_subclass.field_value.development_framework.field_value.length > 0) {
            data.language_description_subclass.field_value.development_framework.field_value.forEach((keyword, index) => {
                keyword.label ? (development_framework_values.push(keyword.label[lang] || keyword.label[Object.keys(keyword.label)[0]])) : development_framework_values.push(keyword.value)
            });
            const development_framework_label = data.language_description_subclass.field_value.development_framework.field_label[lang] || data.language_description_subclass.field_value.development_framework.field_label["en"];
            return { "label": development_framework_label, "value": development_framework_values };
        }
        return null;
    }



    getmethod(data, lang) {
        let method = data.language_description_subclass && data.language_description_subclass.field_value.method;
        if (method) {
            method = data.language_description_subclass.field_value.method.label[lang] || data.language_description_subclass.field_value.method.label[Object.keys(data.language_description_subclass.field_value.method.label)[0]];
            const method_label = data.language_description_subclass.field_value.method.field_label[lang] || data.language_description_subclass.field_value.method.field_label["en"];
            return { label: method_label, value: method };
        }
        return method || "";
    }

    getBaseItem(data, lang) {
        let baseitemArray = [];
        if (data.language_description_subclass && data.language_description_subclass.field_value.n_gram_model && data.language_description_subclass.field_value.n_gram_model.field_value.base_item && data.language_description_subclass.field_value.n_gram_model.field_value.base_item.field_value.length > 0) {
            data.language_description_subclass.field_value.n_gram_model.field_value.base_item.field_value.forEach((keyword, index) => {
                baseitemArray.push(keyword.label[lang] || keyword.label[Object.keys(keyword.label)[0]]);
            });
            const baseItem_label = data.language_description_subclass.field_value.n_gram_model.field_value.base_item.field_label[lang] || data.language_description_subclass.field_value.n_gram_model.field_value.base_item.field_label["en"];
            return { label: baseItem_label, value: baseitemArray };
        }
        return null;
    }

    getld_task(data, lang) {
        let ld_taskArray = [];
        if (data.language_description_subclass &&
            data.language_description_subclass.field_value.ld_task &&
            data.language_description_subclass.field_value.ld_task.field_value.length > 0) {
            data.language_description_subclass.field_value.ld_task.field_value.forEach((keyword, index) => {
                ld_taskArray.push(keyword.label[lang] || keyword.label[Object.keys(keyword.label)[0]]);
            });
            const ldLabel = data.language_description_subclass.field_value.ld_task.field_label[lang] || data.language_description_subclass.field_value.ld_task.field_label["en"];
            return { label: ldLabel, value: ld_taskArray };
        }
        return null;
    }

    getgrammatical_phenomena_coverage(data, lang) {
        let grammatical_phenomena_coverageArray = [];
        if (data.language_description_subclass &&
            data.language_description_subclass.field_value.grammatical_phenomena_coverage &&
            data.language_description_subclass.field_value.grammatical_phenomena_coverage.field_value.length > 0) {
            data.language_description_subclass.field_value.grammatical_phenomena_coverage.field_value.forEach((keyword, index) => {
                grammatical_phenomena_coverageArray.push(keyword.label[lang] || keyword.label[Object.keys(keyword.label)[0]]);
            });
            const grammaticalLabel = data.language_description_subclass.field_value.grammatical_phenomena_coverage.field_label[lang] || data.language_description_subclass.field_value.grammatical_phenomena_coverage.field_label["en"];
            return { label: grammaticalLabel, value: grammatical_phenomena_coverageArray };
        }
        return null;
    }

    getPerplexity(data, lang) {
        let perplexity = data.language_description_subclass && data.language_description_subclass.field_value.n_gram_model && data.language_description_subclass.field_value.n_gram_model.field_value.perplexity;
        if (perplexity) {
            perplexity = data.language_description_subclass.field_value.n_gram_model.field_value.perplexity.field_value;
            const perplexityLabel = data.language_description_subclass.field_value.n_gram_model.field_value.perplexity.field_label[lang] || data.language_description_subclass.field_value.n_gram_model.field_value.perplexity.field_label["en"];
            return { label: perplexityLabel, value: perplexity };
        }
        return perplexity || "";
    }


    getIsFactored(data, lang) {
        let is_factored = data.language_description_subclass && data.language_description_subclass.field_value.n_gram_model && data.language_description_subclass.field_value.n_gram_model.field_value.is_factored;
        if (is_factored) {
            is_factored = data.language_description_subclass.field_value.n_gram_model.field_value.is_factored.field_value;
            const isFactoredLabel = data.language_description_subclass.field_value.n_gram_model.field_value.is_factored.field_label[lang] || data.language_description_subclass.field_value.n_gram_model.field_value.is_factored.field_label["en"];
            return { label: isFactoredLabel, value: is_factored };
        }
        return is_factored || "";
    }

    getFactor(data, lang) {
        let factorArray = [];
        if (data.language_description_subclass && data.language_description_subclass.field_value.n_gram_model &&
            data.language_description_subclass.field_value.n_gram_model.field_value.factor &&
            data.language_description_subclass.field_value.n_gram_model.field_value.factor.field_value.length > 0) {
            data.language_description_subclass.field_value.n_gram_model.field_value.factor.field_value.forEach((keyword, index) => {
                factorArray.push(keyword);
            });
            const factorArrayLabel = data.language_description_subclass.field_value.n_gram_model.field_value.factor.field_label[lang] || data.language_description_subclass.field_value.n_gram_model.field_value.factor.field_label["en"];
            return { label: factorArrayLabel, value: factorArray };
        }
        return null;
    }

    getSmoothing(data, lang) {
        let smoothing = data.language_description_subclass && data.language_description_subclass.field_value.n_gram_model && data.language_description_subclass.field_value.n_gram_model.field_value.smoothing;
        if (smoothing) {
            smoothing = data.language_description_subclass.field_value.n_gram_model.field_value.smoothing.field_value;
            const smothingLabel = data.language_description_subclass.field_value.n_gram_model.field_value.smoothing.field_label[lang] || data.language_description_subclass.field_value.n_gram_model.field_value.smoothing.field_label["en"];
            return { label: smothingLabel, value: smoothing };
        }
        return smoothing || "";
    }



    getInterpolated(data, lang) {
        let interpolated = data.language_description_subclass && data.language_description_subclass.field_value.n_gram_model && data.language_description_subclass.field_value.n_gram_model.field_value.interpolated;
        if (interpolated) {
            interpolated = data.language_description_subclass.field_value.n_gram_model.field_value.interpolated.field_value;
            const interpolatedLabel = data.language_description_subclass.field_value.n_gram_model.field_value.interpolated.field_label[lang] || data.language_description_subclass.field_value.n_gram_model.field_value.interpolated.field_label["en"];
            return { label: interpolatedLabel, value: interpolated };
        }
        return interpolated || "";
    }

    getweighted_grammar(data, lang) {
        let weighted_grammar = data.language_description_subclass && data.language_description_subclass.field_value.weighted_grammar;
        if (weighted_grammar) {
            weighted_grammar = data.language_description_subclass.field_value.weighted_grammar.field_value;
            const weightedLabel = data.language_description_subclass.field_value.weighted_grammar.field_label[lang] || data.language_description_subclass.field_value.weighted_grammar.field_label["en"];
            return { label: weightedLabel, value: weighted_grammar };
        }
        return weighted_grammar || "";

    }

    getrunning_environment_details(data, lang) {
        let running_environment_details = data.running_environment_details;
        if (running_environment_details) {
            running_environment_details = data.running_environment_details.field_value[lang] || data.running_environment_details.field_value[Object.keys(data.running_environment_details.field_value)[0]];
            const runningLabel = data.running_environment_details.field_label[lang] || data.running_environment_details.field_label["en"];
            return { label: runningLabel, value: running_environment_details };
        }
        return running_environment_details || "";
    }

    getFormalism(data, lang) {
        let formalism = data.language_description_subclass && data.language_description_subclass.field_value.formalism;
        if (formalism) {
            formalism = data.language_description_subclass.field_value.formalism.field_value[lang]
                || data.language_description_subclass.field_value.formalism.field_value[Object.keys(data.language_description_subclass.field_value.formalism.field_value)[0]];
            const formalismLabel = data.language_description_subclass.field_value.formalism.field_label[lang] || data.language_description_subclass.field_value.formalism.field_label["en"];
            return { label: formalismLabel, value: formalism };
        }
        return formalism || "";
    }

    getalgorithm(data, lang) {
        let algorithm = data.language_description_subclass && data.language_description_subclass.field_value.algorithm;
        if (algorithm) {
            algorithm = data.language_description_subclass.field_value.algorithm.field_value[lang]
                || data.language_description_subclass.field_value.algorithm.field_value[Object.keys(data.language_description_subclass.field_value.algorithm.field_value)[0]];
            const algorithmLabel = data.language_description_subclass.field_value.algorithm.field_label[lang] || data.language_description_subclass.field_value.algorithm.field_label["en"];
            return { label: algorithmLabel, value: algorithm };
        }
        return algorithm || "";
    }

    getalgorithm_details(data, lang) {
        let algorithm_details = data.language_description_subclass && data.language_description_subclass.field_value.algorithm_details;
        if (algorithm_details) {
            algorithm_details = data.language_description_subclass.field_value.algorithm_details.field_value[lang]
                || data.language_description_subclass.field_value.algorithm_details.field_value[Object.keys(data.language_description_subclass.field_value.algorithm_details.field_value)[0]];
            const algorithmDetailsLabel = data.language_description_subclass.field_value.algorithm_details.field_label[lang] || data.language_description_subclass.field_value.algorithm_details.field_label["en"];
            return { label: algorithmDetailsLabel, value: algorithm_details };
        }
        return algorithm_details || "";
    }

    gettraining_corpus_details(data, lang) {
        let training_corpus_details = data.language_description_subclass && data.language_description_subclass.field_value.training_corpus_details;
        if (training_corpus_details) {
            training_corpus_details = data.language_description_subclass.field_value.training_corpus_details.field_value[lang]
                || data.language_description_subclass.field_value.training_corpus_details.field_value[Object.keys(data.language_description_subclass.field_value.training_corpus_details.field_value)[0]];
            const trainingLabel = data.language_description_subclass.field_value.training_corpus_details.field_label[lang] || data.language_description_subclass.field_value.training_corpus_details.field_label["en"];
            return { label: trainingLabel, value: training_corpus_details };
        }
        return training_corpus_details || "";
    }

    gettraining_process_details(data, lang) {
        let training_process_details = data.language_description_subclass && data.language_description_subclass.field_value.training_process_details;
        if (training_process_details) {
            training_process_details = data.language_description_subclass.field_value.training_process_details.field_value[lang]
                || data.language_description_subclass.field_value.training_process_details.field_value[Object.keys(data.language_description_subclass.field_value.training_process_details.field_value)[0]];
            const trainingLabel = data.language_description_subclass.field_value.training_process_details.field_label[lang] || data.language_description_subclass.field_value.training_process_details.field_label["en"];
            return { label: trainingLabel, value: training_process_details };
        }
        return training_process_details || "";
    }

    getbias_details(data, lang) {
        let bias_details = data.language_description_subclass && data.language_description_subclass.field_value.bias_details;
        if (bias_details) {
            bias_details = data.language_description_subclass.field_value.bias_details.field_value[lang]
                || data.language_description_subclass.field_value.bias_details.field_value[Object.keys(data.language_description_subclass.field_value.bias_details.field_value)[0]];
            const trainingLabel = data.language_description_subclass.field_value.bias_details.field_label[lang] || data.language_description_subclass.field_value.bias_details.field_label["en"];
            return { label: trainingLabel, value: bias_details };
        }
        return bias_details || "";
    }

    getRequiresSoftware(data, lang) {
        let requires_softwareArray = [];
        if (data.requires_software && data.requires_software.field_value.length > 0) {
            data.requires_software.field_value.forEach((keyword, index) => {
                requires_softwareArray.push(keyword)
            });
            const requiresSoftwareLabel = data.requires_software.field_label[lang] || data.requires_software.field_label["en"];
            return { label: requiresSoftwareLabel, value: requires_softwareArray };
        }
        return null;
    }

    getRequiredHardware(data, lang) {
        let required_hardwareArray = [];
        if (data.required_hardware && data.required_hardware.field_value.length > 0) {
            data.required_hardware.field_value.forEach((keyword, index) => {
                required_hardwareArray.push(keyword.label[lang] || keyword.label[Object.keys(keyword.label)[0]]);
            });
            const requiredHardwareLabel = data.required_hardware.field_label[lang] || data.required_hardware.field_label["en"];
            return { label: requiredHardwareLabel, value: required_hardwareArray };
        }
        return null;
    }
}

const ldLrSubclassParser = new LdLrSubclassParser();
export default ldLrSubclassParser;