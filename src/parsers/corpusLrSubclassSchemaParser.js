class CorpusLrSubclassSchemaParser {
    constructor() {
      this.getLrType = this.getLrType.bind(this);
      this.getCorpusSubclass = this.getCorpusSubclass.bind(this);
      this.getCorpusMediaPart = this.getCorpusMediaPart.bind(this);
      this.getCorpusMediaPart = this.getCorpusMediaPart.bind(this);
      this.getDatasetDistribution = this.getDatasetDistribution.bind(this);
      this.getDomainKeywords=this.getDomainKeywords.bind(this);
      this.getSizeObject=this.getSizeObject.bind(this);
      this.getAudioGenreKeywords=this.getAudioGenreKeywords.bind(this);
      this.getSpeechGenreKeywords=this.getSpeechGenreKeywords.bind(this);
      this.getImageGenreKeywords=this.getImageGenreKeywords.bind(this);
      this.getVideoGenreKeywords=this.getVideoGenreKeywords.bind(this);

         
    }

    typeoffield = (type) => {
        switch (type) {
    
          case "field": return ("text")
          case "string": return ("text")
          case "list":  return ("list")
          case "choice" : return ("choice")
          case "boolean" : return ("boolean")
          case "nested object":return ("nested object")

          default: return ""
        }
    }

    getLrType(data) {
      let { help_text, label, required, type } = data.lr_type;
      type = this.typeoffield(type);
      return { "help_text": help_text, "label": label, "required": required, "type": type };
   }

  
  getCorpusSubclass(data) {
    let { help_text, label, required, type, choices } = data.corpus_subclass;
    type = this.typeoffield(type);
    return { "help_text": help_text, "label": label, "required": required, "type": type , "choices":choices};
 }

 getDomainKeywords(data) {
  let { help_text, label, required, type } = data.domain;

  let { required: pk_domain_required, read_only: pk_domain_read_only, label: pk_domain_label, type: pk_domain_type } = data.domain.child.children.pk;
  let { help_text: category_help_text, label: category_label, required: category_required, type: category_type, read_only: category_read_only } = data.domain.child.children.category_label;

  let { required: pk_identifier_required, read_only: pk_identifier_read_only, label: pk_identifier_label, type: pk_identifier_type } = data.domain.child.children.domain_identifier.children.pk;
  let { choices: domain_choices, help_text: domain_help_text, label: domain_label, read_only: domain_read_only, required: domain_required } = data.domain.child.children.domain_identifier.children.domain_classification_scheme;
  let { label: value_label, required: value_required, max_length: value_max_length } = data.domain.child.children.domain_identifier.children.value;
  type = this.typeoffield(type);
  //return { "help_text": help_text, "label": label, "required": required, "type": type, "choices": choices };
  return {
      help_text, label, required, type,
      pk_domain_required, pk_domain_read_only, pk_domain_label, pk_domain_type,
      category_help_text, category_label, category_required, category_type, category_read_only,
      pk_identifier_required, pk_identifier_read_only, pk_identifier_label, pk_identifier_type,
      domain_choices, domain_help_text, domain_label, domain_read_only, domain_required,
      value_label, value_required, value_max_length
  }
}

getTextGenreKeywords(data) {
  let { help_text, label, required, type } = data.text_genre;

  let { required: pk_text_genre_required, read_only: pk_text_genre_read_only, label: pk_text_genre_label, type: pk_text_genre_type } = data.text_genre.child.children.pk;
  let { help_text: category_help_text, label: category_label, required: category_required, type: category_type, read_only: category_read_only } = data.text_genre.child.children.category_label;

  let { required: pk_identifier_required, read_only: pk_identifier_read_only, label: pk_identifier_label, type: pk_identifier_type } = data.text_genre.child.children.text_genre_identifier.children.pk;
  let { choices: text_genre_choices, help_text: text_genre_help_text, label: text_genre_label, read_only: text_genre_read_only, required: text_genre_required } = data.text_genre.child.children.text_genre_identifier.children.text_genre_classification_scheme;
  let { label: value_label, required: value_required, max_length: value_max_length } = data.text_genre.child.children.text_genre_identifier.children.value;
  type = this.typeoffield(type);
  //return { "help_text": help_text, "label": label, "required": required, "type": type, "choices": choices };
  return {
      help_text, label, required, type,
      pk_text_genre_required, pk_text_genre_read_only, pk_text_genre_label, pk_text_genre_type,
      category_help_text, category_label, category_required, category_type, category_read_only,
      pk_identifier_required, pk_identifier_read_only, pk_identifier_label, pk_identifier_type,
      text_genre_choices, text_genre_help_text, text_genre_label, text_genre_read_only, text_genre_required,
      value_label, value_required, value_max_length
  }
}

getAudioGenreKeywords(data) {
  let { help_audio, label, required, type } = data.audio_genre;

  let { required: pk_audio_genre_required, read_only: pk_audio_genre_read_only, label: pk_audio_genre_label, type: pk_audio_genre_type } = data.audio_genre.child.children.pk;
  let { help_audio: category_help_audio, label: category_label, required: category_required, type: category_type, read_only: category_read_only } = data.audio_genre.child.children.category_label;

  let { required: pk_identifier_required, read_only: pk_identifier_read_only, label: pk_identifier_label, type: pk_identifier_type } = data.audio_genre.child.children.audio_genre_identifier.children.pk;
  let { choices: audio_genre_choices, help_audio: audio_genre_help_audio, label: audio_genre_label, read_only: audio_genre_read_only, required: audio_genre_required } = data.audio_genre.child.children.audio_genre_identifier.children.audio_genre_classification_scheme;
  let { label: value_label, required: value_required, max_length: value_max_length } = data.audio_genre.child.children.audio_genre_identifier.children.value;
  type = this.typeoffield(type);
  //return { "help_audio": help_audio, "label": label, "required": required, "type": type, "choices": choices };
  return {
      help_audio, label, required, type,
      pk_audio_genre_required, pk_audio_genre_read_only, pk_audio_genre_label, pk_audio_genre_type,
      category_help_audio, category_label, category_required, category_type, category_read_only,
      pk_identifier_required, pk_identifier_read_only, pk_identifier_label, pk_identifier_type,
      audio_genre_choices, audio_genre_help_audio, audio_genre_label, audio_genre_read_only, audio_genre_required,
      value_label, value_required, value_max_length
  }
}

getSpeechGenreKeywords(data) {
  let { help_speech, label, required, type } = data.speech_genre;

  let { required: pk_speech_genre_required, read_only: pk_speech_genre_read_only, label: pk_speech_genre_label, type: pk_speech_genre_type } = data.speech_genre.child.children.pk;
  let { help_speech: category_help_speech, label: category_label, required: category_required, type: category_type, read_only: category_read_only } = data.speech_genre.child.children.category_label;

  let { required: pk_identifier_required, read_only: pk_identifier_read_only, label: pk_identifier_label, type: pk_identifier_type } = data.speech_genre.child.children.speech_genre_identifier.children.pk;
  let { choices: speech_genre_choices, help_speech: speech_genre_help_speech, label: speech_genre_label, read_only: speech_genre_read_only, required: speech_genre_required } = data.speech_genre.child.children.speech_genre_identifier.children.speech_genre_classification_scheme;
  let { label: value_label, required: value_required, max_length: value_max_length } = data.speech_genre.child.children.speech_genre_identifier.children.value;
  type = this.typeoffield(type);
  //return { "help_speech": help_speech, "label": label, "required": required, "type": type, "choices": choices };
  return {
      help_speech, label, required, type,
      pk_speech_genre_required, pk_speech_genre_read_only, pk_speech_genre_label, pk_speech_genre_type,
      category_help_speech, category_label, category_required, category_type, category_read_only,
      pk_identifier_required, pk_identifier_read_only, pk_identifier_label, pk_identifier_type,
      speech_genre_choices, speech_genre_help_speech, speech_genre_label, speech_genre_read_only, speech_genre_required,
      value_label, value_required, value_max_length
  }
}
 
getVideoGenreKeywords(data) {
  let { help_video, label, required, type } = data.video_genre;

  let { required: pk_video_genre_required, read_only: pk_video_genre_read_only, label: pk_video_genre_label, type: pk_video_genre_type } = data.video_genre.child.children.pk;
  let { help_video: category_help_video, label: category_label, required: category_required, type: category_type, read_only: category_read_only } = data.video_genre.child.children.category_label;

  let { required: pk_identifier_required, read_only: pk_identifier_read_only, label: pk_identifier_label, type: pk_identifier_type } = data.video_genre.child.children.video_genre_identifier.children.pk;
  let { choices: video_genre_choices, help_video: video_genre_help_video, label: video_genre_label, read_only: video_genre_read_only, required: video_genre_required } = data.video_genre.child.children.video_genre_identifier.children.video_genre_classification_scheme;
  let { label: value_label, required: value_required, max_length: value_max_length } = data.video_genre.child.children.video_genre_identifier.children.value;
  type = this.typeoffield(type);
  //return { "help_video": help_video, "label": label, "required": required, "type": type, "choices": choices };
  return {
      help_video, label, required, type,
      pk_video_genre_required, pk_video_genre_read_only, pk_video_genre_label, pk_video_genre_type,
      category_help_video, category_label, category_required, category_type, category_read_only,
      pk_identifier_required, pk_identifier_read_only, pk_identifier_label, pk_identifier_type,
      video_genre_choices, video_genre_help_video, video_genre_label, video_genre_read_only, video_genre_required,
      value_label, value_required, value_max_length
  }
}

getImageGenreKeywords(data) {
  let { help_image, label, required, type } = data.image_genre;

  let { required: pk_image_genre_required, read_only: pk_image_genre_read_only, label: pk_image_genre_label, type: pk_image_genre_type } = data.image_genre.child.children.pk;
  let { help_image: category_help_image, label: category_label, required: category_required, type: category_type, read_only: category_read_only } = data.image_genre.child.children.category_label;

  let { required: pk_identifier_required, read_only: pk_identifier_read_only, label: pk_identifier_label, type: pk_identifier_type } = data.image_genre.child.children.image_genre_identifier.children.pk;
  let { choices: image_genre_choices, help_image: image_genre_help_image, label: image_genre_label, read_only: image_genre_read_only, required: image_genre_required } = data.image_genre.child.children.image_genre_identifier.children.image_genre_classification_scheme;
  let { label: value_label, required: value_required, max_length: value_max_length } = data.image_genre.child.children.image_genre_identifier.children.value;
  type = this.typeoffield(type);
  //return { "help_image": help_image, "label": label, "required": required, "type": type, "choices": choices };
  return {
      help_image, label, required, type,
      pk_image_genre_required, pk_image_genre_read_only, pk_image_genre_label, pk_image_genre_type,
      category_help_image, category_label, category_required, category_type, category_read_only,
      pk_identifier_required, pk_identifier_read_only, pk_identifier_label, pk_identifier_type,
      image_genre_choices, image_genre_help_image, image_genre_label, image_genre_read_only, image_genre_required,
      value_label, value_required, value_max_length
  }
}

    
 getCorpusMediaPart(data) {
  let { help_text, label, required, type } = data.corpus_media_part;
  type = this.typeoffield(type);
 
  let { required: id_media_part_required, read_only: id_media_part_read_only, label: id_media_part_label, type: id_media_part_type } = data.corpus_media_part.child.children.pk;
  let { help_text:  media_type_help_text, label: media_type_label, required: media_type_required, type: media_type_type, read_only: media_type_read_only } = data.corpus_media_part.child.children.corpus_media_type;
  let { help_text:  polymorphic_ctype_help_text, label: polymorphic_ctype_label, required: polymorphic_ctype_required, type:polymorphic_ctype_type, read_only: polymorphic_ctype_type_read_only } = data.corpus_media_part.child.children.polymorphic_ctype;
  let { help_text:  polymorphic_ctype_help_text, label: polymorphic_ctype_label, required: polymorphic_ctype_required, type:polymorphic_ctype_type, read_only: polymorphic_ctype_type_read_only } = data.corpus_media_part.child.children.polymorphic_ctype;
  let { help_text:  corpus_of_corpus_media_part_help_text, label: corpus_of_corpus_media_part_label, required: corpus_of_corpus_media_part_required, type:corpus_of_corpus_media_part_type, read_only: corpus_of_corpus_media_part_read_only } = data.corpus_media_part.child.children.corpus_of_corpus_media_part;


  return {  help_text,  label,  required,  type , id_media_part_required, id_media_part_read_only, id_media_part_label,id_media_part_type,
    media_type_help_text, media_type_label,  media_type_required,  media_type_type,  media_type_read_only ,
    polymorphic_ctype_help_text, polymorphic_ctype_label,  polymorphic_ctype_required, polymorphic_ctype_type,  polymorphic_ctype_type_read_only,
    polymorphic_ctype_help_text, polymorphic_ctype_label,  polymorphic_ctype_required, polymorphic_ctype_type,  polymorphic_ctype_type_read_only,
    corpus_of_corpus_media_part_help_text, corpus_of_corpus_media_part_label,  corpus_of_corpus_media_part_required, corpus_of_corpus_media_part_type,  corpus_of_corpus_media_part_read_only};
}

getSizeObject(data) {

  let { required: size_required, read_only: size_read_only, label: size_label, type: size_type} = data.size;
  let { required: amount_required, read_only: amount_read_only, label: amount_label, type: amount_type} = data.size.child.children.amount;
  let { required: size_unit_required, read_only: size_unit_read_only, label: size_unit_label, type: size_unit_type, choices: size_unit_choices} = data.size.child.children.amount;
  let { required: language_required, read_only: language_read_only, label: language_label, type: language_type} = data.size.child.children.language;
  let { required: language_tag_required, read_only: language_tag_read_only, label: language_tag_label, type: language_tag_type} = data.size.child.children.language.child.children.language_tag;
  let { required: language_id_required, read_only: language_id_read_only, label: language_id_label, type: language_id_type, choices:language_id_choices} = data.size.child.children.language.child.children.language_id;
  let { required: script_id_required, read_only: script_id_read_only, label: script_id_label, type: script_id_type, choices:script_id_choices} = data.size.child.children.language.child.children.script_id;
  let { required: region_id_required, read_only: region_id_read_only, label: region_id_label, type: region_id_type, choices:region_id_choices} = data.size.child.children.language.child.children.region_id;
  let { required: variant_id_required, read_only: variant_id_read_only, label: variant_id_label, type: variant_id_type, choices:variant_id_choices} = data.size.child.children.language.child.children.variant_id;  
  let { required: language_variety_name_required, read_only: language_variety_name_read_only, label: language_variety_name_label, type:language_variety_name_type} = data.size.child.children.language.child.children.language_variety_name;
  let image_genre = this.getImageGenreKeywords(data.size.image_genre);
  let video_genre = this.getVideoGenreKeywords(data.size.video_genre);
  let audio_genre = this.getAudioGenreKeywords(data.size.audio_genre);
  let text_genre = this.getTextGenreKeywords(data.size.text_genre);


  return { size_required,  size_read_only, size_label,  size_type,
  amount_required,  amount_read_only, amount_label,  amount_type, 
  size_unit_required,  size_unit_read_only, size_unit_label,  size_unit_type,  size_unit_choices , 
  language_required,  language_read_only, language_label,  language_type ,
  language_tag_required,  language_tag_read_only, language_tag_label,  language_tag_type ,
  language_id_required,  language_id_read_only, language_id_label,  language_id_type, language_id_choices ,
  script_id_required,  script_id_read_only, script_id_label,  script_id_type, script_id_choices ,
  region_id_required,  region_id_read_only, region_id_label,  region_id_type, region_id_choices ,
  variant_id_required,  variant_id_read_only, variant_id_label,  variant_id_type, variant_id_choices ,
  language_variety_name_required,  language_variety_name_read_only, language_variety_name_label, language_variety_name_type ,image_genre,video_genre,audio_genre,text_genre


  }

}
     
getDatasetDistribution(data) {
  let { help_text, label, required, type } = data.dataset_distribution;
  type = this.typeoffield(type);

  let { required: pk_distribution_required, read_only: pk_distribution_read_only, label: pk_distribution_label, type: pk_distribution_type } = data.dataset_distribution.child.children.pk;
  let { help_text: dataset_distribution_form_help_text, label: dataset_distribution_form_label, required: dataset_distribution_form_required, type: dataset_distribution_form_type, read_only: dataset_distribution_form_read_only , choises: dataset_distribution_form_choises} = data.dataset_distribution.child.children.dataset_distribution_form;
  let { required: distribution_location_required, read_only: distribution_location_read_only, label: distribution_location_label, type: distribution_location_type , max_length: distribution_location_max_length} = data.dataset_distribution.child.children.distribution_location;
  let { required: download_location_required, read_only: download_location_read_only, label: download_location_label, type: download_location_type , max_length: download_location_max_length} = data.dataset_distribution.child.children.download_location;
  let { required: access_location_required, read_only: access_location_read_only, label: access_location_label, type: access_location_type , max_length: access_location_max_length} = data.dataset_distribution.child.children.access_location;
  
  let { required: is_accessed_by_required, read_only: is_accessed_by_read_only, label: is_accessed_by_label, type: is_accessed_by_type} = data.dataset_distribution.child.children.is_accessed_by;
  let { required: pk_is_accessed_by_required, read_only: pk_is_accessed_by_read_only, label: pk_is_accessed_by_label, type: pk_is_accessed_by_type} = data.dataset_distribution.child.children.is_accessed_by.child.children.pk;  
  let { required: is_accessed_by_resource_name_required, read_only: is_accessed_by_resource_name_read_only, label: is_accessed_by_resource_name_label, type: is_accessed_by_resource_name_type, help_text:is_accessed_by_resource_name_help_text  } = data.dataset_distribution.child.children.is_accessed_by.child.children.resource_name;
  let { required: is_accessed_by_lr_identifier_required, read_only: is_accessed_by_lr_identifier_read_only, label: is_accessed_by_lr_identifier_label, type: is_accessed_by_lr_identifier_type , help_text:is_accessed_by_lr_identifier_help_text } = data.dataset_distribution.child.children.is_accessed_by.child.children.lr_identifier;
  let { required: is_accessed_by_lr_pk_identifier_required, read_only: is_accessed_by_lr_identifier_pk_read_only, label: is_accessed_by_lr_identifier_pk_label, type: is_accessed_by_lr_identifier_pk_type} = data.dataset_distribution.child.children.is_accessed_by.child.children.lr_identifier.child.children.pk;
  let { required: is_accessed_by_lr_scheme_identifier_required, read_only: is_accessed_by_lr_identifier_scheme_read_only, label: is_accessed_by_lr_identifier_scheme_label, type: is_accessed_by_lr_identifier_scheme_type, help_text:is_accessed_by_lr_identifier_scheme_help_text, choices: is_accessed_by_lr_identifier_scheme_choices} = data.dataset_distribution.child.children.is_accessed_by.child.children.lr_identifier.child.children.lr_identifier_scheme;
  let { label: is_accessed_by_value_label, required: is_accessed_by_value_required, max_length: is_accessed_by_value_max_length } =data.dataset_distribution.child.children.is_accessed_by.child.children.lr_identifier.child.children.value;
  let { label: is_accessed_by_version_label, required: is_accessed_by_version_required, type: is_accessed_by_version_type, help_text: is_accessed_by_version_help_text, max_length:is_accessed_by_version_max_length, help_text:is_accessed_by_value_help_text } = data.dataset_distribution.child.children.is_accessed_by.child.children.version;
  
  let { required: is_displayed_by_required, read_only: is_displayed_by_read_only, label: is_displayed_by_label, type: is_displayed_by_type} = data.dataset_distribution.child.children.is_displayed_by;
  let { required: pk_is_displayed_by_required, read_only: pk_is_displayed_by_read_only, label: pk_is_displayed_by_label, type: pk_is_displayed_by_type} = data.dataset_distribution.child.children.is_displayed_by.child.children.pk;  
  let { required: is_displayed_by_resource_name_required, read_only: is_displayed_by_resource_name_read_only, label: is_displayed_by_resource_name_label, type: is_displayed_by_resource_name_type, help_text:is_displayed_by_resource_name_help_text  } = data.dataset_distribution.child.children.is_displayed_by.child.children.resource_name;
  let { required: is_displayed_by_lr_identifier_required, read_only: is_displayed_by_lr_identifier_read_only, label: is_displayed_by_lr_identifier_label, type: is_displayed_by_lr_identifier_type , help_text:is_displayed_by_lr_identifier_help_text } = data.dataset_distribution.child.children.is_displayed_by.child.children.lr_identifier;
  let { required: is_displayed_by_lr_pk_identifier_required, read_only: is_displayed_by_lr_identifier_pk_read_only, label: is_displayed_by_lr_identifier_pk_label, type: is_displayed_by_lr_identifier_pk_type} = data.dataset_distribution.child.children.is_displayed_by.child.children.lr_identifier.child.children.pk;
  let { required: is_displayed_by_lr_scheme_identifier_required, read_only: is_displayed_by_lr_identifier_scheme_read_only, label: is_displayed_by_lr_identifier_scheme_label, type: is_displayed_by_lr_identifier_scheme_type, help_text:is_displayed_by_lr_identifier_scheme_help_text, choices: is_displayed_by_lr_identifier_scheme_choices} = data.dataset_distribution.child.children.is_displayed_by.child.children.lr_identifier.child.children.lr_identifier_scheme;
  let { label: is_displayed_by_value_label, required: is_displayed_by_value_required, max_length: is_displayed_by_value_max_length } =data.dataset_distribution.child.children.is_displayed_by.child.children.lr_identifier.child.children.value;
  let { label: is_displayed_by_version_label, required: is_displayed_by_version_required, type: is_displayed_by_version_type, help_text: is_displayed_by_version_help_text, max_length:is_displayed_by_version_max_length, help_text:is_displayed_by_value_help_text } = data.dataset_distribution.child.children.is_displayed_by.child.children.version;

  let { required: is_queried_by_required, read_only: is_queried_by_read_only, label: is_queried_by_label, type: is_queried_by_type} = data.dataset_distribution.child.children.is_queried_by;
  let { required: pk_is_queried_by_required, read_only: pk_is_queried_by_read_only, label: pk_is_queried_by_label, type: pk_is_queried_by_type} = data.dataset_distribution.child.children.is_queried_by.child.children.pk;  
  let { required: is_queried_by_resource_name_required, read_only: is_queried_by_resource_name_read_only, label: is_queried_by_resource_name_label, type: is_queried_by_resource_name_type, help_text:is_queried_by_resource_name_help_text  } = data.dataset_distribution.child.children.is_queried_by.child.children.resource_name;
  let { required: is_queried_by_lr_identifier_required, read_only: is_queried_by_lr_identifier_read_only, label: is_queried_by_lr_identifier_label, type: is_queried_by_lr_identifier_type , help_text:is_queried_by_lr_identifier_help_text } = data.dataset_distribution.child.children.is_queried_by.child.children.lr_identifier;
  let { required: is_queried_by_lr_pk_identifier_required, read_only: is_queried_by_lr_identifier_pk_read_only, label: is_queried_by_lr_identifier_pk_label, type: is_queried_by_lr_identifier_pk_type} = data.dataset_distribution.child.children.is_queried_by.child.children.lr_identifier.child.children.pk;
  let { required: is_queried_by_lr_scheme_identifier_required, read_only: is_queried_by_lr_identifier_scheme_read_only, label: is_queried_by_lr_identifier_scheme_label, type: is_queried_by_lr_identifier_scheme_type, help_text:is_queried_by_lr_identifier_scheme_help_text, choices: is_queried_by_lr_identifier_scheme_choices} = data.dataset_distribution.child.children.is_queried_by.child.children.lr_identifier.child.children.lr_identifier_scheme;
  let { label: is_queried_by_value_label, required: is_queried_by_value_required, max_length: is_queried_by_value_max_length } =data.dataset_distribution.child.children.is_queried_by.child.children.lr_identifier.child.children.value;
  let { label: is_queried_by_version_label, required: is_queried_by_version_required, type: is_queried_by_version_type, help_text: is_queried_by_version_help_text, max_length:is_queried_by_version_max_length, help_text:is_queried_by_value_help_text } = data.dataset_distribution.child.children.is_queried_by.child.children.version;

  let { help_text:samples_location_help_text, label:samples_location_label, required:samples_location_required, type:samples_location_type } = data.dataset_distribution.child.children.samples_location;
  let {  label:samples_location_url_label, required:samples_location_url_required, type:samples_location_url_type , max_length:samples_location_url_max_length} = data.dataset_distribution.child.children.samples_location.child; 

  let { help_text:distribution_text_feature_help_text, label:distribution_text_feature_label, required:distribution_text_feature_location_required, type:distribution_text_feature_location_type } = data.dataset_distribution.child.children.distribution_text_feature;
  let { required: pk_distribution_text_feature_required, read_only: pk_distribution_text_feature_read_only, label: pk_distribution_text_feature_label, type: pk_distribution_text_feature_type} = data.dataset_distribution.child.children.distribution_text_feature.child.children.pk;
  let  domain_distribution_text_feature  = this.getDomainKeywords(data.dataset_distribution.child.children.distribution_text_feature.child.children.size.child.children.domain);
  let  size_distribution_text_feature = this.getSizeObject(data.dataset_distribution.child.children.distribution_text_feature.child.children.size);
  let { required: data_format_distribution_text_feature_required, read_only: data_format_distribution_text_feature_read_only, label: data_format_distribution_text_feature_label, type: data_format_distribution_text_feature_type} = data.dataset_distribution.child.children.distribution_text_feature.child.children.data_format;
  let { data_format_choices} = data.dataset_distribution.child.children.distribution_text_feature.child.children.data_format.child.choices;
  let { required: character_encoding_distribution_text_feature_required, read_only: character_encoding_distribution_text_feature_read_only, label: character_encoding_distribution_text_feature_label, type: character_encoding_distribution_text_feature_type} = data.dataset_distribution.child.children.distribution_text_feature.child.children.character_encoding;
  let { character_encoding_choices} = data.dataset_distribution.child.children.distribution_text_feature.child.children.data_format.child.choices;


  pk_distribution_type = this.typeoffield(pk_distribution_type);
  dataset_distribution_form_type = this.typeoffield(dataset_distribution_form_type);
  distribution_location_type = this.typeoffield(distribution_location_type);
  download_location_type = this.typeoffield(download_location_type);
  access_location_type = this.typeoffield(access_location_type);
  is_accessed_by_type = this.typeoffield(is_accessed_by_type);
  pk_is_accessed_by_type = this.typeoffield(pk_is_accessed_by_type);
  is_accessed_by_resource_name_type = this.typeoffield(is_accessed_by_resource_name_type);
  is_accessed_by_lr_identifier_type = this.typeoffield(is_accessed_by_lr_identifier_type);
  is_accessed_by_lr_identifier_pk_type = this.typeoffield(is_accessed_by_lr_identifier_pk_type);
  is_accessed_by_lr_identifier_scheme_type = this.typeoffield(is_accessed_by_lr_identifier_scheme_type);
  is_accessed_by_lr_identifier_type = this.typeoffield(is_accessed_by_lr_identifier_type);
  is_accessed_by_version_type = this.typeoffield(is_accessed_by_version_type);
  is_displayed_by_type = this.typeoffield(is_displayed_by_type);
  pk_is_displayed_by_type = this.typeoffield(pk_is_displayed_by_type);
  is_displayed_by_resource_name_type = this.typeoffield(is_displayed_by_resource_name_type);
  is_displayed_by_lr_identifier_type = this.typeoffield(is_displayed_by_lr_identifier_type);
  is_displayed_by_lr_identifier_pk_type = this.typeoffield(is_displayed_by_lr_identifier_pk_type);
  is_displayed_by_lr_identifier_scheme_type = this.typeoffield(is_displayed_by_lr_identifier_scheme_type);
  is_displayed_by_version_type = this.typeoffield(is_displayed_by_version_type);
  is_queried_by_type = this.typeoffield(is_queried_by_type);
  is_queried_by_resource_name_type = this.typeoffield(is_queried_by_resource_name_type);
  is_queried_by_lr_identifier_type = this.typeoffield(is_queried_by_lr_identifier_type);
  is_queried_by_lr_identifier_scheme_type = this.typeoffield(is_queried_by_lr_identifier_scheme_type);
  is_queried_by_version_type = this.typeoffield(is_queried_by_version_type);
  samples_location_type = this.typeoffield(samples_location_type);
  samples_location_url_type = this.typeoffield(samples_location_url_type);
  distribution_text_feature_location_type = this.typeoffield(distribution_text_feature_location_type);
  pk_distribution_text_feature_type = this.typeoffield(pk_distribution_text_feature_type);
  size_distribution_text_feature_type = this.typeoffield(size_distribution_text_feature_type);
  amount_distribution_text_feature_type = this.typeoffield(amount_distribution_text_feature_type);
  size_unit_distribution_text_feature_type = this.typeoffield(size_unit_distribution_text_feature_type);


  

  return {
      help_text, label, required, type,
      pk_distribution_required, pk_distribution_read_only, pk_distribution_label, pk_distribution_type,
      dataset_distribution_form_help_text, dataset_distribution_form_label, dataset_distribution_form_required, dataset_distribution_form_type, dataset_distribution_form_read_only,dataset_distribution_form_choises,
      distribution_location_required, distribution_location_read_only, distribution_location_label, distribution_location_type,distribution_location_max_length,
      access_location_required,   access_location_read_only,   access_location_label,  access_location_type ,   access_location_max_length,
      download_location_required,   download_location_read_only,   download_location_label,  download_location_type ,   download_location_max_length ,
      distribution_location_required,   distribution_location_read_only,   distribution_location_label,  distribution_location_type ,   distribution_location_max_length ,
      is_accessed_by_required , is_accessed_by_read_only, is_accessed_by_label, is_accessed_by_type,
      pk_is_accessed_by_required,  pk_is_accessed_by_read_only, pk_is_accessed_by_label, pk_is_accessed_by_type,
      is_accessed_by_resource_name_required,  is_accessed_by_resource_name_read_only, is_accessed_by_resource_name_label, is_accessed_by_resource_name_type,is_accessed_by_resource_name_help_text,
      is_accessed_by_lr_identifier_required,  is_accessed_by_lr_identifier_read_only, is_accessed_by_lr_identifier_label, is_accessed_by_lr_identifier_type,is_accessed_by_lr_identifier_help_text,
      is_accessed_by_lr_pk_identifier_required,  is_accessed_by_lr_identifier_pk_read_only, is_accessed_by_lr_identifier_pk_label, is_accessed_by_lr_identifier_pk_type,
      is_accessed_by_lr_scheme_identifier_required,  is_accessed_by_lr_identifier_scheme_read_only, is_accessed_by_lr_identifier_scheme_label, is_accessed_by_lr_identifier_scheme_type,  is_accessed_by_lr_identifier_scheme_choices,is_accessed_by_lr_identifier_scheme_help_text,
     is_accessed_by_value_label,  is_accessed_by_value_required,  is_accessed_by_value_max_length,
     is_accessed_by_version_label,  is_accessed_by_version_required, is_accessed_by_version_type, is_accessed_by_version_help_text, is_accessed_by_version_max_length ,is_accessed_by_value_help_text,
     is_displayed_by_required , is_displayed_by_read_only, is_displayed_by_label, is_displayed_by_type,
      pk_is_displayed_by_required,  pk_is_displayed_by_read_only, pk_is_displayed_by_label, pk_is_displayed_by_type,
      is_displayed_by_resource_name_required,  is_displayed_by_resource_name_read_only, is_displayed_by_resource_name_label, is_displayed_by_resource_name_type,is_displayed_by_resource_name_help_text,
      is_displayed_by_lr_identifier_required,  is_displayed_by_lr_identifier_read_only, is_displayed_by_lr_identifier_label, is_displayed_by_lr_identifier_type,is_displayed_by_lr_identifier_help_text,
      is_displayed_by_lr_pk_identifier_required,  is_displayed_by_lr_identifier_pk_read_only, is_displayed_by_lr_identifier_pk_label, is_displayed_by_lr_identifier_pk_type,
      is_displayed_by_lr_scheme_identifier_required,  is_displayed_by_lr_identifier_scheme_read_only, is_displayed_by_lr_identifier_scheme_label, is_displayed_by_lr_identifier_scheme_type,  is_displayed_by_lr_identifier_scheme_choices,is_displayed_by_lr_identifier_scheme_help_text,
     is_displayed_by_value_label,  is_displayed_by_value_required,  is_displayed_by_value_max_length,
     is_displayed_by_version_label,  is_displayed_by_version_required, is_displayed_by_version_type, is_displayed_by_version_help_text, is_displayed_by_version_max_length ,is_displayed_by_value_help_text,
     is_queried_by_required , is_queried_by_read_only, is_queried_by_label, is_queried_by_type,
      pk_is_queried_by_required,  pk_is_queried_by_read_only, pk_is_queried_by_label, pk_is_queried_by_type,
      is_queried_by_resource_name_required,  is_queried_by_resource_name_read_only, is_queried_by_resource_name_label, is_queried_by_resource_name_type,is_queried_by_resource_name_help_text,
      is_queried_by_lr_identifier_required,  is_queried_by_lr_identifier_read_only, is_queried_by_lr_identifier_label, is_queried_by_lr_identifier_type,is_queried_by_lr_identifier_help_text,
      is_queried_by_lr_pk_identifier_required,  is_queried_by_lr_identifier_pk_read_only, is_queried_by_lr_identifier_pk_label, is_queried_by_lr_identifier_pk_type,
      is_queried_by_lr_scheme_identifier_required,  is_queried_by_lr_identifier_scheme_read_only, is_queried_by_lr_identifier_scheme_label, is_queried_by_lr_identifier_scheme_type,  is_queried_by_lr_identifier_scheme_choices,is_queried_by_lr_identifier_scheme_help_text,
     is_queried_by_value_label,  is_queried_by_value_required,  is_queried_by_value_max_length,
     is_queried_by_version_label,  is_queried_by_version_required, is_queried_by_version_type, is_queried_by_version_help_text, is_queried_by_version_max_length ,is_queried_by_value_help_text,
     samples_location_help_text, samples_location_label, samples_location_required, samples_location_type ,
  samples_location_url_label, samples_location_url_required, samples_location_url_type , samples_location_url_max_length,
distribution_text_feature_help_text, distribution_text_feature_label, distribution_text_feature_location_required, distribution_text_feature_location_type ,
  pk_distribution_text_feature_required, pk_distribution_text_feature_read_only,  pk_distribution_text_feature_label,  pk_distribution_text_feature_type, domain_distribution_text_feature , size_distribution_text_feature, 
  data_format_distribution_text_feature_required, data_format_distribution_text_feature_read_only,  data_format_distribution_text_feature_label,  data_format_distribution_text_feature_type,  data_format_choices, 
  character_encoding_distribution_text_feature_required, character_encoding_distribution_text_feature_read_only,  character_encoding_distribution_text_feature_label,  character_encoding_distribution_text_feature_type,  character_encoding_choices
  }
}   


getPersonalDataIncluded(data) {
  let { help_text, label, required, type } = data.personal_data_included;
  type = this.typeoffield(type);
  return { "help_text": help_text, "label": label, "required": required, "type": type };
}

getPersonalDataDetails(data) {
  let { help_text, label, required, type } = data.personal_data_details;
  type = this.typeoffield(type);
  return { "help_text": help_text, "label": label, "required": required, "type": type };
}

getSensitiveDataIncluded(data) {
  let { help_text, label, required, type } = data.sensitive_data_included;
  type = this.typeoffield(type);
  return { "help_text": help_text, "label": label, "required": required, "type": type };
}


getSensitiveDataDetails(data) {
  let { help_text, label, required, type } = data.sensitive_data_details;
  type = this.typeoffield(type);
  return { "help_text": help_text, "label": label, "required": required, "type": type };
}


getAnonymized(data) {
  let { help_text, label, required, type } = data.anonymized;
  type = this.typeoffield(type);
  return { "help_text": help_text, "label": label, "required": required, "type": type };
}


getAnonymizationDetails(data) {
  let { help_text, label, required, type } = data.anonymization_details;
  type = this.typeoffield(type);
  return { "help_text": help_text, "label": label, "required": required, "type": type };
}


}


const corpusLrSubclassSchemaParser = new CorpusLrSubclassSchemaParser();

export default corpusLrSubclassSchemaParser;