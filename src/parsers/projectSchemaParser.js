class ProjectScemaParser {
    constructor() {

        //keep them
        this.getLogo = this.getLogo.bind(this);
        this.getKeywords = this.getKeywords.bind(this);
        this.getDomainKeywords = this.getDomainKeywords.bind(this);
        this.getLtAreaKeywords = this.getLtAreaKeywords.bind(this);
        this.getSubjectKeywords = this.getSubjectKeywords.bind(this);
        this.getSocialMedia = this.getSocialMedia.bind(this);


        ///see if we can remove them and use generic parser
        this.getFundingType = this.getFundingType.bind(this);
        this.getFundingCountry = this.getFundingCountry.bind(this);
        this.getGrantNumber = this.getGrantNumber.bind(this);
        this.getFundingSchemeCategory = this.getFundingSchemeCategory.bind(this);
        this.getFundingType = this.getFundingType.bind(this);
        this.getFundingCountry = this.getFundingCountry.bind(this);
        this.getRelatedCall = this.getRelatedCall.bind(this);
        this.getRelatedSubprogramme = this.getRelatedSubprogramme.bind(this);
        this.getRelatedProgramme = this.getRelatedProgramme.bind(this);
        this.getEcMaxContribution = this.getEcMaxContribution.bind(this);
        this.getNationalMaxContribution = this.getNationalMaxContribution.bind(this);

    }

    typeoffield = (type) => {
        switch (type) {

            case "field": return ("text")
            case "string": return ("text")
            case "list": return ("list")
            case "choice": return ("choice")
            case "boolean": return ("boolean")
            case "nested object": return ("nested object")

            default: return ""
        }
    }



    getEcMaxContribution(data) {
        let { help_text, label, required, type, children } = data.ec_max_contribution;
        type = this.typeoffield(type);
        return { "help_text": help_text, "label": label, "required": required, "type": type, "children": children };
    }

    getNationalMaxContribution(data) {
        let { help_text, label, required, type, children } = data.national_max_contribution;
        type = this.typeoffield(type);

        return { "help_text": help_text, "label": label, "required": required, "type": type, "children": children };
    }


    getFundingType(data) {
        let { help_text, label, required, type } = data.funding_type;
        let { choices } = data.funding_type.child;
        type = this.typeoffield(type);

        return { "help_text": help_text, "label": label, "required": required, "type": type, "choices": choices };
    }

    getFundingCountry(data) {
        let { help_text, label, required, type } = data.funding_country;
        let { choices } = data.funding_country.child;
        type = this.typeoffield(type);

        return { "help_text": help_text, "label": label, "required": required, "type": type, "choices": choices };
    }


    getGrantNumber(data) {
        let { help_text, label, required, type, max_length, placeholder } = data.grant_number;
        type = this.typeoffield(type);

        return { "help_text": help_text, "label": label, "required": required, "type": type, "max_length": max_length, "placeholder": placeholder };
    }

    getFundingSchemeCategory(data) {
        let { help_text, label, required, type, max_length } = data.funding_scheme_category;
        type = this.typeoffield(type);

        return { "help_text": help_text, "label": label, "required": required, "type": type, "max_length": max_length };
    }

    getRelatedCall(data) {
        let { help_text, label, required, type, max_length } = data.related_call;
        type = this.typeoffield(type);

        return { "help_text": help_text, "label": label, "required": required, "type": type, "max_length": max_length };
    }
    getRelatedProgramme(data) {
        let { help_text, label, required, type, max_length } = data.related_programme;
        type = this.typeoffield(type);

        return { "help_text": help_text, "label": label, "required": required, "type": type, "max_length": max_length };
    }
    getRelatedSubprogramme(data) {
        let { help_text, label, required, type, max_length } = data.related_subprogramme;
        type = this.typeoffield(type);

        return { "help_text": help_text, "label": label, "required": required, "type": type, "max_length": max_length };
    }





    /////keep only those

    getLogo(data) {
        let { help_text, label, required, type, max_length } = data.logo;
        type = this.typeoffield(type);

        return { "help_text": help_text, "label": label, "required": required, "type": type, "max_length": max_length };
    }

    getKeywords(data) {
        let { help_text, label, required, type } = data.keyword;
        type = this.typeoffield(type);

        return { "help_text": help_text, "label": label, "required": required, "type": type };
    }

    getLtAreaKeywords(data) {
        let { help_text, label, required, type, placeholder } = data.lt_area;
        type = this.typeoffield(type);
        const { recommended_choices, max_length } = data.lt_area.child;

        return {
            "help_text": help_text, "label": label, "required": required, "type": type,
            "recommended_choices": recommended_choices, "max_length": max_length, "placeholder": placeholder
        };
    }

    getDomainKeywords(data) {
        let { help_text, label, required, type } = data.domain;

        let { required: pk_domain_required, read_only: pk_domain_read_only, label: pk_domain_label, type: pk_domain_type } = data.domain.child.children.pk;
        let { help_text: category_help_text, label: category_label, required: category_required, type: category_type, read_only: category_read_only, placeholder: category_placeholder } = data.domain.child.children.category_label;

        let { required: pk_identifier_required, read_only: pk_identifier_read_only, label: pk_identifier_label, type: pk_identifier_type } = data.domain.child.children.domain_identifier.children.pk;
        let { choices: domain_choices, help_text: domain_help_text, label: domain_label, read_only: domain_read_only, required: domain_required } = data.domain.child.children.domain_identifier.children.domain_classification_scheme;
        let { label: value_label, required: value_required, max_length: value_max_length, placeholder: value_placeholder, help_text: value_help_text } = data.domain.child.children.domain_identifier.children.value;
        let { label: domain_identifier_label, help_text: domain_identifier_help_text } = data.domain.child.children.domain_identifier;
        type = this.typeoffield(type);

        return {
            help_text, label, required, type,
            pk_domain_required, pk_domain_read_only, pk_domain_label, pk_domain_type,
            category_help_text, category_label, category_required, category_type, category_read_only, category_placeholder,
            pk_identifier_required, pk_identifier_read_only, pk_identifier_label, pk_identifier_type,
            domain_choices, domain_help_text, domain_label, domain_read_only, domain_required,
            value_label, value_required, value_max_length, value_placeholder, value_help_text,
            domain_identifier_label, domain_identifier_help_text
        }
    }

    getSubjectKeywords(data) {
        let { help_text, label, required, type } = data.subject;

        let { required: pk_subject_required, read_only: pk_subject_read_only, label: pk_subject_label, type: pk_subject_type } = data.subject.child.children.pk;
        let { help_text: category_help_text, label: category_label, required: category_required, type: category_type, read_only: category_read_only, placeholder: category_placeholder } = data.subject.child.children.category_label;

        let { required: pk_identifier_required, read_only: pk_identifier_read_only, label: pk_identifier_label, type: pk_identifier_type } = data.subject.child.children.subject_identifier.children.pk;
        let { choices: subject_choices, help_text: subject_help_text, label: subject_label, read_only: subject_read_only, required: subject_required } = data.subject.child.children.subject_identifier.children.subject_classification_scheme;
        let { label: value_label, required: value_required, max_length: value_max_length, placeholder: value_placeholder } = data.subject.child.children.subject_identifier.children.value;
        let { label: subject_identifier_label, help_text: subject_identifier_help_text } = data.subject.child.children.subject_identifier;
        type = this.typeoffield(type);

        return {
            help_text, label, required, type,
            pk_subject_required, pk_subject_read_only, pk_subject_label, pk_subject_type,
            category_help_text, category_label, category_required, category_type, category_read_only, category_placeholder,
            pk_identifier_required, pk_identifier_read_only, pk_identifier_label, pk_identifier_type,
            subject_choices, subject_help_text, subject_label, subject_read_only, subject_required,
            value_label, value_required, value_max_length, value_placeholder,
            subject_identifier_label, subject_identifier_help_text
        }
    }




    getSocialMedia(data) {
        let { help_text, label, required, type } = data.social_media_occupational_account;
        type = this.typeoffield(type);
        let { required: social_media_required, label: social_media_label, type: social_media_type, help_text: social_media_help_text, choices: social_media_choices } = data.social_media_occupational_account.child.children.social_media_occupational_account_type;
        let { required: value_required, label: value_label, type: value_type, max_length: value_max_length, placeholder: value_placeholder, help_text: value_help_text } = data.social_media_occupational_account.child.children.value;


        return {
            help_text, label, required, type,
            social_media_required, social_media_label, social_media_type, social_media_choices, social_media_help_text,
            value_label, value_required, value_max_length, value_type, value_placeholder, value_help_text
        }
    }


}


const projectSchemaParser = new ProjectScemaParser();

export default projectSchemaParser;