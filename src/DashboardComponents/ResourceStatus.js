import React from "react";
import Grid from '@material-ui/core/Grid';
import { EDITOR_PERMITTED_ROLES, AUTHENTICATED_KEYCLOAK_USER_ROLES } from "../config/constants";
import messages from "../config/messages";
import Chip from '@material-ui/core/Chip';

export default class ResourceStatus extends React.Component {
    constructor(props) {
        super(props);
        this.state = { keycloak: props.keycloak, UserRoles: [] };
        this.isAuthorizedToView = this.isAuthorizedToView.bind(this);
    }

    componentDidMount() {
        const { keycloak } = this.props;
        this.setState({ UserRoles: AUTHENTICATED_KEYCLOAK_USER_ROLES(keycloak) });
    }

    isAuthorizedToView(roles) {
        var isAuthorized = false;
        EDITOR_PERMITTED_ROLES.forEach(item => {
            if (roles.includes(item)) {
                isAuthorized = true;
            }
        })
        return isAuthorized;
    }

    render() {
        const { status, xSize, unpublication_requested = false } = this.props;         
        if (!this.state.UserRoles.length === 0) {
            return <div></div>
        }
        const isAuth = this.isAuthorizedToView(this.state.UserRoles);
        if (!isAuth) {
            return <div></div>
        }

        if (unpublication_requested === true) {
            return <Grid container direction="column" justifyContent="center" alignItems="center">
                <Grid item xs={xSize}>
                    <Chip size="small"  label={"requested for unpublish"} className="ChipTagYellowOutlined" /> 
                     
                </Grid>
            </Grid>
        }

        return (
            <Grid container direction="row" justifyContent="flex-start" alignItems="center">
                <Grid item xs={xSize} >
                    {(status === "p" || status === "published") && <Chip size="small"  label={messages.published_tag} className="ChipTagGreenOutlined" /> }
                    {(status === "g" || status === "ingested") && <Chip size="small"  label={messages.ingested_tag} className="ChipTagDarkPinkOutlined" />}
                    {(status === "i" || status === "internal") && <Chip size="small"  label={messages.internal_tag} className="ChipTagPinkOutlined" /> }
                    {(status === "d" || status === "draft") && <Chip size="small"  label={messages.draft_tag} className="ChipTagLightPinkOutlined" /> }
                </Grid>
            </Grid>
        );

    }
}