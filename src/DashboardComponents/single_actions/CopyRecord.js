import React from "react";
import { withRouter } from "react-router-dom";
import axios from "axios";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
//import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Tooltip from '@material-ui/core/Tooltip';
import FileCopyIcon from '@material-ui/icons/FileCopy';
//import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
//import CircularProgress from '@material-ui/core/CircularProgress';
import { toast } from "react-toastify";
import { DASHBOARD_COPY_RECORD } from "../../config/editorConstants";
import { TOOL_SERVICE, CORPUS, LD, LCR, PROJECT, ORGANIZATION, ML_MODEL, GRAMMAR, OTHER, Uncategorized_Language_Description } from "../../config/constants";
import messages from "../../config/messages";
import OpenInNewIcon from '@material-ui/icons/OpenInNew';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';

class CopyRecord extends React.Component {
    constructor(props) {
        super(props);
        this.state = { displayDialog: true, loading: false, source: null, error: null, version: "", resource_name: "", given_name: "", functional_service: false };
    }

    componentWillUnmount = () => {
        if (this.state.source) {
            this.state.source.cancel("");
        }
    }

    updateDate = (field, newDate) => {
        this.setState({ date: newDate });
    }

    disableDisplay = () => {
        if (this.state.source) {
            this.state.source.cancel("");
        }
        this.setState({ displayDialog: true, loading: false, source: null, error: null, version: "", resource_name: "", given_name: "", functional_service: false });
        this.props.hideCopyRecord(this.props.action);
    }

    handleSubmit = (payload) => {
        if (this.state.source) {
            this.state.source.cancel("");
        }
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        this.setState({ source: source, loading: true });
        axios.patch(DASHBOARD_COPY_RECORD(this.props.resource.id), payload, { cancelToken: source.token })
            .then(res => { this.setState({ loading: false, source: null }); this.successResponse(res) })
            .catch(err => { this.setState({ loading: false, source: null }); this.errorResponse(err) })
    }

    successResponse = (response) => {
        toast.success("Success", { autoClose: 3500 });
        this.redirectToLandingPage(response);
        //this.disableDisplay();
    }

    errorResponse = (responseError) => {
        toast.error("Your request failed.", { autoClose: 3500 });
        let errData = '';
        try {
            const isDuplicateError = this.duplicateName(responseError.response.data);
            const isVersionError = this.versionError(responseError.response.data);
            if (isDuplicateError && isDuplicateError.length > 0) {
                errData = isDuplicateError[0]
            } else if (isVersionError && isVersionError.length > 0) {
                errData = isVersionError[0]
            } else {
                errData = responseError.response.data;
            }
        } catch (err) {
            errData = 'The request has failed. Please contact the ELG team.'
        }
        this.setState({ error: errData });//render json as error report
    }

    duplicateName = (errData) => {
        if (errData.hasOwnProperty("duplication_error")) {
            const duplication_error = errData.duplication_error;
            const duplicate_records = errData.duplicate_records;
            if (duplication_error && duplicate_records) {
                const errorMessageArray = [];
                //duplication_error.map((errorMessage, index) => errorMessageArray.push(errorMessage));
                //duplicate_records.map((errorMessage, index) => errorMessageArray.push(errorMessage));
                errorMessageArray.push("There is already a record with this name. Please enter a different name.")
                return errorMessageArray;
            }
        } else {
            return null;
        }
    }

    versionError = (errData) => {
        if (errData.hasOwnProperty("described_entity") && errData.described_entity.hasOwnProperty("version")) {
            const veresionErrorArray = errData.described_entity.version.map(item => item);
            return veresionErrorArray;
        } else {
            return null;
        }
    }

    ShowErrorMessage(error) {
        return <div>
            {error && <div >
                <h3>Error</h3>
                <div className="boxed">
                    <Paper elevation={13} >
                        <code>
                            <pre id="special">
                                {JSON.stringify(error)}
                            </pre>
                        </code>
                    </Paper>
                </div>
            </div>
            }
        </div>
    }

    redirectToLandingPage = (response) => {
        let url = '/myItems';
        try {
            const pk = response.data.new_resource_pk;
            switch (this.props.resource.resource_type) {
                case PROJECT: url = `/project/${pk}`; break;
                case ORGANIZATION: url = `/organization/${pk}`; break;
                case TOOL_SERVICE: url = `/tool-service/${pk}`; break;
                case CORPUS: url = `/corpus/${pk}`; break;
                case LCR: url = `/lcr/${pk}`; break;
                case LD: url = `/ld/${pk}`; break;
                case ML_MODEL: url = `/ld/${pk}`; break;
                case GRAMMAR: url = `/ld/${pk}`; break;
                case OTHER: url = `/ld/${pk}`; break;
                case Uncategorized_Language_Description: url = `/ld/${pk}`; break;
                case "ToolService": url = `/tool-service/${pk}`; break;
                case "LexicalConceptualResource": url = `/lcr/${pk}`; break;
                case "LanguageDescription": url = `/ld/${pk}`; break;
                default: url = `/myItems`; break;
            }
        } catch (err) {
            console.log("cannot redirect");
            url = `/myItems`;
        }
        this.props.history.push({ pathname: url });
    }

    getLRDialogue = () => {
        return <>
            <Dialog open={this.state.displayDialog} onClose={this.disableDisplay} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description" disableBackdropClick={true} maxWidth='lg'>

                <DialogContent>
                    <Typography className="pb-3" align="center" color="primary" variant="body1">Please fill in the following fields in order to create a copy of  <small className="teal--font">{this.props.resource.resource_name}</small></Typography>
                    <DialogContentText component={"div"}>
                        <TextField className="pb-3 wd-100"
                            //type="text"
                            required={true}
                            label={"resource name"}
                            placeholder={"X tagger for French, Y corpus"}
                            variant="outlined"
                            helperText="The official name or title of the language resource/technology"
                            value={this.state.resource_name}
                            onChange={(e) => { this.setState({ resource_name: e.target.value }) }}
                        />
                        <TextField className="pb-3 wd-100"
                            //type="text"
                            required={false}
                            label={"Version"}
                            placeholder={"1.0.0"}
                            variant="outlined"
                            helperText={
                                <span>
                                    The new version of the record that will be created.
                                    Recommended format: major_version.minor_version.patch (see semantic versioning guidelines at <a target="_blank" rel="noopener noreferrer" href="http://semver.org"> http://semver.org</a> <FileCopyIcon style={{ width: 10, height: 10 }} />)
                                </span>
                            }
                            value={this.state.version}
                            onChange={(e) => { this.setState({ version: e.target.value }) }}
                        />
                        {[TOOL_SERVICE, "ToolService"].includes(this.props.resource.resource_type) && <div className="pb-3">
                            <FormControlLabel className="upload-checkboxes p-05 wd-100"
                                control={<Checkbox
                                    checked={this.state.functional_service === true ? true : false}
                                    color="primary"
                                    inputProps={{ 'aria-label': 'functional service checkbox' }}
                                    onChange={e => { this.setState({ functional_service: e.target.checked }); }}
                                />}
                                label={
                                    <span>
                                        {messages.label_functional_service} |&nbsp;
                                        <a href={messages.dialog_functional_service_infoLink_message} target="_blank" rel="noopener noreferrer">If the metadata record is for a service to be integrated in ELG</a>
                                        <OpenInNewIcon style={{ width: 10, height: 10 }} />
                                    </span>
                                }
                            />
                        </div>}
                    </DialogContentText>
                    <DialogContentText component={"div"}>
                        {this.state.error && this.ShowErrorMessage(this.state.error)}
                    </DialogContentText>
                </DialogContent>

                <DialogActions>
                    <Button
                        color="primary"
                        onClick={() => this.disableDisplay()}
                        autoFocus
                    >
                        cancel
                    </Button>
                    <Tooltip title={this.state.resource_name ? `create a copy` : 'resource name is required '}>
                        <span>
                            <Button
                                color="primary"
                                disabled={this.state.loading || (!this.state.resource_name)}
                                onClick={
                                    () =>
                                        [TOOL_SERVICE, "ToolService"].includes(this.props.resource.resource_type) ?
                                            this.handleSubmit({ "resource_name": this.state.resource_name, "version": this.state.version, "elg_compatible_service": this.state.functional_service }) :
                                            this.handleSubmit({ "resource_name": this.state.resource_name, "version": this.state.version })
                                }
                            >
                                Create copy
                            </Button>
                        </span>
                    </Tooltip>
                </DialogActions>
            </Dialog>
        </>
    }

    get_Project_Organization_Dialogue = () => {
        return <>
            <Dialog open={this.state.displayDialog} onClose={this.disableDisplay} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description" disableBackdropClick={true} maxWidth='lg'>

                <DialogContent>
                    <Typography className="pb-3" align="center" color="primary" variant="body1">Please fill in the following fields in order to create a copy of <small className="teal--font">{this.props.resource.resource_name}</small></Typography>
                    <DialogContentText component={"div"}>
                        <TextField className="pb-3 wd-100"
                            //type="text"
                            required={true}
                            label={this.props.resource.resource_type === PROJECT ? "Project name" : "Organization name"}
                            placeholder={this.props.resource.resource_type === PROJECT ? "Project X" : "European Commission"}
                            variant="outlined"
                            helperText={this.props.resource.resource_type === PROJECT ? "The official title of the project" : "The official title of the organization"}
                            value={this.state.resource_name}
                            onChange={(e) => { this.setState({ resource_name: e.target.value }) }}
                        />
                    </DialogContentText>
                    <DialogContentText component={"div"}>
                        {this.state.error && this.ShowErrorMessage(this.state.error)}
                    </DialogContentText>
                </DialogContent>

                <DialogActions>
                    <Button
                        color="primary"
                        onClick={() => this.disableDisplay()}
                        autoFocus
                    >
                        cancel
                    </Button>
                    <Tooltip title={this.state.resource_name ? `create a copy` : `${this.props.resource.resource_type === PROJECT ? 'Project' : 'Organization'} name is a required field`}>
                        <span>
                            <Button
                                color="primary"
                                disabled={this.state.loading || !this.state.resource_name}
                                onClick={() => this.handleSubmit({ "resource_name": this.state.resource_name })}
                            >
                                Create copy
                            </Button>
                        </span>
                    </Tooltip>
                </DialogActions>
            </Dialog>
        </>
    }


    render() {
        const { resource_type } = this.props.resource;
        if ((resource_type === CORPUS || resource_type === LCR || resource_type === LD || resource_type === ML_MODEL || resource_type === GRAMMAR || resource_type === OTHER || resource_type === Uncategorized_Language_Description || resource_type === TOOL_SERVICE || resource_type === "ToolService" || resource_type === "LexicalConceptualResource" || resource_type === "LanguageDescription")) {
            return this.getLRDialogue();
        } else if (resource_type === PROJECT || resource_type === ORGANIZATION) {
            return this.get_Project_Organization_Dialogue();
        } else {
            return <></>
        }

    }
}

export default withRouter(CopyRecord)