import React from 'react';
//import { withRouter, Link } from "react-router-dom";
import axios from "axios";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
//import Typography from '@material-ui/core/Typography';
//import GetAppIcon from '@material-ui/icons/GetApp';
//import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
//import ExpandLessIcon from '@material-ui/icons/ExpandLess';
//import CountUp from 'react-countup';
//import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
//import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import { SERVER_API_CONSUMER_USAGE_EXPAND } from "../../config/constants";
//import CircularProgress from '@material-ui/core/CircularProgress';
import ProgressBar from "../../componentsAPI/CommonComponents/ProgressBar";
import Chip from '@material-ui/core/Chip';
import { PieChart, Pie, Cell } from 'recharts';
//import CustomProgressBar from "../../componentsAPI/ProjectReusableComponents/CustomProgressBar";
//import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';
//const columns = [{ key: "start_at", display_value: "Started" }, { key: "end_at", display_value: "Ended" }, { key: "bytes", display_value: "Bytes" }, { key: "status", display_value: "Status" }, { key: "call_type", display_value: "Call type" }, { key: "is_latest_usage", display_value: "Latest usage" }];
const columns = [{ key: "start_at", display_value: "Started" }, { key: "duration", display_value: "Duration (seconds)" }, { key: "bytes", display_value: "Bytes" }, { key: "status", display_value: "Status" }, { key: "mapped_call_type", display_value: "Call type" }];

const COLORS = ['#299ba3', '#7c5cd6', '#636e7b'];

class ExpandedUsageView extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = { data: null, source: null, loading: null, showDialogue: true, };
    }

    componentDidMount() {
        this.getData();
    }

    componentWillUnmount() {
        if (this.state.source) {
            this.state.source.cancel("");
        }
    }

    getData = () => {
        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        this.setState({ source: source, loading: true });
        axios.get(SERVER_API_CONSUMER_USAGE_EXPAND(this.props.resource.item_id), null, { cancelToken: source.token }).then((response) => {
            this.setState({ data: response.data, source: null, loading: false });
        }).catch((errorResponse) => {
            console.log("error while fetching expanded data");
            this.setState({ source: null, loading: false, data: null });
        });
    }

    disableDisplay = () => {
        this.setState({ showDialogue: false });
        this.props.handleExpandeView(false);
    }

    formatDate = (string2Format) => {
        return new Intl.DateTimeFormat("en-GB", {
            year: "numeric",
            month: "long",
            day: "2-digit",
            hour: "2-digit",
            minute: "2-digit",
            second: "2-digit",
        }).format(new Date(string2Format));
    }

    formatDuration = (string2Format) => {
        return new Intl.NumberFormat('en-GB').format(string2Format);
    }

    formatStatus = (statusString) => {
        return statusString === "succeeded" ?
            <Chip size="small" label={"succeeded"} className="ChipTagGreen" />
            :
            <Chip size="small" label={statusString} className="ChipTagLightPink" />
    }

    createchartdata = () => {
        const chartData = [];
        try {
            const statusesSet = new Set();
            const status2FrequencyMap = new Map();
            this.state.data.map((resource, resourceIndex) => {
                statusesSet.add(resource.status);
                if (status2FrequencyMap.has(resource.status)) {
                    let freq = status2FrequencyMap.get(resource.status);
                    status2FrequencyMap.set(resource.status, freq + 1);
                } else {
                    status2FrequencyMap.set(resource.status, 1);
                }
                return resource;
            });
            const arrayWithStatuses = [...statusesSet];
            for (let i = 0; i <= arrayWithStatuses.length; i++) {
                chartData.push({ name: arrayWithStatuses[i], value: status2FrequencyMap.get(arrayWithStatuses[i]) });
            }
            //chartData.push({name:"failed",value:34})
            return chartData.filter(item => item.name);
        } catch (err) {
            return [];
        }
    }

    renderCustomizedLabel = ({ cx, cy, midAngle, innerRadius, outerRadius, percent, index }) => {
        //https://recharts.org/en-US/examples/PieChartWithCustomizedLabel
        const RADIAN = Math.PI / 180;
        const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
        const x = cx + radius * Math.cos(-midAngle * RADIAN);
        const y = cy + radius * Math.sin(-midAngle * RADIAN);

        return (
            <text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
                {`${(percent * 100).toFixed(0)}%`}
            </text>
        );
    };

    renderPieChart = (chartData = []) => {
        try {
            if (!chartData || (chartData && chartData.length === 0)) {
                return <></>;
            }
            return <> Status <PieChart width={200} height={100}>
                <Pie
                    data={chartData}
                    cx={'50%'}
                    cy={'100%'}
                    startAngle={180}
                    endAngle={0}
                    innerRadius={30}
                    outerRadius={80}
                    fill="#9ca5b0"
                    paddingAngle={5}
                    dataKey="value"
                    nameKey="name"
                    labelLine={false}
                    label={this.renderCustomizedLabel}
                >
                    {
                        chartData.map((entry, index) => <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />)
                    }
                </Pie>
            </PieChart>
            </>
        } catch (err) {
            return <></>
        }
    }

    tableRender = () => {
        return <React.Fragment>
            <Grid container direction="row" alignItems="flex-start" spacing={2}>
                <Grid item xs={12}>
                    <TableContainer component={Paper} style={{ marginBottom: "5%" }}>
                        <Table aria-label="Batch service registration table" size="small" className="my-items-table">
                            <TableHead>
                                <TableRow >
                                    {columns.map((item, index) => <TableCell key={index}>{item.display_value}</TableCell>)}
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    this.state.data.sort((item1, item2) => {
                                        if (item1 && item2) {
                                            /*
                                            if (item1.is_latest_usage) {
                                                return -1;
                                            } else if (item2.is_latest_usage) {
                                                return 1;
                                            }
                                            return 0;
                                            */
                                            return new Date(item2.start_at) - new Date(item1.start_at);
                                        }
                                        return 0;
                                    }).map((resource, resourceIndex) => {
                                        return <TableRow key={resourceIndex + JSON.stringify(resource)}>
                                            {columns.map((item, index) => {
                                                if (item.key === "start_at" || item.key === "end_at") {
                                                    return <TableCell key={resourceIndex + index + 1}>{this.formatDate(resource[item.key])}</TableCell>
                                                } else if (item.key === "is_latest_usage") {
                                                    return <TableCell key={resourceIndex + index + 1}>{resource[item.key] ? "Yes" : "No"}</TableCell>
                                                } else if (item.key === "status") {
                                                    return <TableCell key={resourceIndex + index + 1}>{this.formatStatus(resource[item.key])}</TableCell>
                                                } else if (item.key === "duration") {
                                                    return <TableCell key={resourceIndex + index + 1}>{this.formatDuration(resource[item.key])}</TableCell>
                                                }
                                                return <TableCell key={resourceIndex + index + 1}>{resource[item.key]}</TableCell>
                                            })}
                                        </TableRow>
                                    }
                                    )
                                }
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
            </Grid>
        </React.Fragment >
    }

    render() {
        const hasData = this.state.data;
        return <>
            <Dialog open={this.state.showDialogue} onClose={this.disableDisplay} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description" maxWidth='xl' fullWidth style={{insetBlockStart: "130px"}}>
                <DialogTitle align="center">{this.props.resource.resource_name}</DialogTitle>
                {hasData ? <DialogContent>
                    <DialogContentText component={"div"}>
                        {this.tableRender()}
                        {true && <>{this.renderPieChart(this.createchartdata())}</>}
                    </DialogContentText>
                </DialogContent> : <>
                    {this.state.loading ? <ProgressBar /> : "No available information"}
                </>
                }
                <DialogActions>
                    <Button
                        color="primary"
                        onClick={this.disableDisplay}
                        autoFocus
                    >
                        Cancel
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    }
}

export default ExpandedUsageView;