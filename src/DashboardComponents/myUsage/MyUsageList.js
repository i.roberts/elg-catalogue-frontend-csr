import React from "react";
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { withRouter, Link } from "react-router-dom";
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import {
    //  TOOL_SERVICE, CORPUS, LD, ML_MODEL, GRAMMAR, PROJECT, ORGANIZATION, LCR,
    //AUTHENTICATED_KEYCLOAK_USER_USERNAME, 
    getUrlToLandingPageFromCatalogue,
    //Uncategorized_Language_Description, OTHER,
    //HIDE_ENTITIES_FROM_MY_ITEMS
    //    IS_CONTENT_MANAGER
} from "../../config/constants";
import Chip from '@material-ui/core/Chip';
import CountUp from 'react-countup';
import ExpandedUsageView from "./ExpandedUsageView";

class MyUsageList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            resourceData: props.resource, keycloak: props.keycloak, anchorEl: false, loading: false, showExpandView: false
        };
    }

    handleClick = (event) => {
        this.setState({ anchorEl: event.currentTarget });
    }

    handleClose = () => {
        this.setState({ anchorEl: false });
    }

    navigateToResourceDetail(resource) {
        if (resource.status === "draft") {
            return;
        }
        const url2LandingPage = getUrlToLandingPageFromCatalogue(resource);
        if (url2LandingPage) {
            return url2LandingPage;
        }
    }

    handleCheck = (e) => {
        if (e.target.checked) {
            this.props.addCheckedResource2List(this.state.resourceData);
        } else {
            this.props.removeResourceFromCheckedList(this.state.resourceData);
        }
    }

    handleExpand = (value) => {
        this.setState({ showExpandView: value });
    }


    render() {
        const resource = this.state.resourceData;
        if (!resource.hasOwnProperty("item_id")) {
            Object.assign(resource, { "item_id": JSON.parse(JSON.stringify(resource["id"])) });//put id to item_id property, this is used for expansion
        }
        delete Object.assign(resource, { "id": resource["record_id"] });//put record_id to id property
        const resourceLandingPage = this.navigateToResourceDetail(resource);

        if (this.state.showExpandView) {
            return <ExpandedUsageView resource={resource} handleExpandeView={this.handleExpand} navigateToResourceDetail={this.navigateToResourceDetail} />
        }

        return (
            <React.Fragment>
                <Paper className="DashboardListItem">
                    <Grid item xs={12} container spacing={1} justifyContent="space-between" alignItems="center" className="ResourceListItem--inner">
                        <Grid item sm={8}>
                            {resourceLandingPage && <Typography variant="h5" className="ResourceListTitle pt-05" ><Link to={resourceLandingPage}>{resource.resource_name}</Link></Typography>}
                            <Grid item container sm={12}>
                                <Grid item sm={8}>
                                    <Grid item container spacing={1} className="pb-05 pt-05">
                                    {resource.version && resource.version !== "undefined" && <Grid item><Typography variant="caption" className="grey--font">  Version: {resource.version}</Typography></Grid> }
                                        <Grid item>{resource.is_active_version === true && <div><span className="badge bg-teal-light mr-05">Active version</span></div>}</Grid>
                                        <Grid item>{resource.is_latest_version === true && <div><span className="badge bg-orange-light mr-05">Latest version</span></div>}</Grid>
                                    </Grid>

                                    {resource.execution_start ? <Grid item container direction="column" justifyContent="flex-start" alignItems="flex-start" >
                                        {resource.execution_start && <Grid item><Typography variant="caption">Execution started at:&nbsp;
                                            {new Intl.DateTimeFormat("en-GB", {
                                                year: "numeric",
                                                month: "long",
                                                day: "2-digit"
                                            }).format(new Date(resource.execution_start))} </Typography></Grid>
                                        }
                                    </Grid> : void 0}
                                    <Grid item sm={12} container direction="row" justifyContent="flex-start" alignItems="flex-start">
                                        {resource.duration && <Typography variant="caption" className="pt-05" >Duration: {resource.duration} (secs)</Typography>}
                                    </Grid>
                                    <Grid item sm={12} container direction="row" justifyContent="flex-start" alignItems="flex-start">
                                        {resource.bytes && <Typography variant="caption" className="pt-05" >Bytes: {resource.bytes}</Typography>}
                                    </Grid>
                                    <Grid item sm={12} container direction="row" justifyContent="flex-start" alignItems="flex-start">
                                        {resource.number_of_usages && <Typography variant="caption" className="pt-05" >Number of times used: <CountUp end={resource.number_of_usages} /></Typography>}
                                    </Grid>
                                    <Grid item container direction="row" justifyContent="flex-start" alignItems="flex-start" sm={12} >
                                        {resource.status === "succeeded" ?
                                            <Chip size="small" label={"succeeded"} className="ChipTagGreenOutlined" />
                                            :
                                            <Chip size="small" label={resource.status} className="ChipTagLightPinkOutlined" />}
                                    </Grid>
                                </Grid>

                            </Grid>

                        </Grid>
                        <Grid item sm={4}>
                        <Grid item sm={12} style={{textAlign: "center"}} >
                            <Button
                                onClick={() => this.handleExpand(true)}
                                classes={{ root: 'inner-link-outlined--teal' }}
                                aria-controls="simple-menu"
                                aria-haspopup="true"
                                disabled={!resource.other_usages || (resource.other_usages && resource.other_usages.length <= 0)}
                                endIcon={<MoreHorizIcon />}>Show more</Button>
                        </Grid>

                        

                    </Grid>
                    </Grid>


                </Paper >
            </React.Fragment >
        );

    }
}
export default withRouter(MyUsageList);
