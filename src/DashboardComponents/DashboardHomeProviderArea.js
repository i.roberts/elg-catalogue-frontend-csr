
import React from "react";
import { Redirect, withRouter } from "react-router-dom";
import axios from "axios";
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Grid from '@material-ui/core/Grid';
import Avatar from '@material-ui/core/Avatar';
import { ReactComponent as UploadIcon } from "./../assets/elg-icons/editor/upload-thick-bottom.svg";
import { ReactComponent as WorkflowIcon } from "./../assets/elg-icons/editor/virtual-box.svg";
import { ReactComponent as ResourcesImage } from "./../assets/elg-icons/editor/common-file-stack.svg";
import { ReactComponent as ArrowImage } from "./../assets/elg-icons/editor/navigation-arrows-right-1.svg";
import Button from '@material-ui/core/Button';

import { //HEADER_BASE_URL, 
  searchManagmentKeywordCombineWithFacetUrl, AUTHENTICATED_KEYCLOAK_USER_ROLES, EDITOR_PERMITTED_ROLES
} from "./../config/constants";

class DashboardHomeProviderArea extends React.Component {
  constructor(props) {
    super(props);
    this.state = { keycloak: props.keycloak, data: "" };
  }

  componentDidMount() {
    this.getCountOfItems();
  }

  getCountOfItems() {
    if (!this.state.keycloak || !this.state.keycloak.authenticated) {
      return;
    }
    let authorized = false;
    EDITOR_PERMITTED_ROLES.forEach(item => {
      if (AUTHENTICATED_KEYCLOAK_USER_ROLES(this.state.keycloak).includes(item)) {
        authorized = true;
      }
    });
    if (!authorized) {
      return;
    }
    axios.get(searchManagmentKeywordCombineWithFacetUrl("", ""))
      .then(res => {
        this.setState({ data: res.data });
      }).catch(err => console.log(err));
  }

  render() {
    if (!this.state.keycloak || !this.state.keycloak.authenticated) {
      return <Redirect to="/" />
    }

    return (<>

      {/* a new grid row*/}

      <Grid item container direction="row" justifyContent="flex-start" alignItems="stretch" spacing={2} sm={12} xs={12}>

        <Grid item sm={3} xs={12} style={{ display: 'flex' }} >

          <Card style={{ width: '100%', display: 'flex', justifyContent: 'space-between', flexDirection: 'column' }} className="card-url" onClick={(event, newValue) => { this.props.history.push("/myItems") }}>

            <CardContent>
              <Grid container direction="row" justifyContent="space-around" alignItems="flex-start" spacing={1}>
                <Grid item sm={12} xs={12}>
                  <Avatar className="dashboard-page-avatar teal-cirle" style={{ float: "left" }}><ResourcesImage className="white--font xsmall-icon" /> </Avatar>
                </Grid>
                <Grid item sm={12} xs={12}>
                  <Typography className="bold grey--dark--font bigger-text">Total items</Typography>
                  <Typography variant="body1" className="pt-1">Number of items you have created.</Typography>
                  <Typography variant="h2" className="pt-1" > {this.state.data.count} </Typography>

                </Grid>
              </Grid>

            </CardContent>
            <CardActions style={{ justifyContent: "flex-end" }}> <Button classes={{ root: 'dashboard-page--CardAction' }} onClick={(event, newValue) => { this.props.history.push("/myItems") }}>  <ArrowImage className="teal--font xsmall-icon" /> </Button> </CardActions>

          </Card>


        </Grid>

        <Grid item sm={3} xs={12} style={{ display: 'flex' }} >

          <Card style={{ width: '100%', display: 'flex', justifyContent: 'space-between', flexDirection: 'column' }} onClick={(event, newValue) => { this.props.history.push("/createResource") }} className="card-url">

            <CardContent>
              <Grid container direction="row" justifyContent="space-around" alignItems="flex-start" spacing={1}>
                <Grid item sm={12} xs={12}>
                  <Avatar className="dashboard-page-avatar purple-cirle" ><ResourcesImage className="white--font xsmall-icon" /> </Avatar>
                </Grid>
                <Grid item sm={12} xs={12}>
                  <Typography className="bold grey--dark--font bigger-text">Interactive editor</Typography>
                  <Typography variant="body1" className="pt-1">Use the interactive editor to create new items.</Typography>

                </Grid>
              </Grid>
            </CardContent>
            <CardActions style={{ justifyContent: "flex-end" }}> <Button classes={{ root: 'dashboard-page--CardAction' }} onClick={(event, newValue) => { this.props.history.push("/createResource") }}>  <ArrowImage className="purple--font xsmall-icon" /> </Button> </CardActions>

          </Card>

        </Grid>

        <Grid item sm={3} xs={12} style={{ display: 'flex' }}>

          <Card style={{ width: '100%', display: 'flex', justifyContent: 'space-between', flexDirection: 'column' }} className="card-url" onClick={(event, newValue) => { this.props.history.push("/upload/upload-single-file/") }}>

            <CardContent>
              <Grid container direction="row" justifyContent="space-around" alignItems="flex-start" spacing={1}>
                <Grid item sm={12} xs={12}>
                  <Avatar className="dashboard-page-avatar teal-cirle" ><UploadIcon className="white--font xsmall-icon" /> </Avatar>
                </Grid>
                <Grid item sm={12} xs={12}>
                  <Typography className="bold grey--dark--font bigger-text">Upload items</Typography>
                  <Typography variant="body1" className="pt-1" > Upload single or multiple metadata records (in XML format) and content files. </Typography>

                </Grid>

              </Grid>

            </CardContent>
            <CardActions style={{ justifyContent: "flex-end" }}> <Button classes={{ root: 'dashboard-page--CardAction' }} onClick={(event, newValue) => { this.props.history.push("/upload/upload-single-file/") }}>  <ArrowImage className="teal--font xsmall-icon" /> </Button> </CardActions>

          </Card>

        </Grid>

        <Grid item sm={3} xs={12} style={{ display: 'flex' }} >

          <Card style={{ width: '100%', display: 'flex', justifyContent: 'space-between', flexDirection: 'column' }} className="card-url" onClick={(event, newValue) => { this.props.history.push("/upload/validate-xml-files/") }}>

            <CardContent>

              <Grid container direction="row" justifyContent="space-around" alignItems="flex-start" spacing={1}>
                <Grid item sm={12} xs={12}>
                  <Avatar className="dashboard-page-avatar purple-cirle" ><WorkflowIcon className="white--font xsmall-icon" /> </Avatar>
                </Grid>
                <Grid item sm={12} xs={12}>
                  <Typography className="bold grey--dark--font bigger-text">Validate your metadata records</Typography>
                  <Typography variant="body1" className="pt-1" > Validate your metadata records (in XML format) before uploading.  </Typography>

                </Grid>

              </Grid>

            </CardContent>
            <CardActions style={{ justifyContent: "flex-end" }}> <Button classes={{ root: 'dashboard-page--CardAction' }} onClick={(event, newValue) => { this.props.history.push("/upload/validate-xml-files/") }}>  <ArrowImage className="purple--font xsmall-icon" /> </Button> </CardActions>



          </Card>

        </Grid>

      </Grid>


    </>
    );
  }
}

export default withRouter(DashboardHomeProviderArea);
