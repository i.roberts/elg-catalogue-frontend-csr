import React from "react";
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import messages from "../../../config/messages";
import PerformMetadataTechnicalValidation from "./PerformMetadaTechnicalValidation";
import { TOOL_SERVICE } from "../../../config/constants";

export default class ValidatorBulkActions extends React.Component {

    constructor(props) {
        super(props);
        this.state = { selectedAction: "" }
    }

    clearSelect = (value) => {
        this.setState({ selectedAction: value });
    }

    getRecordsOfStatus = (array, status, functional_service) => {
        let status_items = this.props.checkedList?.filter(item => item.status === status) || []
        if (functional_service && status_items && status_items.length > 0) {
            status_items = status_items.filter(item => item.elg_compatible_service && (item.resource_type === TOOL_SERVICE || item.resource_type === "ToolService") && item.service_status === "Completed" && item.metadata_valid !== "yes" && item.technically_valid !== "yes")
        }
        if (status_items.length) {
            switch (status) {
                case "ingested":
                    array.includes(messages.validator_action_technical) ? void 0 : array.push(messages.validator_action_technical);
                    break;
                default: break;
            }
        }
        return status_items;
    }

    render() {
        const choices = []
        const ingested_items = this.getRecordsOfStatus(choices, "ingested", true);

        switch (this.state.selectedAction) {
            case messages.validator_action_technical:
                return <PerformMetadataTechnicalValidation {...this.props} ingested_items={ingested_items} updateRecords={this.props.updateRecords} clearSelect={this.clearSelect} />
            default: break;
        }

        if (choices.length === 0) {
            return <></>;
        }
        return <>
            <FormControl variant="outlined" className="wd-100">
                <InputLabel htmlFor="outlined-bulk-action">Action</InputLabel>
                <Select
                    variant="outlined"
                    native
                    value={this.state.selectedAction}
                    onChange={(e) => { this.setState({ selectedAction: e.target.value }); }}
                    label="Action"
                    inputProps={{
                        name: 'validator-bulk-action',
                        id: 'validator-bulk-action',
                    }}
                >
                    <option aria-label="None" value="" />
                    {choices.map((item, index) => <option key={index} value={item}>{item}</option>)}
                </Select>
                <FormHelperText>Select an action to perform on your selected items</FormHelperText>
            </FormControl>

        </>
    }
}