import React from "react";
import Typography from '@material-ui/core/Typography';
import Chip from '@material-ui/core/Chip';

export default class DistributionVideoFeature extends React.Component {
    render() {
        const { distribution, metadataLanguage } = this.props;
        const { distribution_video_feature = [] } = distribution || "";
        if (!distribution_video_feature) {
            return <></>;
        }
        const distribution_video_feature_label = distribution_video_feature ? distribution_video_feature.field_label[metadataLanguage] || distribution_video_feature.field_label["en"] : "";
        const distribution_video_feature_Array = distribution_video_feature ? distribution_video_feature.field_value.map((video, videoIndex) => {

            let data_format_label = video.data_format ? (video.data_format.field_label[metadataLanguage] || video.data_format.field_label["en"]) : "";
            let data_formatArray = (video.data_format && video.data_format.field_value.map(format => format.label ? (format.label[metadataLanguage] || format.label[Object.keys(format.label)[0]]) : format.value)) || [];
            let mimetype_label = video.mimetype ? (video.mimetype.field_label[metadataLanguage] || video.mimetype.field_label["en"]) : "";
            let mimetypeArray = (video.mimetype && video.mimetype.field_value.map(mimetype => mimetype)) || [];

            const size_label = video.size ? (video.size.field_label[metadataLanguage] || video.size.field_label["en"]) : "";
            const sizeArray = (video.size && video.size.field_value.map((sizeItem, sizeIndex) => {
                const amount = sizeItem.amount ? sizeItem.amount.field_value : "";
                let size_unit = sizeItem.size_unit ? (sizeItem.size_unit.field_value.label ? (sizeItem.size_unit.field_value.label[metadataLanguage] || sizeItem.size_unit.field_value.label[Object.keys(sizeItem.size_unit.field_value.label)[0]]) : sizeItem.size_unit.field_value.value) : "";
                return this.props.language_domain_textGenre_audioGenre_speechGenre_videoGenre_imageGenre(sizeItem, sizeIndex, metadataLanguage, amount, size_unit);
            })) || [];
            const video_format_label = video.video_format ? (video.video_format.field_label[metadataLanguage] || video.video_format.field_label["en"]) : "";
            const video_format_Array = (video.video_format && video.video_format.field_value.map((format, formatIndex) => {
                const data_format = format.data_format ? (format.data_format.field_value.label ? (format.data_format.field_value.label[metadataLanguage] || format.data_format.field_value.label[Object.keys(format.data_format.field_value.label)[0]]) : format.data_format.field_value.value) : "";
                const data_format_label = format.data_format ? (format.data_format.field_label[metadataLanguage] || format.data_format.field_label["en"]) : "";

                const colour_space_label = format.colour_space ? (format.colour_space.field_label[metadataLanguage] || format.colour_space.field_label["en"]) : "";
                //const colour_spaceArray = format.colour_space ? format.colour_space.field_value.map((color_space, color_space_index) => <a key={color_space_index} target="_blank" rel="noopener noreferrer" href={color_space.value || ""}>{color_space.label[metadataLanguage] || color_space.label[Object.keys(color_space.label)[0]]}</a>) : [];
                const colour_spaceArray = format.colour_space ? format.colour_space.field_value.map((color_space, color_space_index) => color_space.label[metadataLanguage] || color_space.label[Object.keys(color_space.label)[0]]) : [];

                const colour_depth = format.colour_depth ? (format.colour_depth.field_label[metadataLanguage] || format.colour_depth.field_label["en"]) : "";
                const colour_depthArray = format.colour_depth ? format.colour_depth.field_value.map(colour_depth => colour_depth) : [];

                const frame_rate_label = format.frame_rate ? (format.frame_rate.field_label[metadataLanguage] || format.frame_rate.field_label["en"]) : "";
                const frame_rate = format.frame_rate ? format.frame_rate.field_value : null;

                const resolution = format.resolution ? (format.resolution.field_label[metadataLanguage] || format.resolution.field_label["en"]) : "";
                const resolutionArray = format.resolution ? format.resolution.field_value.map((resolution, resolutionIndex) => {
                    const size_width = resolution.size_width ? resolution.size_width.field_value : "";
                    const size_height = resolution.size_height ? resolution.size_height.field_value : "";
                    const resolution_standard = resolution.resolution_standard ? resolution.resolution_standard.label[metadataLanguage] || resolution.resolution_standard.label[Object.keys(resolution.resolution_standard.label)[0]] : ""
                    return <div key={resolutionIndex} className="padding5"><span className="info_value">{size_width} {size_width || size_height ? "x" : ""} {size_height} {size_width || size_height ? "-" : ""} {resolution_standard}</span></div>
                }) : [];

                const visual_modelling = format.visual_modelling ? format.visual_modelling.label[metadataLanguage] || format.visual_modelling.label[Object.keys(format.visual_modelling.label)[0]] : "";
                const visual_modelling_label = format.visual_modelling ? (format.visual_modelling.field_label[metadataLanguage] || format.visual_modelling.field_label["en"]) : "";

                const fidelity = format.fidelity ? format.fidelity.field_value : null;
                const fidelity_label = format.fidelity ? (format.fidelity.field_label[metadataLanguage] || format.fidelity.field_label["en"]) : "";

                const compressed = format.compressed ? format.compressed.field_value : null;
                const compressed_label = format.compressed ? (format.compressed.field_label[metadataLanguage] || format.compressed.field_label["en"]) : "";

                const compression_name = format.compression_name ? format.compression_name.label[metadataLanguage] || format.compression_name.label[Object.keys(format.compression_name.label)[0]] : "";
                const compression_name_label = format.compression_name ? (format.compression_name.field_label[metadataLanguage] || format.compression_name.field_label["en"]) : "";

                const compression_loss = format.compression_loss ? format.compression_loss.field_value : null;
                const compression_loss_label = format.compression_loss ? (format.compression_loss.field_label[metadataLanguage] || format.compression_loss.field_label["en"]) : "";

                return <div key={formatIndex}>
                    {data_format && <div className="padding5"><Typography className="bold-p--id">{data_format_label}</Typography><Chip className="ChipTagTeal" label={data_format} /></div>}
                    {colour_spaceArray && colour_spaceArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{colour_space_label}</Typography>{colour_spaceArray.map((color_space, color_space_index) => <span key={color_space_index}>{color_space}&nbsp;</span>)}</div>}
                    {colour_depthArray && colour_depthArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{colour_depth}</Typography>{colour_depthArray.map((colour_depth, colour_depth_index) => <span key={colour_depth_index}>{colour_depth},&nbsp;</span>)}</div>}
                    {frame_rate !== null && <div className="padding5"><Typography className="bold-p--id">{frame_rate_label}</Typography><span className="info_value">{frame_rate}</span></div>}
                    {resolutionArray && resolutionArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{resolution}</Typography>{resolutionArray.map((resolution, resolutionIndex) => <span key={resolutionIndex}>{resolution}</span>)}</div>}
                    {visual_modelling && <div className="padding5"><Typography className="bold-p--id">{visual_modelling_label}</Typography><span className="info_value">{visual_modelling}</span></div>}
                    {fidelity !== null && <div className="padding5"><Typography className="bold-p--id">{fidelity_label}</Typography><span className="info_value">{JSON.stringify(fidelity)}</span></div>}
                    {compressed !== null && <div className="padding5"><Typography className="bold-p--id">{compressed_label}</Typography><span className="info_value">{JSON.stringify(compressed)}</span></div>}
                    {compression_name && <div className="padding5"><Typography className="bold-p--id">{compression_name_label}</Typography><span className="info_value">{compression_name}</span></div>}
                    {compression_loss !== null && <div className="padding5"><Typography className="bold-p--id">{compression_loss_label}</Typography><span className="info_value">{JSON.stringify(compression_loss)}</span></div>}
                </div >
            })) || [];
            return <div key={videoIndex}>
                {sizeArray && sizeArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{size_label}</Typography>{sizeArray.map((size, sizeIndex) => <span key={sizeIndex}>{size}</span>)}</div>}
                {data_formatArray && data_formatArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{data_format_label} </Typography>{data_formatArray.map((format, formatIndex) => <Chip key={formatIndex} size="small" label={format} className="ChipTagTeal" />)}</div>}
                {mimetypeArray && mimetypeArray.length > 0 && <div className="padding5"><Typography className="bold-p--id">{mimetype_label} </Typography>{mimetypeArray.map((mimetype, mimetypeIndex) => <span key={mimetypeIndex} className="info-url" >{mimetype}</span>)}</div>}
                {video_format_Array && video_format_Array.length > 0 && <div className="padding5"><Typography className="bold-p--id">{video_format_label}</Typography>{video_format_Array.map((videoFormat, videoFormatIndex) => <span key={videoFormatIndex}>{videoFormat}</span>)}</div>}
            </div >
        }) : [];
        return <div>
            {distribution_video_feature_Array && distribution_video_feature_Array.length > 0 && <div className="padding5"><Typography className="section-headings">{distribution_video_feature_label} </Typography><span className="info_value">{distribution_video_feature_Array.map((item, index) => <div key={index}>{item}</div>)}</span></div>}
        </div>
    }
}