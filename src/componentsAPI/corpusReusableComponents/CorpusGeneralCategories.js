import React from "react";
import Typography from '@material-ui/core/Typography';



export default class CorpusGeneralCategories extends React.Component {
    componentDidMount() {
        const { data, showArea } = this.props;
        const { time_coverage, geographic_coverage, register, spatial, temporal } = data.described_entity.field_value.lr_subclass.field_value || [];
        
        if ((time_coverage && time_coverage.field_value.length > 0) || (geographic_coverage&& geographic_coverage.field_value.length > 0 ) || (register&& register.field_value.length > 0) || (spatial&& spatial.field_value.length > 0) || (temporal&& temporal.field_value.length > 0)) 
        {
            showArea ? void 0 : this.props.showAreaFunction(true);
        }
    }
    render() {
        const { data, metadataLanguage, title} = this.props;
        const { time_coverage, geographic_coverage, register, spatial, temporal } = data.described_entity.field_value.lr_subclass.field_value || [];
        const showTitle = ((time_coverage && time_coverage.field_value.length > 0) || (geographic_coverage && geographic_coverage.field_value.length > 0) || (register && register.field_value.length > 0));

        if (!data) {
            return <div></div>
        }

        return <>
                <div>
                   { showTitle && <Typography variant="h3" className="title-links">{title}</Typography>} 

                    {time_coverage && time_coverage.field_value.length > 0 && <Typography className="bold-p--id">{time_coverage.field_label[metadataLanguage] || time_coverage.field_label["en"]}</Typography>}
                    {time_coverage && time_coverage.field_value.length > 0 && <div className="padding5"> {time_coverage.field_value.map((item, index) => {
                        let cov = item[metadataLanguage] || item[Object.keys(item)[0]];
                        return <div key={index}>
                            <span className="info_value">{cov}</span>
                        </div>
                    })} </div> }

                    {geographic_coverage && geographic_coverage.field_value.length > 0 && <Typography className="bold-p--id">{geographic_coverage.field_label[metadataLanguage] || geographic_coverage.field_label["en"]}</Typography>}
                    {geographic_coverage && geographic_coverage.field_value.length > 0 && <div className="padding5"> { geographic_coverage.field_value.map((item, index) => {
                        let cov = item[metadataLanguage] || item[Object.keys(item)[0]];
                        return <div  key={index}>
                            <span className="info_value">{cov}</span>
                        </div>
                    })} </div> }

                    {register && register.field_value.length > 0 && <Typography className="bold-p--id">{register.field_label[metadataLanguage] || register.field_label["en"]}</Typography>}
                    {register &&  <div className="padding5"> {   register.field_value.map((item, index) => {
                        let reg = item[metadataLanguage] || item[Object.keys(item)[0]];
                        return <div key={index}>
                            <span className="info_value">{reg}</span>
                        </div>
                    })}  </div> } 

                    {spatial && spatial.field_value.length > 0 && <Typography className="bold-p--id">{spatial.field_label[metadataLanguage] || spatial.field_label["en"]}</Typography>}
                    {spatial &&  <div className="padding5"> {   spatial.field_value.map((item, index) => {                         
                        return <div key={index}>
                            <span className="info_value">{item}</span>
                        </div>
                    })}  </div> }
                    {temporal && temporal.field_value.length > 0 && <Typography className="bold-p--id">{temporal.field_label[metadataLanguage] || spatial.field_label["en"]}</Typography>}
                    {temporal && temporal.field_value.length > 0 && <div className="padding5"> { temporal.field_value.map((item, index) => {                        
                        let start_date = item.start_date ? item.start_date.field_value : "";
                        let end_date = item.end_date ? item.end_date.field_value : "";
                        return <div  key={index}>
                            {(start_date || end_date) && <div className="padding5"><span className="info_value">{start_date && Intl.DateTimeFormat("en-GB", {
                                    year: "numeric",
                                    month: "long",
                                    day: "2-digit"
                                }).format(new Date(start_date))} - {end_date && Intl.DateTimeFormat("en-GB", {
                                    year: "numeric",
                                    month: "long",
                                    day: "2-digit"
                                }).format(new Date(end_date))}</span></div>}
                        </div>
                    })} </div> }



                    
                    </div> 
        </>
    }
}