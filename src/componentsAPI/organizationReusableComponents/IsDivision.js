import React from "react";
import Typography from '@material-ui/core/Typography';
import commonParser from "./../../parsers/CommonParser";
import WebsitesWithIcon from '../CommonComponents/WebsitesWithIcon';
import GeneralIdentifier from '../CommonComponents/GeneralIdentifier';
import ResourceIsDivision from '../CommonComponents/ResourceIsDivision';
import Chip from '@material-ui/core/Chip';

import { Link } from "react-router-dom";
import { ReactComponent as NavIcon } from "./../../assets/elg-icons/navigation-right.svg";

class IsDivision extends React.Component {

    render() {
        const {  data, metadataLanguage } = this.props;
        if (!data) {
            return <div></div>
        }  
        let division_category_label = data.described_entity.field_value.division_category ? data.described_entity.field_value.division_category.field_label[metadataLanguage] || data.described_entity.field_value.division_category.field_label["en"] : "";
        let division_category_value = data.described_entity.field_value.division_category ? data.described_entity.field_value.division_category.label[metadataLanguage] || data.described_entity.field_value.division_category.label["en"] : "";
     
        let is_division_of_label = (data.described_entity.field_value.is_division_of && data.described_entity.field_value.is_division_of.field_value.length > 0) ? 
            data.described_entity.field_value.is_division_of.field_label[metadataLanguage] || data.described_entity.field_value.is_division_of.field_label["en"] : "" ;
                
        let is_division_ofArray = data.described_entity.field_value.is_division_of && data.described_entity.field_value.is_division_of.field_value.length > 0 ?  data.described_entity.field_value.is_division_of.field_value.map((item, index) => {
            let organization_name = item.organization_name ? item.organization_name.field_value[metadataLanguage] || item.organization_name.field_value[Object.keys(item.organization_name.field_value)[0]]:"";
            let websiteArray = (item.website && item.website.field_value.length > 0) ? item.website.field_value : [];
             
            let full_metadata_record = commonParser.getFullMetadata(item.full_metadata_record);
            return <div key={index} className="padding5">
                <Typography className="bold-p--id">{is_division_of_label}</Typography>
               {full_metadata_record ?
                        <div className="padding5 internal_url">
                            <span><NavIcon className="xsmall-icon mr-05" /></span>
                            <Link to={full_metadata_record.internalELGUrl}>
                                <span className="info_value">{organization_name} </span>
                            </Link>
                            <GeneralIdentifier data={item} identifier_name={"organization_identifier"} identifier_scheme={"organization_identifier_scheme"} metadataLanguage={metadataLanguage} />                            
                        </div> : <div>
                        <span className="info_value">{organization_name}</span>
                        <GeneralIdentifier data={item} identifier_name={item.organization_identifier} identifier_scheme={item.organization_identifier_scheme} metadataLanguage={metadataLanguage} />                         
                        </div>
                }    
                {websiteArray && websiteArray.length > 0 &&  <WebsitesWithIcon websiteArray={websiteArray} />} 
                {item.is_division_of && item.is_division_of.field_value.length>0 && <ResourceIsDivision data={item.is_division_of} metadataLanguage={metadataLanguage} /> }
            </div>
        }) : [];


 
        
        
        return (
            <>
                {(division_category_value || is_division_ofArray.length > 0) && <div className="border_top pt1"></div>}

                {division_category_value && <div className="padding5"><Typography className="bold-p--id">{division_category_label}</Typography> <Chip size="small" label={division_category_value} className="ChipTagGrey" /> </div>}
                {is_division_ofArray.length > 0 && <div className="padding5"><ul className="info_value">{is_division_ofArray.map((item, index) => <li key={index}>{item} </li>)}</ul></div>}

            </>
        );
    }

}

export default IsDivision;