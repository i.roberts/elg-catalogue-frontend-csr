import React from "react";

import Typography from '@material-ui/core/Typography';

class AddressSet extends React.Component {

    render() {
        const { addressArray,metadataLanguage,AddressSetLabel } = this.props;
        return (
            <React.Fragment>
                {addressArray && addressArray.length>0 && 
                <div className="padding15">                
                 { addressArray.map((addressitem, index) => {
                     let street = addressitem.address? (addressitem.address.field_value[metadataLanguage] || addressitem.address.field_value[Object.keys(addressitem.address.field_value)[0]]): "";
                     let region = addressitem.region ? (addressitem.region.field_value[metadataLanguage] || addressitem.region.field_value[Object.keys(addressitem.region.field_value)[0]]): ""; 
                     let city = addressitem.city ? (addressitem.city.field_value[metadataLanguage] || addressitem.city.field_value[Object.keys(addressitem.city.field_value)[0]]): ""; 
                     let zipcode =  addressitem.zip_code? (addressitem.zip_code.field_value):"";
                     let country = addressitem.country ? (addressitem.country.label[metadataLanguage] || addressitem.country.label[Object.keys(addressitem.country.label)[0]]): ""; 
                     return <div key={index}>
                     <Typography variant="h3" className="title-links">{AddressSetLabel}</Typography>    
                     <div className="info_value">{street}</div>
                     <div className="info_value">{region}</div>
                     <div className="info_value">{city}</div>
                     <div className="info_value">{zipcode}</div>
                     <div className="info_value">{country}</div>
                     </div> 
                 })}
                 </div>
            
            }   

            </React.Fragment>
        );
    }

}

export default AddressSet;