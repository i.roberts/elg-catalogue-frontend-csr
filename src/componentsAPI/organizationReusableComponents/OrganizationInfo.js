import React from "react";
import Typography from '@material-ui/core/Typography';
import SocialMediaComponent from './SocialMediaComponent';
import WebsitesWithIcon from '../CommonComponents/WebsitesWithIcon';
import EmailsWithIcon from '../CommonComponents/EmailsWithIcon';
import GeneralIdentifier from '../CommonComponents/GeneralIdentifier';

class OrganizationInfo extends React.Component {

    render() {
        const { OrganizationEmailsLabel, StartupLabel, OrganizationLegalStatusLabel, CountryOfRegistrationLabel,
            altnameArray, websiteArray, emailArray, OrganizationLegalStatus, Startup, CountryOfRegistration, SocialMedia, SocialMediaLabel ,
            identifier_name, identifier_scheme,described_entity,metadataLanguage} = this.props;
        //const{OrganizationWebsitesLabel}=this.props;


        return (
            <React.Fragment>
                <Typography variant="h3" className="padding15">Organization information</Typography>

                {altnameArray && altnameArray.length > 0 &&
                    <div className="padding15">
                        {altnameArray.map((altname, altnameIndex) =>
                            <span key={altnameIndex} className="info_value">{altname} </span>
                        )}
                    </div>
                }

                {websiteArray && websiteArray.length > 0 &&
                    <div className="padding15">
                        { /*websiteArray.length > 1 ? <Typography className="bold-p--id">{OrganizationWebsitesLabel} </Typography> : <Typography className="bold-p--id">Website </Typography>*/}
                        <WebsitesWithIcon websiteArray={websiteArray} />
                    </div>
                }

                {emailArray && emailArray.length > 0 &&
                    <div className="padding15">
                        {emailArray.length > 1 ? <Typography className="bold-p--id">{OrganizationEmailsLabel} </Typography> : <Typography className="bold-p--id">Email </Typography>}
                        <EmailsWithIcon emailArray={emailArray} />
                    </div>

                }
                {OrganizationLegalStatus &&
                    <div className="padding15">
                        <Typography className="bold-p--id">{OrganizationLegalStatusLabel}</Typography>
                        <span className="info_value" >    {OrganizationLegalStatus}</span>
                    </div>
                }

                {    
                    <GeneralIdentifier data={described_entity} identifier_name={identifier_name} identifier_scheme={identifier_scheme} metadataLanguage={metadataLanguage} />
                     
                }


                {Startup &&
                    <div className="padding15">
                        <Typography className="bold-p--id">{StartupLabel}</Typography>
                        <span className="info_value" >  {` ${Startup}`} </span>
                    </div>
                }

                {CountryOfRegistration && CountryOfRegistration.length > 0 &&
                    <div className="padding15">
                        <Typography className="bold-p--id">{CountryOfRegistrationLabel}</Typography>
                        {CountryOfRegistration.map((country, countryIndex) =>
                            <span key={countryIndex} className="info_value">{country} </span>
                        )}
                    </div>
                }

                {   
                    <SocialMediaComponent socialmedia={SocialMedia} label={SocialMediaLabel} />
                    
                }


            </React.Fragment>
        );
    }

}

export default OrganizationInfo;