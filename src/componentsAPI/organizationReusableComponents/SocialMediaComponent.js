import React from "react";

import Typography from '@material-ui/core/Typography';
import SocialMediaType from '../CommonComponents/SocialMediaType'

class SocialMediaComponent extends React.Component {

    render() {
        const { socialmedia, label } = this.props;
        return (
            <React.Fragment>

            {socialmedia &&
                    socialmedia.length > 0 ? (
                        <div className="padding15">
                            <Typography className="bold-p--id">{label}</Typography>
                            {socialmedia.map((keyword, smindex) =>
                                <span style={{display:"inline-block"}}  key={smindex}>
                                    <SocialMediaType account_type={keyword.social_media_occupational_account_type.field_value} value={keyword.value.field_value} />
                                </span>

                            )}
                        </div>)
                    : void 0
                             }
 </React.Fragment>
        );
    }

}

export default SocialMediaComponent;
