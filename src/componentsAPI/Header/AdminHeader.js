import React from "react";
import axios from "axios";
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
//import Grid from '@material-ui/core/Grid';
import { DJANGO_ADMIN_PAGE, DJANGO_ADMIN_PAGE_LOGIN, SHOW_ADMINISTRATOR_ROLES, AUTHENTICATED_KEYCLOAK_USER_ROLES } from "../../config/constants";
import { ReactComponent as AdminIcon } from "../../assets/elg-icons/editor/single-neutral-actions-setting.svg";

export default class AdminHeader extends React.Component {
    constructor(props) {
        super(props);
        this.state = { headerData: '', keycloak: props.keycloak, UserRoles: [] };
        this.isUnmount = false;
        this.isAuthorizedToView = this.isAuthorizedToView.bind(this);
        this.loginToAcquireSessionFromDgango = this.loginToAcquireSessionFromDgango.bind(this);
    }

    componentDidMount() {
        const { keycloak } = this.props;
        this.setState({ UserRoles: AUTHENTICATED_KEYCLOAK_USER_ROLES(keycloak) });
    }

    loginToAcquireSessionFromDgango(keycloak) {
        const { tokenParsed } = keycloak || "";
        const config = { headers: { 'Content-Type': 'application/json' } };
        axios.post(DJANGO_ADMIN_PAGE_LOGIN, tokenParsed, config).then(res => {
            //window.open(DJANGO_ADMIN_PAGE, "_blank");
            window.open(DJANGO_ADMIN_PAGE, "_self");
        }).catch(err => console.log("error at login"));
    }

    isAuthorizedToView(roles) {
        var isAuthorized = false;
        SHOW_ADMINISTRATOR_ROLES.forEach(item => {
            if (roles.includes(item)) {
                isAuthorized = true;
            }
        })
        return isAuthorized;
    }

    render() {
        if (!this.state.UserRoles.length === 0) {
            return <div></div>
        }
        const isAuth = this.isAuthorizedToView(this.state.UserRoles);
        if (!isAuth) {
            return <div></div>
        } else {
            //this.loginToAcquireSessionFromDgango(this.props.keycloak);
        }
        const show_icon = this.props.show_icon;
        return <>
        {show_icon===true ? 
                <Button className="app--bar--default" onClick={() => this.loginToAcquireSessionFromDgango(this.props.keycloak)} startIcon={<AdminIcon className="xsmall-icon" />}><Typography variant="h6" className="pl-1">Administration</Typography></Button>
             
        :
        <Button className="drawerLinkMenu" onClick={() => this.loginToAcquireSessionFromDgango(this.props.keycloak)}><span>Administration</span></Button>
    }
    </>
    }

}