import React from "react";
import Button from '@material-ui/core/Button';
import { ReactComponent as BuildIcon } from "../../assets/elg-icons/layout-dashboard.svg";
import { /*SHOW_DASHBOARD_ROLES,*/ AUTHENTICATED_KEYCLOAK_USER_ROLES } from "../../config/constants";
//import Tooltip from '@material-ui/core/Tooltip';
import { withRouter } from "react-router-dom";

class UserDashboardComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = { UserRoles: [] };//isOpen: false,
    }

    componentDidMount() {
        const { keycloak } = this.props;
        this.setState({ UserRoles: AUTHENTICATED_KEYCLOAK_USER_ROLES(keycloak) });
    }

    isAuthorizedToView = (roles) => {
        var isAuthorized = false;
        /*SHOW_DASHBOARD_ROLES.forEach(item => {
            if (roles.includes(item)) {
                isAuthorized = true;
            }
        });*/
        isAuthorized = this.props.keycloak && this.props.keycloak.authenticated;
        return isAuthorized;
    }

    render() {
        const { keycloak } = this.props;
        if (!keycloak || (keycloak && !Boolean(keycloak.authenticated))) {
            return <></>
        }
        if (!this.isAuthorizedToView(this.state.UserRoles)) {
            return <></>
        }
        return <>
            <Button onClick={() => { this.props.history.push("/mygrid"); }} className="dashboard-header-button" endIcon={<BuildIcon className="dashboard-header-icon" />}> My grid </Button>
        </>
    }
}
export default withRouter(UserDashboardComponent);