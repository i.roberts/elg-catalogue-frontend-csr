import React from "react";
import { withRouter } from "react-router-dom";
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { EDITOR_PERMITTED_ROLES, AUTHENTICATED_KEYCLOAK_USER_ROLES } from "../../config/constants";
import { ReactComponent as CreateIcon } from "../../assets/elg-icons/editor/pencil-write.svg";
import { Link } from "react-router-dom";
//import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import { ReactComponent as KeyboardArrowDownIcon } from "../../assets/elg-icons/arrow-down-1.svg";

class CreateResourceHeader extends React.Component {
    constructor(props) {
        super(props);
        this.state = { isOpen: false, UserRoles: [] };
    }

    componentDidMount() {
        const { keycloak } = this.props;
        this.setState({ UserRoles: AUTHENTICATED_KEYCLOAK_USER_ROLES(keycloak) });
    }

    isAuthorizedToView = (roles) => {
        var isAuthorized = false;
        EDITOR_PERMITTED_ROLES.forEach(item => {
            if (roles.includes(item)) {
                isAuthorized = true;
            }
        });
        return isAuthorized;
    }


    render() {
        if (!this.isAuthorizedToView(this.state.UserRoles)) {
            return <></>
        }
        const show_icon = this.props.show_icon;
        return <>
            {show_icon === true ? <Grid item xs={12} md={2} className="dropdown">
                <Button className="app--bar--default" value={this.state.value} onClick={(event, newValue) => { this.props.history.push("/createResource"); }} startIcon={<CreateIcon className="xsmall-icon" />}>
                    <Typography variant="h6" className="pl-1"><span className="mr-05">Add items</span><span><KeyboardArrowDownIcon className="xxsmall-icon" /></span></Typography>
                </Button>
                <div className="dropdown-content">
                    <Link to="/createResource">Interactive editor</Link>
                    <Link to="/upload/validate-xml-files/">Validate XML</Link>
                    <Link to="/upload/upload-single-file/">Upload XML</Link>
                </div>
            </Grid>
                :
                <Link className="mx-3 nav-item ng-star-inserted active-list-item headerMenu" to="/createResource">Add items</Link>
            }

        </>

    }
}

export default withRouter(CreateResourceHeader);