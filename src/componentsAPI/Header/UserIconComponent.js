import React from "react";
import axios from "axios";
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
//import InputIcon from "../../assets/elg-icons/input.svg";
import InputIcon from '@material-ui/icons/Input';
import Button from '@material-ui/core/Button';
import { DJANGO_ADMIN_PAGE_LOGOUT, SHOW_ADMINISTRATOR_ROLES, AUTHENTICATED_KEYCLOAK_USER_ROLES } from "../../config/constants";
import Tooltip from '@material-ui/core/Tooltip';
import Grid from '@material-ui/core/Grid';
import { getLogoutUrl } from "../../config/constants";

export default class UserIconComponent extends React.Component {
    constructor(props) {
        super(props);
        this.logout = this.logout.bind(this);
    }

    componentDidMount() {
        const { keycloak } = this.props;
        this.setState({ UserRoles: AUTHENTICATED_KEYCLOAK_USER_ROLES(keycloak) });
    }

    logout() {
        const { keycloak } = this.props;
        const isAuth = this.isAuthorizedToView(this.state.UserRoles);
        const logoutOptionsObject = { 'redirectUri': getLogoutUrl() }
        if (isAuth) {
            axios.get(DJANGO_ADMIN_PAGE_LOGOUT).then(res => { keycloak.logout(logoutOptionsObject); }).catch(e => { keycloak.logout(logoutOptionsObject); });
        } else {
            keycloak.logout(logoutOptionsObject);
        }
    }

    isAuthorizedToView(roles) {
        var isAuthorized = false;
        SHOW_ADMINISTRATOR_ROLES.forEach(item => {
            if (roles.includes(item)) {
                isAuthorized = true;
            }
        })
        return isAuthorized;
    }

    render() {
        const { keycloak, baseURL } = this.props;
        if (!keycloak || (keycloak && !Boolean(keycloak.authenticated))) {
            return (
                <span onClick={() => { keycloak ? keycloak.login() : void 0 }} style={{ cursor: "pointer", marginRight: "1.25rem" }}>
                    <AccountCircleIcon />
                </span>
            )
        }
        const { given_name = "", family_name = "", email = "" } = keycloak.idTokenParsed || "";
        return (
            <Grid container direction="row" justifyContent="flex-end" alignItems="center" >
                <Grid item xs={9}><span style={{ cursor: "pointer" }} className="profile_name">
                    <a href={`${baseURL}profile`}>{<span>{given_name} {family_name}</span> || email} </a>
                </span>
                </Grid>
                <Grid item xs={3}>
                    <Tooltip title="Log out"><Button onClick={() => this.logout()} className="logout-header-button"><InputIcon /></Button></Tooltip>
                </Grid>
            </Grid>
        )
    }
}