
import React from 'react';
import { withRouter } from "react-router-dom";
import GoToCatalogue from "../CommonComponents/GoToCatalogue";
import ValidateXml from "./ValidateXml";
import Container from '@material-ui/core/Container';

class PublicValidateXml extends React.Component {

  render() {
    return (
      <>
        <div className="search-top">
          <GoToCatalogue />
        </div>
        <Container maxWidth="lg">
                <div className="metadata-main-card-container">
        <ValidateXml {...this.props} helmet_value="validate xml" />
        </div>
        </Container>
      </>
    );
  }
}

export default withRouter(PublicValidateXml);