
import React from 'react';
import { withRouter } from "react-router-dom";
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import { Helmet } from "react-helmet";
import Grid from '@material-ui/core/Grid';
import { VALIDATE_XML_ENDPOINT } from "../../config/constants";
import { FilePond, registerPlugin } from 'react-filepond';
import 'filepond/dist/filepond.min.css';
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
import FilePondPluginFileValidateType from 'filepond-plugin-file-validate-type';
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.css';
import { toast } from "react-toastify";
import CircularProgress from '@material-ui/core/CircularProgress';
registerPlugin(FilePondPluginImagePreview, FilePondPluginFileValidateType);

class ValidateXml extends React.Component {
  constructor(props) {
    super(props);
    this.state = { xmlUploadError: null, successResponse: null, loading: false }
  }

  handle_simple_xml_error = (errorResponse) => {
    let obj = {};
    try {
      obj = JSON.parse(errorResponse);
    } catch (err) {
      obj.message = "Internal server error";
    }
    const format = JSON.stringify(obj, null, 2);
    this.setState({ xmlUploadError: format, successResponse: null, loadaing: false });
    toast.error("Upload failed.", { autoClose: 3500 });
  }

  handleSimpleXMLResponse = (response) => {
    const responseParsed = JSON.parse(response);
    const formated = this.processResponse(responseParsed);
    this.setState({ xmlUploadError: null, successResponse: formated, under_construction: false, functional_service: false, loading: false });
    //console.log(response);
  }

  processResponse = (responseParsed) => {
    if (!responseParsed) {
      return 'empty response';
    }
    try {
      if (responseParsed.hasOwnProperty("errors") || responseParsed.hasOwnProperty("batch/") || responseParsed.hasOwnProperty("batch")) {
        // return JSON.stringify(responseParsed, null, 2);
      } else {
        //return responseParsed.info;
      }
      return JSON.stringify(responseParsed, null, 2);
    } catch (error) {
      return JSON.stringify(responseParsed, null, 2);
    }
  }

  ShowErrorMessage = (error) => {
    return <div>
      {error && <div style={{ marginBottom: "100px", paddingBottom: "100px" }}>
        <h3>Error</h3>
        <div className=" boxed">
          <Paper elevation={13} >
            <code>
              <pre id="special">
                {error}
              </pre>
            </code>
          </Paper>
        </div>
      </div>
      }
    </div>
  }

  ShowSuccessMessage = (valid) => {
    return <div>
      {valid && <div style={{ marginBottom: "100px", paddingBottom: "100px" }}>
        <h3>Validation report</h3>
        <div className=" boxed">
          <Paper elevation={13} >
            <code>
              <pre id="special">
                {valid}
              </pre>
            </code>
          </Paper>
        </div>
      </div>
      }
    </div>
  }

  loadingMessage = () => {
    return <>
      <Typography variant="subtitle1">Your  file is being processed by the system. Please wait <CircularProgress size={20} /></Typography>
    </>
  }


  render() {
    return (
      <>
        <Helmet>
          <title>ELG - {this.props.helmet_value} </title>
        </Helmet>
        <div>
          <div className="centered-text"><Typography variant="subtitle1">You can upload and validate your XML file(s) against the ELG XML schema.</Typography></div>
          <div className="centered-text mb2"><Typography variant="caption">If you want to validate multiple files, please add them all directly in the zip file (without subfolders).</Typography></div>
          <Grid container direction="row" justifyContent="center" alignItems="center" spacing={2}>

            <Grid item container xs={9} className="mb2" spacing={1}>

            </Grid>
          </Grid>

          <FilePond
            credits={false}
            allowMultiple={false}
            name="file"
            onprocessfileabort={() => {
              this.setState({ xmlUploadError: null, successResponse: null, loading: false });
            }}
            labelIdle={'Drag & Drop your file or <span class="filepond--label-action"> Browse </span>'}
            server={
              {
                url: VALIDATE_XML_ENDPOINT,
                process: {
                  method: 'POST',
                  onload: (response) => this.handleSimpleXMLResponse(response),
                  onerror: (response) => this.handle_simple_xml_error(response),
                },
                remove: null,
                revert: null,
                fetch: null,
                restore: null,
                load: null
              }
            }
            onremovefile={(file) => { this.setState({ xmlUploadError: null, successResponse: null, loading: false }); }}
            onprocessfilestart={(file) => { this.setState({ xmlUploadError: null, successResponse: null, loading: true }) }}
            onde
          />
          {this.state.loading && this.loadingMessage()}
          {this.state.xmlUploadError && this.ShowErrorMessage(this.state.xmlUploadError)}
          {this.state.successResponse && this.ShowSuccessMessage(this.state.successResponse)}
        </div>
      </>
    );
  }
}

export default withRouter(ValidateXml);