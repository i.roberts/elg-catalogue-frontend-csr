
import React from 'react';
import { withRouter, Link } from "react-router-dom";
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import { Helmet } from "react-helmet";
import Grid from '@material-ui/core/Grid';
import { UPLOAD_XML_METADATARECORD_ENDPOINT, getAuthorizationHeader, get_landing_page_url, getEditorPath_labeless_Schema } from "../../config/constants";
import messages from "../../config/messages";
import { FilePond, registerPlugin } from 'react-filepond';
import 'filepond/dist/filepond.min.css';
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
import FilePondPluginFileValidateType from 'filepond-plugin-file-validate-type';
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.css';
import { toast } from "react-toastify";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import CircularProgress from '@material-ui/core/CircularProgress';
registerPlugin(FilePondPluginImagePreview, FilePondPluginFileValidateType);

const xml = ["text/xml, application/xml"];


class SingleXmlUpload extends React.Component {
  constructor(props) {
    super(props);
    this.state = { xmlUploadError: null, under_construction: false, functional_service: false, xml_upload_with_data: false, loading: false, xml: ["text/xml, application/xml"] }
  }

  handle_simple_xml_error = (errorResponse) => {
    let errData = '';
    try {
      errorResponse = JSON.parse(errorResponse);
      const isDuplicateError = this.duplicateName(errorResponse);
      const isVersionError = this.versionError(errorResponse);
      if (isDuplicateError && isDuplicateError.length > 0) {
        errData = isDuplicateError[0]
      } else if (isVersionError && isVersionError.length > 0) {
        errData = isVersionError[0]
      } else {
        errData = JSON.stringify(errorResponse, null, 2);
      }
      this.setState({ xmlUploadError: errData, loading: false });
      toast.error("Upload failed.", { autoClose: 3500 });
      return;
    } catch (err) {
      errData = "Internal server error";
      this.setState({ xmlUploadError: errData, loading: false });
      toast.error("Upload failed.", { autoClose: 3500 });
      return;
    }
  }

  duplicateName = (errData) => {
    try {
      if (errData.hasOwnProperty("duplication_error")) {
        const duplication_error = errData.duplication_error;
        const duplicate_records = errData.duplicate_records;
        if (duplication_error && duplicate_records) {
          const errorMessageArray = [];
          //duplication_error.map((errorMessage, index) => errorMessageArray.push(errorMessage));
          //duplicate_records.map((errorMessage, index) => errorMessageArray.push(errorMessage));
          errorMessageArray.push("There is already a record with this name. Please enter a different name.")
          return errorMessageArray;
        }
      } else {
        return null;
      }
    } catch (err) {
      return null;
    }
  }

  versionError = (errData) => {
    try {
      if (errData.hasOwnProperty("described_entity") && errData.described_entity.hasOwnProperty("version")) {
        const veresionErrorArray = errData.described_entity.version.map(item => item);
        return veresionErrorArray;
      } else {
        return null;
      }
    } catch (error) {
      return null;
    }
  }


  handleSimpleXMLResponse = (response) => {
    const responseParsed = JSON.parse(response);
    if (!this.state.xml_upload_with_data) {
      this.props.history.push(get_landing_page_url(responseParsed))
    } else {
      this.props.history.push(getEditorPath_labeless_Schema(responseParsed))
      //console.log(getEditorPath_labeless_Schema(responseParsed));
    }

    toast.success("File has been successfully uploaded.", { autoClose: 3500 });
  }

  ShowErrorMessage = (error) => {
    return <div>
      {error && <div style={{ marginBottom: "100px", paddingBottom: "100px" }}>
        <h3>Error</h3>
        <div className=" boxed">
          <Paper elevation={13} >
            <code>
              <pre id="special">
                {error}
              </pre>
            </code>
          </Paper>
        </div>
      </div>
      }
    </div>
  }

  loadingMessage = () => {
    return <>
      <Typography variant="subtitle1">Your xml file is being processed by the system. Please wait <CircularProgress size={20} /></Typography>
    </>
  }


  render() {

    return (
      <>
        <Helmet>
          <title>ELG - {this.props.helmet_value} </title>
        </Helmet>
        <div>
          <div className="centered-text"><Typography variant="subtitle1">You can upload one <span className="bold-titles">xml</span> file each time.</Typography></div>
          <div className="centered-text mb2"><Typography variant="subtitle1">It is highly recommended that you <Link to="/upload/validate-xml-files/">validate </Link>your XML file against the ELG schema before you proceed.</Typography></div>
          <Grid container direction="row" justifyContent="center" alignItems="center" spacing={2}>

            <Grid item container xs={9} className="mb2" spacing={1}>

              <Grid item xs={4}>
                <FormControlLabel className="upload-checkboxes p-05 wd-100"
                  control={<Checkbox
                    checked={this.state.under_construction === true ? true : false}
                    color="primary"
                    inputProps={{ 'aria-label': 'under construction checkbox' }}
                    onChange={e => { this.setState({ under_construction: e.target.checked }); }}
                  />}
                  label={messages.label_under_construction}
                />
              </Grid>

              <Grid item xs={8}>
                <Typography variant="subtitle1">{messages.editor_under_construction}</Typography>
              </Grid>

              <Grid item xs={4}>
                <FormControlLabel className="upload-checkboxes p-05 wd-100"
                  control={<Checkbox
                    checked={this.state.functional_service === true ? true : false}
                    color="primary"
                    inputProps={{ 'aria-label': 'functional service checkbox' }}
                    onChange={e => { this.setState({ functional_service: e.target.checked }); }}
                  />}
                  label={messages.label_functional_service} />
              </Grid>

              <Grid item xs={8}>
                <Typography variant="subtitle1">If the metadata record is for a service to be integrated in ELG <span><a href={messages.dialog_functional_service_infoLink_message} target="_blank" rel="noopener noreferrer">({messages.dialog_functional_service_infoLink_message})</a></span>, please check the box "ELG-compatible service". </Typography>
              </Grid>

              <Grid item xs={4}>
                <FormControlLabel className="upload-checkboxes p-05 wd-100"
                  control={<Checkbox
                    checked={this.state.xml_upload_with_data === true ? true : false}
                    color="primary"
                    inputProps={{ 'aria-label': 'xml upload with data' }}
                    onChange={e => { this.setState({ xml_upload_with_data: e.target.checked }); }}
                  />}
                  label={messages.label_xml_upload_with_data} />
              </Grid>

              <Grid item xs={8}>
                <Typography variant="subtitle1">If you intend to upload a data file after the xml upload. After the xml upload you will be redirected to the editor where you can upload your files and associate them with the corresponding distributions.</Typography>
              </Grid>

            </Grid>
          </Grid>

          <FilePond
            credits={false}
            allowMultiple={false}
            name="file"
            instantUpload={false}
            acceptedFileTypes={xml}
            labelIdle={'Drag & Drop your file or <span class="filepond--label-action"> Browse </span>'}
            server={
              {
                url: UPLOAD_XML_METADATARECORD_ENDPOINT,
                process: {
                  method: 'POST',
                  headers: {
                    'Authorization': getAuthorizationHeader(this.props.keycloak),
                    'UNDER-CONSTRUCTION': this.state.under_construction === true ? true : false,
                    'FUNCTIONAL-SERVICE': this.state.functional_service === true ? true : false,
                    "XML-UPLOAD-WITH-DATA": this.state.xml_upload_with_data === true ? true : false
                  },
                  onload: (response) => this.handleSimpleXMLResponse(response),
                  onerror: (response) => this.handle_simple_xml_error(response),
                },
                remove: null,
                revert: null,
                fetch: null,
                restore: null,
                load: null
              }
            }
            onremovefile={(file) => { this.setState({ xmlUploadError: null, under_construction: false, functional_service: false, xml_upload_with_data: false, loading: false }); xml[0] = "text/xml"; xml[1] = "application/xml" }}
            onprocessfilestart={(file) => { this.setState({ xmlUploadError: null, loading: true }); xml[0] = "text/xml"; xml[1] = "application/xml" }}
            oninit={() => { xml[0] = "text/xml"; xml[1] = "application/xml" }}
          />
          {this.state.loading && this.loadingMessage()}
          {this.state.xmlUploadError && this.ShowErrorMessage(this.state.xmlUploadError)}
        </div>
      </>
    );
  }
}

export default withRouter(SingleXmlUpload);