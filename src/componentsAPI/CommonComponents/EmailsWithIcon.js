import React from "react";
import { ReactComponent as MailOutlineIcon} from './../../assets/elg-icons/email-action-send-1.svg';

function reverseString(str) {
    return str.split("").reverse().join("");
}

export default class EmailsWithIcon extends React.Component {   

    render() {
        const { emailArray } = this.props;
        if (!emailArray) {
            return <div></div>
        }
        return <>
            {emailArray && emailArray.map((site, siteIndex) => {                 
                 return <div key={siteIndex}>
                    <a href={`mailto:${reverseString(site)}`} className="info_url" target="_blank" rel="noopener noreferrer" >
                        {<div><span><MailOutlineIcon className="xsmall-icon"/></span> <span>Email</span></div>}
                        
                    </a>
                </div>
                }

            )} 
    
            
            </>
    }
}
