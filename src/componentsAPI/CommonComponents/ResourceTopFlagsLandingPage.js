import React from "react";
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Chip from '@material-ui/core/Chip';

export default class ResourceTopFlagsLandingPage extends React.Component {

    render() {
        const { tombstone, alignItems, xSize } = this.props;

        return (
            <Grid container direction="column" justifyContent="center" alignItems={alignItems}>
                <Grid item xs={xSize}>
                    {tombstone === "Y" && <div className="pb-1"> <Chip size="medium" label={"Deleted"} className="ChipTagDarkGrey" /><Typography variant="body2" color="textSecondary" component="p"> For further information, please contact us at <a href="https://www.european-language-grid.eu/contact/" target="_blank" rel="noopener noreferrer">https://www.european-language-grid.eu/contact/</a> </Typography></div> }
                    {tombstone === "T" && <div className="pb-1"><Chip size="medium" label={"Temporarily unavailable"} className="ChipTagDarkGrey" /> <Typography variant="body2" color="textSecondary" component="p"> For further information, please contact us at <a href="https://www.european-language-grid.eu/contact/" target="_blank" rel="noopener noreferrer">https://www.european-language-grid.eu/contact/</a> </Typography></div>}
                </Grid>
            </Grid>
        );

    }
}