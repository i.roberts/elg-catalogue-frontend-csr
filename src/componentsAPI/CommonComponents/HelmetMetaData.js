import React from "react";
import { Helmet } from "react-helmet";
import {  BASE_URL  } from "./../../config/constants";

export default class HelmetMetaData extends React.Component {

    render() {

        const { data, keywords, domainKeywords, subjectKeywords, intentedKeywords, corpus_subclass, languages,
            resourceName, description, resourceShortName , for_information_only, ltAreaKeywordsArray,
            disciplines, servicesOffered, OrganizationRolesArray, ld_subclass,lcr_subclass, type, pk} = this.props;

        const SEO_KEYWORDS = new Set();
        keywords && keywords.forEach(i => SEO_KEYWORDS.add(i))
        domainKeywords && domainKeywords.forEach(i => SEO_KEYWORDS.add(i));
        subjectKeywords && subjectKeywords.forEach(i => SEO_KEYWORDS.add(i));
        intentedKeywords && intentedKeywords.forEach(i => SEO_KEYWORDS.add(i));
        ltAreaKeywordsArray && ltAreaKeywordsArray.forEach(i => SEO_KEYWORDS.add(i));        
        corpus_subclass && SEO_KEYWORDS.add(corpus_subclass); 
        disciplines && disciplines.forEach(i => SEO_KEYWORDS.add(i));
        servicesOffered && servicesOffered.forEach(i => SEO_KEYWORDS.add(i));
        OrganizationRolesArray && OrganizationRolesArray.forEach(i => SEO_KEYWORDS.add(i));        
        ld_subclass && SEO_KEYWORDS.add(ld_subclass);
        lcr_subclass && SEO_KEYWORDS.add(lcr_subclass);
        languages && languages.forEach(i => SEO_KEYWORDS.add(i));
        const lrfunction = data.described_entity.field_value.lr_subclass && data.described_entity.field_value.lr_subclass.field_value.function ? data.described_entity.field_value.lr_subclass.field_value.function : null;
        if (lrfunction) lrfunction.field_value.forEach(i => {
            if (i.label) {
                // function is an LTClassRecommended
                SEO_KEYWORDS.add(i.label["en"]);
            } else if (i.value) {
                // function is an LTClassOther
                SEO_KEYWORDS.add(i.value);
            }
        });

        return <div>
            <Helmet>
                <title>ELG - {resourceName}</title>
                {!for_information_only && data.management_object.json_ld && <script type="application/ld+json">
                    {JSON.stringify(data.management_object.json_ld)}
                </script>}
                <meta name="description" content={description} ></meta>
                <meta name="keywords" content={[...SEO_KEYWORDS].join(", ")} />
                <link rel="canonical" href={`${BASE_URL}catalogue/${type}/${pk}`}></link>
                <meta property="og:url" content={`${BASE_URL}catalogue/${type}/${pk}`} />
                <meta property="og:site_name" content="European Language Grid" />
                <meta property="og:type" content="website" />
                <meta property="og:title" content={`ELG - ${resourceName}`} />
                <meta property="og:description" content={description}></meta>
                <meta property="quote" content={resourceShortName} />
                <meta property="og:quote" content={`ELG - ${resourceName}`} />
                <meta property="og:hashtag" content={resourceShortName} />
                <meta property="og:image" content={"https://live.european-language-grid.eu/assets/img/elg-logo.svg"} />
                <meta property="og:image:width" content="1080" />
                <meta property="og:image:height" content="608" />
                <meta content="image/*" property="og:image:type" />
                <meta property="og:image:alt" content="European Language Grid" />
                <meta name="twitter:card" content="summary" />
                <meta name="twitter:title" property="og:title" itemprop="name" content={`ELG - ${resourceName}`} />
                <meta name="twitter:description" property="og:description" itemprop="description" content={description} />
                <meta name="twitter:image" content={"https://live.european-language-grid.eu/assets/img/elg-logo.svg"} />
            </Helmet>
        </div>
    }
}
