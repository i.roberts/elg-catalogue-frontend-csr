import React from "react";
//import FormHelperText from '@material-ui/core/FormHelperText';
//import FormControl from '@material-ui/core/FormControl';
//import InputLabel from '@material-ui/core/InputLabel';
import Tooltip from '@material-ui/core/Tooltip';
import Select from '@material-ui/core/Select';
import Typography from "@material-ui/core/Typography";
import MenuItem from '@material-ui/core/MenuItem';


export default class SelectAndOr extends React.Component {
    render() {
        let disabled = true;
        if (this.props.field === "global_selection_mode") {
            disabled = this.props.selectedFacets && this.props.selectedFacets.length > 0;
        } else {
            disabled = this.props.facet_values.filter(item => item && this.props.selectedFacets.includes(item.key)).length > 0;
        }

        return <div style={{ padding: "9px 16px"}} className="flex-select">
             <div>
            <Typography variant="caption" className="pl05 grey--font">Filters selection behaviour: </Typography>
            </div>
            <div className="flex-select-1">
            <Tooltip title={disabled ? "In order to change filtering mode you need to cancel all of your selections" : ""}>
                <Select
                    className ="wd-100"
                    disabled={disabled}
                    labelId="demo-simple-select-required-label"
                    id="demo-simple-select-required"
                    value={this.props.initialValue}
                    onChange={(e) => { this.props.handleSelectionModeChoice(this.props.field, e.target.value, this.props.selectedFacets) }}
                >
                    <MenuItem className="facet-select" value={this.props.selectionMode.and}>{this.props.selectionMode.and}</MenuItem>
                    <MenuItem className="facet-select" value={this.props.selectionMode.or}>{this.props.selectionMode.or}</MenuItem>
                </Select>
            </Tooltip>
            </div>
           
        </div>
    }
}