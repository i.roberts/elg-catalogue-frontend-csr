import React from "react";
import Button from '@material-ui/core/Button';
import { ReactComponent as ClaimIcon } from "./../../assets/elg-icons/editor/single-neutral-actions-text.svg";
import messages from "./../../config/messages";
import { EDITOR_CLAIM } from "./../../config/editorConstants";
//import { AUTHENTICATED_KEYCLOAK_USER_USERNAME, SYS_ADMIM_ID } from "./../../config/constants";
import { AUTHENTICATED_KEYCLOAK_USER_USERNAME } from "./../../config/constants";
import axios from "axios";
import Typography from '@material-ui/core/Typography';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import { toast } from "react-toastify";
import Tooltip from '@material-ui/core/Tooltip';
import CircularProgress from '@material-ui/core/CircularProgress';

export default class Claim extends React.Component {
    constructor(props) {
        super(props);
        this.state = { isAuthorized: false, already_claimed: null, show_but_disabled: false, dialogOpen: false, loading: false };
    }

    componentDidMount() {
        const { data, keycloak } = this.props;
        const claimed = data.management_object.claimed || null; //field claimed will have the username of the user that issued the claim request

        if (keycloak && Boolean(keycloak.authenticated)) {
            this.setState({ isAuthorized: true });
        }
        if (claimed !== null) {
            this.setState({ already_claimed: true });
            this.SetVisibilityOfClaim();
        }
        //this.SetVisibilityOfClaim();
    }

    SetVisibilityOfClaim = () => { //this will help us separate the case when the logged in user is the claimer     
        const { data, keycloak } = this.props;
        const claimed = data.management_object.claimed || null; //field claimed will have the username of the user that issued the claim request        
        const claimer_username = AUTHENTICATED_KEYCLOAK_USER_USERNAME(keycloak);

        if (claimed === claimer_username) {
            this.setState({ show_but_disabled: true })  //if I am the logged in user that claimed the record, show the claim button as disabled             
        }
    }

    handleClose = () => {
        this.setState({ dialogOpen: false });
    };

    handleOpen = () => {
        this.setState({ dialogOpen: true });
    };

    claimRecord = () => {
        this.setState({ show_but_disabled: true, already_claimed: true, loading: true });
        axios.patch(EDITOR_CLAIM(this.props.pk)).then((res) => {
            toast.success(`${messages.claim_success_message}`, { autoClose: 3500 });
            this.setState({ show_but_disabled: true, already_claimed: true, loading: false });
        }).catch((err) => {
            console.log(err)
            toast.error("Claim action failed", { autoClose: 3500 });
            this.setState({ show_but_disabled: false, already_claimed: false, loading: false });
        });
        return "";
    }

    render() {
        const { data } = this.props;
        const status = data.management_object.status || null;
        //const currator_id = data.management_object.curator || null;
        const is_claimable = data.management_object.is_claimable || false;

        //This rule was used to show/hide claim before the ELE.
        /*if (currator_id !== SYS_ADMIM_ID || status !== "p" || data.source_of_metadata_record != null) { //"claim" is a process that is triggered only on metadata records uploaded with the sys-admin account
            return <></> //also source_of_metadata_record != null -> harvested -> not claimable
        }*/

        if (!is_claimable || status !== "p" || data.source_of_metadata_record != null) { //"claim" is a process that is triggered only on metadata records uploaded with the sys-admin account
            return <></> //also source_of_metadata_record != null -> harvested -> not claimable
        }

        return <div className="pb-3">
            {!this.state.isAuthorized && !this.state.already_claimed && <>
                <Tooltip title={messages.claim_button_hover}><Button
                    className="inner-link-default--purple wd-100"
                    onClick={this.handleOpen}>
                    <ClaimIcon className="small-icon" /><span className="p-1">{messages.editor_action_claim_secondary} </span>
                </Button></Tooltip>

                <Dialog onClose={this.handleClose} aria-labelledby="customized-dialog-title" open={this.state.dialogOpen}>
                    <DialogTitle id="customized-dialog-title" onClose={this.handleClose}>
                        {messages.editor_action_claim_primary}
                    </DialogTitle>

                    <DialogContent dividers>
                        <Typography gutterBottom>{messages.claim_intro_text}</Typography>
                        <Typography gutterBottom>{messages.claim_full_text}</Typography>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">Close</Button>
                    </DialogActions>
                </Dialog>
            </>}

            {this.state.isAuthorized && (this.state.show_but_disabled || !this.state.already_claimed) &&   //this means that the record is claimed by the logged in user                             
                <Tooltip title={this.state.show_but_disabled ? messages.editor_action_claimed : messages.claim_button_hover}>
                    <span>
                        <Button
                            className="inner-link-default--purple wd-100"
                            disabled={this.state.show_but_disabled}
                            onClick={this.claimRecord}>
                            <ClaimIcon className="small-icon" /><span className="p-1">{this.state.show_but_disabled ? messages.editor_action_claimed : messages.editor_action_claim_secondary} </span>
                            {this.state.loading && <CircularProgress size={20} />}
                        </Button>

                    </span>
                </Tooltip>
            }

        </div>
    }

}