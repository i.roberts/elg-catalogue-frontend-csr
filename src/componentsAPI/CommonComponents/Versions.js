import React from "react";
import Typography from '@material-ui/core/Typography';
import commonParser from "../../parsers/CommonParser";
import GeneralIdentifier from "./GeneralIdentifier";
import { Link } from "react-router-dom";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';

export default class Versions extends React.Component {

    constructor(props) {
        super(props);
        this.state = { expandedVersion: false };
    }

    togglexpandedVersion = () => {
        this.setState({ expandedVersion: !this.state.expandedVersion })
    }


    render() {
        const { all_versions, all_versions_label, metadataLanguage } = this.props;
        const { expandedVersion } = this.state;

        //console.log(all_versions)
        if (!all_versions) {
            return <></>
        }
        return <div className="ActionsButtonArea" style={{ marginBottom: '1em' }}>
            <Typography variant="h3" className="title-links">{all_versions_label}</Typography>

            {
                all_versions && all_versions.field_value.length > 0 ?
                    (<div>

                        {expandedVersion ? (
                            <div>
                                {all_versions.field_value.map((versionItem, index) => {
                                    let resource_name = (versionItem.resource_name.field_value[metadataLanguage] || versionItem.resource_name.field_value[Object.keys(versionItem.resource_name.field_value)[0]]) || "";
                                    let version = versionItem.version ? versionItem.version.field_value : '';
                                    let highlight = versionItem.highlight ? "highlight" : "";
                                    //let version_label = (versionItem.version.field_value[metadataLanguage] || versionItem.version.field_value[Object.keys(versionItem.version.field_value)[0]]) || "";
                                    let full_metadata_record = commonParser.getFullMetadata(versionItem.full_metadata_record);
                                    return <div key={index} className={"padding5 p-05 " + highlight}>
                                        {full_metadata_record ? <>
                                            <div className="mb-05">
                                                <Link to={full_metadata_record.internalELGUrl}>
                                                    <span> {resource_name} ({version}) </span>
                                                </Link>
                                            </div>
                                            <GeneralIdentifier data={versionItem} identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"} metadataLanguage={metadataLanguage} />
                                        </> :
                                            resource_name && <><div className="info_value mb-05">  {resource_name} ({version}) </div>
                                                <GeneralIdentifier data={versionItem} identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"} metadataLanguage={metadataLanguage} /></>}
                                    </div>
                                })}

                                <span className="ExpandButton align-right padding5 grey--font" onClick={this.togglexpandedVersion} > <ExpandLessIcon className="grey--font" /> </span>
                            </div>
                        ) : (
                            <div>
                                {all_versions.field_value.slice(0, 3).map((versionItem, index) => {
                                    let resource_name = (versionItem.resource_name.field_value[metadataLanguage] || versionItem.resource_name.field_value[Object.keys(versionItem.resource_name.field_value)[0]]) || "";
                                    let version = versionItem.version ? versionItem.version.field_value : '';
                                    let highlight = versionItem.highlight ? "highlight" : "";
                                    //let version_label = (versionItem.version.field_value[metadataLanguage] || versionItem.version.field_value[Object.keys(versionItem.version.field_value)[0]]) || "";
                                    let full_metadata_record = commonParser.getFullMetadata(versionItem.full_metadata_record);
                                    return <div key={index} className={"padding5 p-05 " + highlight}>
                                        {full_metadata_record ? <>
                                            <div className="mb-05">
                                                <Link to={full_metadata_record.internalELGUrl}>
                                                    <span> {resource_name} ({version}) </span>
                                                </Link>
                                            </div>
                                            <GeneralIdentifier data={versionItem} identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"} metadataLanguage={metadataLanguage} />
                                        </> :
                                            resource_name && <><div className="info_value mb-05">  {resource_name} ({version}) </div>
                                                <GeneralIdentifier data={versionItem} identifier_name={"lr_identifier"} identifier_scheme={"lr_identifier_scheme"} metadataLanguage={metadataLanguage} /></>}
                                    </div>
                                })}

                                {all_versions.field_value.length > 3 ?
                                    <span className="ExpandButton align-right padding5 grey--font" onClick={this.togglexpandedVersion} > <ExpandMoreIcon className="grey--font" /> </span>
                                    : void 0}
                            </div>
                        )
                        }

                    </div>)
                    : void 0
            }


        </div>

    }
}