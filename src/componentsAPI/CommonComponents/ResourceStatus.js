import React from "react";
import Grid from '@material-ui/core/Grid';
import { ADMIN_PERMITTED_ROLES, AUTHENTICATED_KEYCLOAK_USER_ROLES } from "../../config/constants";
import messages from "../../config/messages";

export default class ResourceStatus extends React.Component {
    constructor(props) {
        super(props);
        this.state = { keycloak: props.keycloak, UserRoles: [] };
        this.isUnmount = false;
        this.isAuthorizedToView = this.isAuthorizedToView.bind(this);
    }

    componentDidMount() {
        const { keycloak } = this.props;
        this.setState({ UserRoles: AUTHENTICATED_KEYCLOAK_USER_ROLES(keycloak) });
    }

    isAuthorizedToView(roles) {
        var isAuthorized = false;
        ADMIN_PERMITTED_ROLES.forEach(item => {
            if (roles.includes(item)) {
                isAuthorized = true;
            }
        })
        return isAuthorized;
    }

    render() {
        const { status, xSize } = this.props;
        if (!this.state.UserRoles.length === 0) {
            return <div></div>
        }
        const isAuth = this.isAuthorizedToView(this.state.UserRoles);
        if (!isAuth) {
            return <div></div>
        }
        return (
            <Grid container direction="column" justifyContent="center" alignItems="flex-end">
                <Grid item xs={xSize}>
                    { /*status ==="p" && <span className="caption grey--font ui purple right ribbon label">published</span>*/}
                    {status === "g" && <span className="caption grey--font ui purple  right ribbon label">{messages.ingested_tag}</span>}
                </Grid>
            </Grid>
        );

    }
}