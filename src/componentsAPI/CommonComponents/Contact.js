import React from "react";
import Typography from '@material-ui/core/Typography';
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
import PersonOutlineIcon from '@material-ui/icons/PersonOutline';
import Grid from '@material-ui/core/Grid';
import WebsitesWithIcon from './WebsitesWithIcon';

export default class Contact extends React.Component {
    render() {
        const { data, metadataLanguage } = this.props;
        if (!data.described_entity.contact) {
            return <div></div>
        }

        return <div className="ContactArea">
            {data.described_entity.contact.length > 0 && <Typography className="bold-p--id"> Contact  </Typography>}
            {data.described_entity.contact.length > 0 && data.described_entity.contact.map((provider, providerIndex) => {
                let actor_type = provider.actor_type;
                if (actor_type === "Person") {
                    let given_name = provider.given_name[metadataLanguage] || provider.given_name[Object.keys(provider.given_name)[0]];
                    let surname = provider.surname[metadataLanguage] || provider.surname[Object.keys(provider.surname)[0]];
                    //let email = provider.email && (provider.email.map(email => email) || []);
                    given_name = (given_name === "unspecified" || given_name === undefined || given_name === "undefined") ? "" : given_name;
                    surname = (surname === "unspecified" || surname === undefined || surname === "undefined") ? "" : surname;
                    return (
                        <div className="padding15" key={providerIndex}>
                            <Grid container spacing={1}>
                                <Grid item xs={3} sm={3}>
                                    <PersonOutlineIcon className="general-icon general-icon--rounded" />
                                </Grid>
                                <Grid item xs={9} sm={9}>
                                    <div key={providerIndex}>
                                        <Typography variant="h4" className="bold-titles">{given_name} {surname}</Typography>
                                    </div>
                                </Grid>
                            </Grid>
                        </div>
                    )


                }
                else if (actor_type === "Organization") {
                    const organization_name = provider.organization_name[metadataLanguage] || provider.organization_name[Object.keys(provider.organization_name)[0]];
                    const website = provider.website || [];
                    return (
                        <div className="padding15" key={providerIndex}>
                            <Grid container spacing={1}>
                                <Grid item xs={3} sm={3}>
                                    <AccountBalanceIcon className="general-icon general-icon--rounded" />
                                </Grid>
                                <Grid item xs={9} sm={9}>
                                    <div key={providerIndex}>
                                        <div><Typography variant="h4" className="bold-titles">{organization_name}</Typography></div>
                                        <WebsitesWithIcon websiteArray={website} />
                                    </div>
                                </Grid>
                            </Grid>

                        </div>
                    )
                } else if (actor_type === "Group") {
                    const organization_name = provider.organization_name[metadataLanguage] || provider.organization_name[Object.keys(provider.organization_name)[0]];
                    const website = provider.website || [];
                    return (
                        <div className="padding15" key={providerIndex}>
                            <Grid container spacing={1}>
                                <Grid item xs={3} sm={3}>
                                    <AccountBalanceIcon className="general-icon general-icon--rounded" />
                                </Grid>
                                <Grid item xs={9} sm={9}>
                                    <div key={providerIndex}>
                                        <div><Typography variant="h4" className="bold-titles">{organization_name}</Typography></div>
                                        <WebsitesWithIcon websiteArray={website} />
                                    </div>
                                </Grid>
                            </Grid>

                        </div>
                    )
                } else {
                    return (
                        <span key={providerIndex}></span>
                    )
                }
            })}
        </div>
    }
}