import React, { Fragment } from "react";
import Typography from '@material-ui/core/Typography';
import GeneralIdentifier from "../CommonComponents/GeneralIdentifier";
import ResourceActorSingleItem from "../CommonComponents/ResourceActorSingleItem";



export default class Attribution extends React.Component {
    render() {
        const { qualified_attribution } = this.props.data.described_entity.field_value;
        const { metadataLanguage } = this.props;

        if (!this.props.data) {
            return <div></div>
        }

        if (!qualified_attribution) {
            return <div></div>
        }
        return <div className="padding15">
            {qualified_attribution && qualified_attribution.field_value.length > 0 && <Typography variant="h3" className="title-links"> {qualified_attribution.field_label[metadataLanguage] || qualified_attribution.field_label["en"]}  </Typography>}
            {qualified_attribution && qualified_attribution.field_value.length > 0 && qualified_attribution.field_value.map((item, index) =>
                <Fragment key={index}>
                    <ResourceActorSingleItem data={item.attributed_agent.field_value} metadataLanguage={metadataLanguage} />
                    {item.had_role && item.had_role.field_value.length > 0 && <div className="padding5">
                        <Typography className="bold-p--id">{item.had_role.field_label[metadataLanguage] || item.had_role.field_label["en"]}</Typography>
                        {item.had_role.field_value.map((hadRoleItem, hadRoleindex) => {                            
                           return <div key={hadRoleindex}>                               
                                {/*<Typography className="bold-p--id">{hadRoleItem.category_label.field_label[metadataLanguage] || hadRoleItem.category_label.field_label["en"]}</Typography> */}                               
                                <span className="info_value">{hadRoleItem.category_label.field_value[metadataLanguage] || hadRoleItem.category_label.field_value["en"]}   </span>                                
                                <GeneralIdentifier data={hadRoleItem} identifier_name={"role_identifier"} identifier_scheme={"role_identifier_scheme"} metadataLanguage={metadataLanguage} />
                            </div>
                        })
                        }
                    </div>
                    }
                </Fragment>
            )
            }

           </div>
    }
}