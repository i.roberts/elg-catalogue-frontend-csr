import React from "react";
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import Tooltip from '@material-ui/core/Tooltip';

import {
    EmailShareButton,
    FacebookShareButton,
    TwitterShareButton,
    LinkedinShareButton,
} from "react-share";
import {
    EmailIcon,
    FacebookIcon,
    TwitterIcon,
    LinkedinIcon,
} from "react-share";

 

export default class ShareMetadata extends React.Component {
    constructor(props) {
        super(props);
        this.state = { copySuccess: '' }
    }

    copyToClipboard = (e) => {
        const el = document.createElement('input');
        el.value = window.location.href;
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
        this.setState({ copySuccess: 'Copied!' });
    };

    render() {
        const shareUrl = this.props.shareUrl;
        const hashtag = this.props.short_name ? this.props.short_name  : "ELG"; 
        return <div className="ActionsButtonArea" style={{ marginBottom: '1em' }}>
            <Typography variant="h3" className="title-links">Share</Typography>
            <Grid container direction="row" justifyContent="flex-start" alignItems="center" spacing={2}>
                <Grid item>
                    <EmailShareButton url={shareUrl} subject={this.props.title} body={this.props.description} separator=":: ">
                        <EmailIcon size={40} bgStyle={{fill :'#ebb447'}} borderRadius={4}/>
                    </EmailShareButton>
                </Grid>
                <Grid item>
                    <FacebookShareButton url={shareUrl} quote = {this.props.title} hashtag={`#${hashtag}`}  >
                        <FacebookIcon size={40}  borderRadius={4}/>
                    </FacebookShareButton>
                </Grid>
                <Grid item>
                    <TwitterShareButton url={shareUrl} title={this.props.title} hashtag={`#${hashtag}`}>
                        <TwitterIcon size={40} borderRadius={4}/>
                    </TwitterShareButton>
                </Grid>
                <Grid item>
                    <LinkedinShareButton url={shareUrl}  title={this.props.title} summary={this.props.description} >
                        <LinkedinIcon size={40} borderRadius={4}/>
                    </LinkedinShareButton>
                </Grid>

                <Grid item>
                    <Tooltip title={this.state.copySuccess || "Copy"}><button onClick={this.copyToClipboard} className="react-share__ShareButton copy-button"> <FileCopyIcon fontSize="small"  /> </button></Tooltip> 
                </Grid>

            </Grid>
        </div>
    }
}