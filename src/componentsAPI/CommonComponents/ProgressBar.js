import React from "react";
import CircularProgress from '@material-ui/core/CircularProgress';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
export default class ProgressBar extends React.Component {

    render() {
        return (
            <Container maxWidth="lg">
                <Grid container direction="row" justifyContent="center" alignItems="center"  >
                    {this.props.number_of_failed_loads >= 1 && <Grid item><div className="loader">An unexpected error occured. Please wait while we are trying to recover</div><hr /></Grid>}
                    <Grid item ><div className="loader"><CircularProgress /></div></Grid>
                </Grid>
            </Container>
        )
    }
}
