import React from "react";
import Grid from "@material-ui/core/Grid"
import Typography from "@material-ui/core/Typography";
import ProgressBar from "../CommonComponents/ProgressBar";
import { Bar } from "react-chartjs-2";
import { Chart, registerables } from 'chart.js';
import zoomPlugin from 'chartjs-plugin-zoom';
Chart.register(...registerables); //https://www.chartjs.org/docs/next/getting-started/integration.html
Chart.register(zoomPlugin);

export default class BarChart extends React.Component {
    constructor(props) {
      super(props);  
      this.state = { BarData: props.BarData, loading: true, }
    }   
    static getDerivedStateFromProps(nextProps, prevState) {
      return {
        BarData: nextProps.BarData
      };
  }
  componentDidMount() {     
      this.setState({ loading: false });    
  }

  render() {
      //https://github.com/reactchartjs/react-chartjs-2/issues/90 
      //chart does not render new data on parent state update
      const BarData = this.state.BarData;
      const options = this.props.options;
      const title = this.props.title; 
      
      if (this.state.loading ) {
        return <ProgressBar />
      } 

     
      return <Grid item container  sm={12} className="chart-container" spacing={2}>               
                <Grid item xs={12}>
                  <Typography variant="h3" className="section-links FunctionCentered"> {title} </Typography>
                  <Bar data={BarData} options={options} redraw={true} key={Math.random()} />                 
                </Grid>
              </Grid>
              
           
      
    }
}