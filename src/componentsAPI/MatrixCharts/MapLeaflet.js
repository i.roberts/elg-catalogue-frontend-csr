import React from "react";
import axios from "axios";
import {
  MapContainer, TileLayer,
  Popup, Marker,
  GeoJSON,
} from 'react-leaflet';
import Paper from "@material-ui/core/Paper";
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import europe from './../../data/europe_modified_eu_eea.json';
import 'leaflet/dist/leaflet.css';
import L from 'leaflet';
import icon from "./../../assets/elg-icons/pin-1.svg";
import { DLE_LANGUAGES } from "./../../config/constants";
const truncated_number_of_items_to_show = 6;
//import icon from 'leaflet/dist/images/marker-icon.png';

const iconMarker = new L.Icon({
  iconUrl: icon,
  iconRetinaUrl: icon,
  iconSize: [26, 40],
  iconAnchor: [10, 41],
  popupAnchor: [2, -40],
  shadowUrl: null,
  shadowSize: null,
  shadowAnchor: null,
  className: "svg-icon",
});
export { iconMarker };


//let DefaultIcon = L.icon({
//  iconUrl: icon, 
//});


L.Marker.prototype.options.icon = iconMarker;

const center = [56.505, 6]
//const position =  [43.653225, -79.383186]

function getColor(d) {
  return d > 1000 ? '#103E42' :
    d > 500 ? '#185E63' :
      d > 200 ? '#248C94' :
        d > 100 ? '#289CA4' :
          d > 50 ? '#3AC5CF' :
            d > 20 ? '#6BD4DB' :
              d > 10 ? '#9CE2E7' :
                '#ebd6ad';
}

export default class MapLeaflet extends React.Component {

  constructor(props) {
    super(props);
    //make europe.geojson easier to call
    var countryGeoJson = europe.features;
    this.state = {
      DATA: countryGeoJson, languages: "", languageValue: "", expandedLangEu: false, expandedLangOther: false,
      //mapTheme: "" , 
      markers: [], width: window.innerWidth, height: window.innerHeight,
      map:null
    }
  }

  componentDidUpdate() {
    if (this.state.map) {
      this.state.map.invalidateSize(true);
    }
  }

  updateDimensions() {
    const height = window.innerWidth >= 992 ? window.innerHeight : 400
    this.setState({ height: height })
    this.setState({ width: window.innerWidth });
  }

  componentDidMount() {
    this.getEULanguages();
    this.setState({ mapTheme: this.style })
    //window.addEventListener("resize", this.updateDimensions())
  }

  //componentWillUnmount() {
  //  window.removeEventListener("resize", this.updateDimensions())
 // }

  style() {
    return {
      fillColor: getColor(101),
      weight: 1,
      // opacity: 1,
      color: 'white',
      dashArray: '3',
      fillOpacity: 0.5,
      opacity: .5, //needs this for the resetStyle to work
    };
  }

  /*hoveredStyle () {
    return {
      fillColor: getColor(501),   
    };
  }*/

  getEULanguages() {
    try {
      axios.get(DLE_LANGUAGES)
        .then(res => {
          this.setState({ languages: res.data.results });
        });
    }
    catch (e) {
      console.log(e);
      return;
    }
  }
  //https://stackoverflow.com/questions/65625746/how-to-add-multiple-markers-using-react-leaflet-upon-api-call
  getMarkers(iso_codes, languageValue) {
    //get lat-long pairs for each iso_code  
    let filterCountries = this.state.languages.filter(item => item.preferred_name === languageValue) || [];
    if (iso_codes.length > 0) {
      let dynamicMarkers = [];
      iso_codes.forEach((iso_code) => {
        let tmp = this.state.DATA.filter(item => item.properties.ISO2 === iso_code)[0];
        let filterCountry = filterCountries[0].countries.filter(item => item.alpha_2 === iso_code) || "";
        //console.log(filterCountry)
        if (tmp) {
          dynamicMarkers.push({
            "lat": tmp.properties.LAT, "lon": tmp.properties.LON,
            "L1": filterCountry[0].speakers.L1 ? filterCountry[0].speakers.L1 : "",
            "L2": filterCountry[0].speakers.L2 ? filterCountry[0].speakers.L2 : "",
            "without_specification": filterCountry[0].speakers.without_specification ? filterCountry[0].speakers.without_specification : "",
            "name": filterCountry[0].speakers.name
          });
        }
      });
      this.setState({ markers: dynamicMarkers })
    }
  }

  handleChange = (event) => {
    this.setState({ languageValue: event.target.value });
    let filterCountries = this.state.languages.filter(item => item.preferred_name === event.target.value) || [];
    let iso_codes = [];
    if (filterCountries.length > 0) {
      filterCountries[0].countries.forEach((country) => {
        iso_codes.push(country.alpha_2)
      });
      //this.setState({ mapTheme: this.hoveredStyle }) //needs work to get it to change color only on selected countries
      this.getMarkers(iso_codes, event.target.value);
    }

  };

  //function that will called when mouseover each feature
  onMouseOver = (event) => {
    event.target.bringToFront()
    event.target.setStyle({
      weight: 3,
      color: "#636e7b", //the color of the border arround each country
      dashArray: "",
      fillOpacity: .7,
      opacity: .7, //needs this for the resetStyle to work
    });
  }

  //reseting the mouseover style, should use resetStyle() but it always error, so i did it
  onMouseOut = (event) => {
    event.target.bringToBack()
    event.target.setStyle({
      color: "white",
      weight: 2
    });
  }


  //function that will called when you click a feature
  onMouseClick = (event) => {
  }

  onEachFeature = (feature, layer) => {
    layer.options.color = "white"
    layer.options.fillOpacity = 0.7
    layer.options.weight = 2
    layer.options.dashArray = 1
    layer.options.opacity = 0.7
    //layer.bindPopup(feature.properties.NAME)
    layer.on({
      mouseover: this.onMouseOver,
      mouseout: this.onMouseOut,
      click: this.onMouseClick
    })

  }
  togglexpandedLangEu = () => { this.setState({ expandedLangEu: !this.state.expandedLangEu }) }
  togglexpandedLangOther = () => { this.setState({ expandedLangOther: !this.state.expandedLangOther }) }

  handleResize = (map) => {
    //console.log(map)
    map.target.setView(center, 3.5);
    setTimeout(() => { map.target.invalidateSize(); }, 1500);
  }

  //handleGoTo = (map)=>{
  //map.flyTo(position) 
  //}

  render() {
    const { expandedLangEu, expandedLangOther } = this.state;
    return <Grid item spacing={1} container xs={12} sm={12} direction="row" justifyContent="flex-start">
      <Grid item sm={2}>
        <Paper elevation={1} className="chart-filters">

          <Grid item xs={12} className="p-05"><Typography variant="h4" className="bold">Official EU languages</Typography></Grid>
          <FormControl component="fieldset" className="pl05">
            <FormGroup className="map-radio-group">
              {expandedLangEu ? (
                <div>
                  <RadioGroup aria-label="language" name="language1" value={this.state.languageValue} onChange={this.handleChange}>
                    {this.state.languages && this.state.languages.length > 0 && this.state.languages.filter(item => item.eu === true).sort((a, b) => (a.preferred_name > b.preferred_name) ? 1 : -1).map((language, index) =>
                      <Grid item key={index}><FormControlLabel key={index} value={language.preferred_name} control={<Radio />} label={language.preferred_name} /></Grid>)
                    }
                  </RadioGroup>
                  <span className="ExpandButton teal--font" onClick={this.togglexpandedLangEu} > Show less </span>
                </div>
              ) : (
                <div>
                  <RadioGroup aria-label="language" name="language1" value={this.state.languageValue} onChange={this.handleChange}>
                    {this.state.languages && this.state.languages.length > 0 && this.state.languages.filter(item => item.eu === true).sort((a, b) => (a.preferred_name > b.preferred_name) ? 1 : -1).slice(0, truncated_number_of_items_to_show).map((language, index) =>
                      <Grid item key={index}><FormControlLabel key={index} value={language.preferred_name} control={<Radio />} label={language.preferred_name} /></Grid>)
                    }
                  </RadioGroup>
                  {this.state.languages.length > truncated_number_of_items_to_show ?
                    <span className="ExpandButton teal--font" onClick={this.togglexpandedLangEu} > Show more </span>
                    : void 0}
                </div>
              )}
            </FormGroup>
          </FormControl>
          <Grid item xs={12} className="p-05"><Typography variant="h4" className="bold">Other european languages</Typography></Grid>
          <FormControl component="fieldset" className="pl05">
            <FormGroup className="map-radio-group">
              {expandedLangOther ? (
                <div>
                  <RadioGroup aria-label="language" name="language1" value={this.state.languageValue} onChange={this.handleChange}>
                    {this.state.languages && this.state.languages.length > 0 && this.state.languages.filter(item => item.eu === false).sort((a, b) => (a.preferred_name > b.preferred_name) ? 1 : -1).map((language, index) =>
                      <Grid item key={index}><FormControlLabel key={index} value={language.preferred_name} control={<Radio />} label={language.preferred_name} /></Grid>)
                    }
                  </RadioGroup>
                  <span className="ExpandButton teal--font" onClick={this.togglexpandedLangOther} > Show less </span>
                </div>
              ) : (
                <div>
                  <RadioGroup aria-label="language" name="language1" value={this.state.languageValue} onChange={this.handleChange}>
                    {this.state.languages && this.state.languages.length > 0 && this.state.languages.filter(item => item.eu === false).sort((a, b) => (a.preferred_name > b.preferred_name) ? 1 : -1).slice(0, truncated_number_of_items_to_show).map((language, index) =>
                      <Grid item key={index}><FormControlLabel key={index} value={language.preferred_name} control={<Radio />} label={language.preferred_name} /></Grid>)
                    }
                  </RadioGroup>
                  {this.state.languages.length > truncated_number_of_items_to_show ?
                    <span className="ExpandButton teal--font" onClick={this.togglexpandedLangOther} > Show more </span>
                    : void 0}
                </div>
              )}
            </FormGroup>
          </FormControl>

        </Paper>
      </Grid>
      <Grid item sm={10}>
        <MapContainer
          id='my-map'
          center={center}
          zoom={3.5}
          attributionControl={true}
          zoomControl={true}
          doubleClickZoom={true}
          scrollWheelZoom={false}
          dragging={true}
          animate={true}
          easeLinearity={0.35}
          style={{ height: "65vh", width: "100%", zIndex: "1" }}
          whenReady={(map) => {
            this.handleResize(map);
          }}
          whenCreated={(map) => {
            //console.log("The underlying leaflet map instance:", map)
            this.setState({map:map});
          }}         
        >
          <TileLayer
            attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
          {/*<Legend />*/}
          <GeoJSON
            data={this.state.DATA}
            style={this.style}
            onEachFeature={this.onEachFeature}
          />

          {this.state.markers.length > 0 &&
            this.state.markers.map((marker) => (
              <Marker
                position={[
                  marker.lat,
                  marker.lon
                ]}
              //icon={icon}
              >
                <Popup>
                  <p>Number of speakers in {marker.name}: </p>
                  {marker.L1 ? <p>L1: {Number(marker.L1).toLocaleString('en')}</p> : ""}
                  {marker.L2 ? <p>L2: {Number(marker.L2).toLocaleString('en')}</p> : ""}
                  {marker.without_specification ? <p>L1/L2 (not specified): {Number(marker.without_specification).toLocaleString('en')}</p> : ""}
                </Popup>
              </Marker>
            ))}

        </MapContainer>
      </Grid>

      <Grid item sm={12} className="pt-1">
        <Typography variant="caption">Select a language from the list to see the European countries where it is spoken as a first (L1) or second language (L2).  </Typography>
        <br/>
        <Typography variant="caption">The information presented does not cover all languages spoken in Europe. Only languages under investigation by the ELE project are presented and only EU member states, EEA countries and Switzerland are active on the map. 
        The position of the pin is random and it does <u>not</u> correspond to a specific region the language is spoken in.  </Typography>       
        <br/>
        <Typography variant="caption"><span>The information about the number of speakers per country was mainly derived from <a href="https://www.ethnologue.com/" target="_blank" rel="noopener noreferrer">Ethnologue</a> (September 2021), while for some languages the numbers were provided by experts in the ELE consortium (see  <a href="https://european-language-equality.eu/languages/" target="_blank" rel="noopener noreferrer">https://european-language-equality.eu/languages/</a>).</span>
        </Typography>
      </Grid>

    </Grid>


  }
}