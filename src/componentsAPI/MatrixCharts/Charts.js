import React from "react";
import Grid from '@material-ui/core/Grid';
import DLECharts from './DLECharts';
import ResourcesChartCrossLanguage from './ResourcesChartCrossLanguage';
//import ResourcesChart from './ResourcesChart';
import ResourcesChartSingleLanguage from './ResourcesChartSingleLanguage';
//import MapLeaflet from "./MapLeaflet";
import About from "./About";
import { TabContent, TabPane, } from "reactstrap";
import NavigationTabs from "./../corpusReusableComponents/NavigationTabs";
import { ELE_BASE_URL } from "./../../config/constants"; 
import eleLogo from "./../../assets/images/rgb_ele__logo--colour.svg";

const tabTitles = [  "About",  "DLE Scores",
 // "Technological factors stats",
  "Cross-language comparison",
  "Within-language comparison",
  //"Map",
  ""
];



export default class Charts extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tab: '1', 
    };
  }
 

  toggleTab = (tabIndex) => {
    this.setState({ tab: tabIndex });
  }
 

  render() {

    return <div className="tab-pane-container">
            {<Grid item xs={2} sm={2} style={{float:"right"}}>
        <a href={ELE_BASE_URL}>
          <img className="ele__logo" src={eleLogo} alt="ELE logo" /></a>
      </Grid>}      

      <NavigationTabs toggleTab={this.toggleTab} activeTab={this.state.tab} tabTitles={tabTitles} />
      <TabContent activeTab={this.state.tab}>
        <TabPane tabId={`${tabTitles.indexOf('About') + 1}`}>
          <Grid container spacing={3} direction="row" justifyContent="center" alignItems="stretch" className="MetaDataDetailsMain">
            <About />
          </Grid>
        </TabPane> 
        <TabPane tabId={`${tabTitles.indexOf('DLE Scores') + 1}`}>
          <Grid container spacing={3} direction="row" justifyContent="center" alignItems="stretch" className="MetaDataDetailsMain">
            <DLECharts />
          </Grid>
        </TabPane> 
        <TabPane tabId={`${tabTitles.indexOf('Cross-language comparison') + 1}`}>
          <Grid container spacing={3} direction="row" justifyContent="center" alignItems="stretch" className="MetaDataDetailsMain">
            <ResourcesChartCrossLanguage />
          </Grid>
        </TabPane> 
        <TabPane tabId={`${tabTitles.indexOf('Within-language comparison') + 1}`}>
          <Grid container spacing={3} direction="row" justifyContent="center" alignItems="stretch" className="MetaDataDetailsMain">
            <ResourcesChartSingleLanguage />
          </Grid>
        </TabPane> 
        {/*<TabPane tabId={`${tabTitles.indexOf('Map') + 1}`}>
          <Grid container spacing={3} direction="row" justifyContent="center" alignItems="stretch" className="MetaDataDetailsMain">
              <MapLeaflet />  
          </Grid>
  </TabPane>*/}         
      </TabContent>


    </div>





  }
}