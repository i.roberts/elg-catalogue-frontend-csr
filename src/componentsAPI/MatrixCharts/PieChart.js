import React from "react";
import Grid from "@material-ui/core/Grid"
import Typography from "@material-ui/core/Typography";
import ProgressBar from "../CommonComponents/ProgressBar";
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from 'chart.js';
import { Pie } from 'react-chartjs-2';
ChartJS.register(ArcElement, Tooltip, Legend);
 
export default class PieChart extends React.Component {
    constructor(props) {
      super(props);  
      this.state = { BarData: props.BarData, 
        loading: true, }
    }   
    static getDerivedStateFromProps(nextProps, prevState) {
      return {
        BarData: nextProps.BarData
      };
  }
 
  componentDidMount() {     
      this.setState({ loading: false });                
  }

  render() {
      //https://github.com/reactchartjs/react-chartjs-2/issues/90 
      //chart does not render new data on parent state update
      const BarData = this.state.BarData;
      const options = this.props.options;
      
      if (!BarData || BarData.length === 0 ) {
        return <ProgressBar />
      }      
      return <Grid item container  sm={12} className="chart-container pie" spacing={2} >               
                <Grid item xs={12} >
                  <Typography variant="h3" className="section-links FunctionCentered"> Scores </Typography>
                  <Pie data={BarData} type="pie" options={options} redraw={true} key={Math.random()} />                
                </Grid>
              </Grid>
      
    }
}