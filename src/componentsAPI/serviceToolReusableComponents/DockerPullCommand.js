import React from "react";
import Tooltip from "@material-ui/core/Tooltip";

export default class DockerPullCommand extends React.Component {
    constructor(props) {
        super(props);
        this.state = { copyPullCommandText: null }
        this.pullCommandInput = React.createRef();
    }

    copyPullCommand = () => {
        const copyText = this.pullCommandInput.current;
        copyText.select();
        copyText.setSelectionRange(0, 99999);
        document.execCommand("copy");
        this.setState({copyPullCommandText: "Copied"})
    }

    render() {
        return <div className="padding5">
            <span className="styles__pullCode" onMouseLeave={() => {
                this.setState({copyPullCommandText: null})
            }}>
                <input ref={this.pullCommandInput} className="copyPullCommandPullCommand" readOnly
                       defaultValue={`docker pull ${this.props.dockerDownloadLocation}`}/>
                <Tooltip title={this.state.copyPullCommandText || "Copy"}>
                    <span className="copyButton" onClick={this.copyPullCommand}>
                        <svg xmlns="http://www.w3.org/2000/svg"
                             preserveAspectRatio="xMidYMid meet" className="dicon"
                             viewBox="0 0 24 24"><g><path fill="none"
                                                          d="M0 0h24v24H0V0z"></path><path
                            d="M16 1H4c-1.1 0-2 .9-2 2v14h2V3h12V1zm3 4H8c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h11c1.1 0 2-.9 2-2V7c0-1.1-.9-2-2-2zm0 16H8V7h11v14z"></path></g></svg>
                    </span>
                </Tooltip>
            </span>
        </div>;
    }
}