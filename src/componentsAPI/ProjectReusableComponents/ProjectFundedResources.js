import React from "react";
//import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import ResourceImage from "./../../assets/images/resource.svg";

class ProjectFundedResources extends React.Component {



    render() {
        //const { described_entity } = this.props;



        return (
            <React.Fragment>
                {
                    <div className="padding15">
                        <Typography variant="h2" className="padding15">Funded Resources</Typography>
                        <div className="padding5">
                            <img alt="img" src={ResourceImage} title="Resource" />
                            <span className="info_url"> Resource name #1 </span>
                        </div>
                        <div className="padding5">
                            <img alt="img" src={ResourceImage} title="Resource" />
                            <span className="info_url"> Resource name #2 </span>
                        </div>

                    </div>
                }



            </React.Fragment>
        );
    }

}

export default ProjectFundedResources;