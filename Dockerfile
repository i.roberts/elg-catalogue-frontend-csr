FROM node:14.19.0-alpine3.15 AS builder

WORKDIR /usr/app
COPY ./ /usr/app

RUN npm install -g npm@8.5.1 && \
    npm install && \
    npx browserslist@latest --update-db && \
    npm run build

FROM httpd:2.4
COPY ./my-httpd.conf /usr/local/apache2/conf/httpd.conf
COPY --from=builder  /usr/app/build/ /usr/local/apache2/htdocs/